package com.enel.scom.api.eordercnxrecepormyfact;

import org.springframework.boot.SpringApplication;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

import com.enel.scom.api.eordercnxrecepormyfact.bean.Entrada;
import com.enel.scom.api.eordercnxrecepormyfact.bean.Fecha;
import com.enel.scom.api.eordercnxrecepormyfact.mapper.PrintPlano4JMapper;
import com.enel.scom.api.eordercnxrecepormyfact.service.GeneralServices;
import com.enel.scom.api.eordercnxrecepormyfact.service.PrintPlano4JServices;
import com.enel.scom.api.eordercnxrecepormyfact.service.PrintPlanoSCOMServices;
import com.enel.scom.api.eordercnxrecepormyfact.util.Constants;
import com.enel.scom.api.eordercnxrecepormyfact.util.Others;


@SpringBootApplication
public class EordercnxrecepormyfactApplication implements CommandLineRunner {


	public static void main(String[] args) {
		SpringApplication.run(EordercnxrecepormyfactApplication.class, args);
		
	}
	
	@Autowired
	GeneralServices services;
	
	@Autowired
	PrintPlanoSCOMServices printplanoSCOM_Service;
	
	@Autowired
	PrintPlano4JServices printplano4J_Service;
	
	@Autowired
	@Qualifier("orclTemplate")
	private JdbcTemplate jdbcORCLTemplate;
	
	@Autowired
	@Qualifier("postgreTemplate")
	private JdbcTemplate jdbcPostgreTemplate;
	
	private static final Logger logger = LoggerFactory.getLogger(EordercnxrecepormyfactApplication.class);

	
	@Override
	public void run(String... args) throws Exception {
		//ejecutarCorrectivo();
		
		//services.validarFactorMedidor("SNX", "8CA03", "3156", "100");
		//System.exit(0);
		
		// Correctivo R.I. 09/08/2023 INICIO
		// Creación y llenado de tabla temporal en el SCOM
		jdbcPostgreTemplate.execute(PrintPlano4JMapper.CREATE_TMP_ORM_FAC_REQUISITO);
		// Obtener datos de Oracle
        List<Map<String, Object>> ormFacRequisito = jdbcORCLTemplate.queryForList(PrintPlano4JMapper.SELECT_FOR_ORM_FAC_REQUISITO);

        // Llenar la tabla temporal en PostgreSQL
        for (Map<String, Object> row : ormFacRequisito) {
            jdbcPostgreTemplate.update(
            		PrintPlano4JMapper.INSERT_INTO_TMP_ORM_FAC_REQUISITO,
                row.get("COD_REQUISITO"),
                row.get("EVALUAR"),
                row.get("ACTIVO")
            );
        }
		// Correctivo R.I. 09/08/2023 FIN
        
        // Pruebas TMP_ORM_FAC_REQUISITO
        //printplano4J_Service.probarTmpOrmFacRequisito();
		//printplanoSCOM_Service.probarTmpOrmFacRequisito();
		//System.exit(0);
		// Pruebas TMP_ORM_FAC_REQUISITO
		
		logger.info("Inicio.");
		String proceso = "EOrderCNXRecepORMyFact";
		try {
			// borrar/comentar cuando se haga el pase a los servidores linux produccion
			// args = new String[3];
	        // args[0] = "EDEL";
	        // args[1] = "pe02850937";
	        // args[2] = "20230228130812";
	        
	        Others.lAcumulaTemp = 0;
	        Others.lFilasProc = 0;
	        Others.lFilasErr = 0;
	        Others.lFilasCorrectas=0;

	        
			if(args.length!=3) {
				logger.info("\nUso de parámetros: {} <Empresa> <Usuario> <Nro.ODT>\n", proceso);
				System.exit(1);
			}
			
			Entrada entradaDTO = new Entrada();
		 	entradaDTO.setGc_EMPRESA(args[0]);
		 	entradaDTO.setGc_USER(args[1]);
		 	//entradaDTO.setGc_CONEXION(args[2]);
		 	entradaDTO.setGi_ODT(Long.parseLong(args[2]));
		 
		 	logger.info("Datos de entrada.");
			logger.info("CodigoEmpresa: {}.", entradaDTO.getGc_EMPRESA());
			//logger.info("Conexion: {}.", entradaDTO.getGc_CONEXION());
			logger.info("CodigoUsuario: {}.", entradaDTO.getGc_USER());
			logger.info("Nro_ODT: {}.", entradaDTO.getGi_ODT());
			
			//CONEXION A LA BASE DE DATOS - YA SE HACE POR SPRING BOOT
			
			Fecha fechaDTO = new Fecha();
			fechaDTO.setGcFecha(services.getFechaHoy());
			
			logger.info("Fecha Hora Inicio del Proceso: {}",fechaDTO.getGcFecha());
			
			/* Verifica la Empresa */
			Others.gi_Empresa=services.bfnVerificaEmpresa(entradaDTO.getGc_EMPRESA());
			
			if(Others.gi_Empresa==null) { //Valor de Empresa encontrado 
				logger.error("Error: No se pudo obtener valor de Empresa:  {} ", entradaDTO.getGc_EMPRESA());
				System.exit(1);
			}
			
			/* Obtener ID Usuario */
			Others.gIdUsuario= services.bfnObtenerIdUsuario(entradaDTO.getGc_USER().trim());
			if(Others.gIdUsuario==null) {
				logger.error("Error: No se pudo obtener el Usuario:  {} ", entradaDTO.getGc_USER().trim());
				System.exit(1);
			}
			
			/* Obtener Path Salida*/
			Others.gcPathOUT =services.bfnObtenerPath("PATH_CNX","LGC_NC_OU");
			if(Others.gcPathOUT == null) {
				logger.error("Error: No se encuentra configurada la Ruta de Salida [COB_LGC_OU]");
				System.exit(1);
			}
			logger.info("Others.gcPathOUT: {}", Others.gcPathOUT);
			
			// borrar/comentar cuando se haga el pase a los servidores linux produccion
			// Others.gcPathOUT = Constants.RUTA_LINUX_DES_LOCAL.concat(Others.gcPathOUT);
			
			Others.gcPathORMIn =services.bfnGetPathORM_In();
			if(Others.gcPathORMIn == null) {
				logger.error("Error: No se encontró la ruta de entrada de proceso de respuesta ORM");
				System.exit(1);
			}
			logger.info("Others.gcPathORMIn: {}", Others.gcPathORMIn);
			
			// borrar/comentar cuando se haga el pase a los servidores linux produccion
			// Others.gcPathORMIn = Constants.RUTA_LINUX_DES_LOCAL.concat(Others.gcPathORMIn);
			
			/* Obtener estados de transferencia */
			Others.vCodEstPendRecep =services.bfnObtenerEstadoTransferencia("PRECE");
			if(Others.vCodEstPendRecep == null) {
				logger.error("Error: No se encuentra configurado el Estado de Transferencia [PRECE]");
				System.exit(1);
			}
			
			Others.vCodEstEnProceso =services.bfnObtenerEstadoTransferencia("PROCE");
			if(Others.vCodEstEnProceso == null) {
				logger.error("Error: No se encuentra configurado el Estado de Transferencia [PROCE]");
				System.exit(1);
			}
			
			Others.vCodEstRecep =services.bfnObtenerEstadoTransferencia("RECEP");
			if(Others.vCodEstRecep == null) {
				logger.error("Error: No se encuentra configurado el Estado de Transferencia [RECEP]");
				System.exit(1);
			}
			
			/*Recepcionado con Error*/
			Others.vCodEstRecepError =services.bfnObtenerEstadoTransferencia("RECER");
			if(Others.vCodEstRecepError == null) {
				logger.error("Error: No se encuentra configurado el Estado de Transferencia [RECER]");
				System.exit(1);
			}
			
			/*Obteniendo Errores*/
			Map<String, String> error;
			
			error =services.bfnObtenerErrores("ASY000");
			if(error == null) {
				logger.error("Error: No se encuentra configurado el Estado de Transferencia [ASY000]");
				System.exit(1);
			}
			Others.vCodErrASY000=error.get("VALOR_ALF");
			Others.vDesErrASY000=error.get("DESCRIPCION");
			
			
			error =services.bfnObtenerErrores("ASY002");
			if(error == null) {
				logger.error("Error: No se encuentra configurado el Estado de Transferencia [ASY002]");
				System.exit(1);
			}
			Others.vCodErrASY002=error.get("VALOR_ALF");
			Others.vDesErrASY002=error.get("DESCRIPCION");
			
			
			error =services.bfnObtenerErrores("ASY013");
			if(error == null) {
				logger.error("Error: No se encuentra configurado el Estado de Transferencia [ASY013]");
				System.exit(1);
			}
			Others.vCodErrASY013=error.get("VALOR_ALF");
			Others.vDesErrASY013=error.get("DESCRIPCION");
			
			
			error =services.bfnObtenerErrores("ASY024");
			if(error == null) {
				logger.error("Error: No se encuentra configurado el Estado de Transferencia [ASY024]");
				System.exit(1);
			}
			Others.vCodErrASY024=error.get("VALOR_ALF");
			Others.vDesErrASY024=error.get("DESCRIPCION");
			
			
			error =services.bfnObtenerErrores("ASY045");
			if(error == null) {
				logger.error("Error: No se encuentra configurado el Estado de Transferencia [ASY045]");
				System.exit(1);
			}
			Others.vCodErrASY045=error.get("VALOR_ALF");
			Others.vDesErrASY045=error.get("DESCRIPCION");
			
			
			error =services.bfnObtenerErrores("ASY099");
			if(error == null) {
				logger.error("Error: No se encuentra configurado el Estado de Transferencia [ASY099]");
				System.exit(1);
			}
			Others.vCodErrASY099=error.get("VALOR_ALF");
			Others.vDesErrASY099=error.get("DESCRIPCION");
			
			
			Others.cCodCuadrillaGen =services.bfnObtenerCuadrillaGen();
			if(Others.cCodCuadrillaGen == null) {
				logger.error("Error: No se encuentra configurado codigo de cuadrilla generico de ejecucion");
				System.exit(1);
			}
			
			/*roliva*/
			Others.vTipoRespon = services.bfnObtenerParametros("ORM_CAMPO","TIPRES");
			if(Others.vTipoRespon == null) {
				logger.error("Error: No se encuentra configurado Tipo Responsable Ordenes creadas en campo");
				System.exit(1);
			}
			
			
			Others.vRolRespon = services.bfnObtenerParametros("ORM_CAMPO","ROLRES");
			if(Others.vRolRespon == null) {
				logger.error("Error: No se encuentra configurado Rol Responsable Ordenes creadas en campo");
				System.exit(1);
			}
			
			Others.vCodPrior = services.bfnObtenerParametros("ORM_CAMPO","PRIOR");
			if(Others.vCodPrior == null) {
				logger.error("Error: No se encuentra configurado Prioridad de ordenes creadas en campo");
				System.exit(1);
			}
			
			/* Archivo Errores para Usuario */
			Others.gcNomArchivoERR = String.format("%sEOR_CNX_RECEP_ORM_y_FACT_ERRORES%d.txt", Others.gcPathOUT,entradaDTO.getGi_ODT());
			/* Archivo de Entrada del Proceso de Respuesta Masiva ORM */
			Others.gcNomArchivoMasivaORM_SR = String.format("EOR_CNX_RECEP_ORM%d.txt", entradaDTO.getGi_ODT());
			Others.gcNomArchivoMasivaORM = String.format("%s/EOR_CNX_RECEP_ORM%d.txt", Others.gcPathORMIn,entradaDTO.getGi_ODT());
			/* Archivo de Entrada del Proceso de Respuesta Masiva Factibilidad */
			Others.gcNomArchivoMasivaFACT =String.format("%sEOR_CNX_RECEP_FACT%d.txt", Others.gcPathOUT,entradaDTO.getGi_ODT());
			
			logger.info("gcNomArchivoERR: {}",Others.gcNomArchivoERR);
			logger.info("gcNomArchivoMasivaORM_SR: {}",Others.gcNomArchivoMasivaORM_SR);
			logger.info("gcNomArchivoMasivaORM: {}",Others.gcNomArchivoMasivaORM);
			logger.info("gcNomArchivoMasivaFACT: {}",Others.gcNomArchivoMasivaFACT);


			
			boolean bfnAbrirArchivo=bfnAbrirArchivo();	
			if(!bfnAbrirArchivo) {
				System.exit(1);
			}
			
			/*Tag Común*/
			Others.qTagComun=services.bfnFetchTagComun();
			/* Tag Común - Cantidad*/
			if(Others.qTagComun==null || Others.qTagComun.isEmpty()){
				System.exit(1);
			}
			Others.iTotTagComun=Others.qTagComun.size();
			
			/*Tag Conexiones(Acta de Revisión)*/
			Others.qTagActaRevision=services.bfnFetchTagActaRevision();
			/* Tag Conexiones(Acta de Revisión) - Cantidad*/
			if(Others.qTagActaRevision==null || Others.qTagComun.isEmpty()){
				System.exit(1);
			}
			Others.iTotTagActaRevision=Others.qTagActaRevision.size();
			
			/* Recepciona las ordenes de Mantenimiento y Factibilidad reportadas por el eOrder
			 -Obtiene los registros a procesar */
			
			 Others.qRegPro=services.bfnObtenerRegistrosProcesar(Others.vCodEstPendRecep); //ORDENES DE SCOM
			logger.info("qRegPro: {}", Others.qRegPro.size());
			
			Others.qRegPro4J=services.bfnObtenerRegistrosProcesar4J(Others.vCodEstPendRecep); //ORDENES DE 4J
			logger.info("qRegPro4J: {}", Others.qRegPro4J.size());
			
			logger.info("Inicio de impresión de archivo plano...");
			
			Others.lFilasArchivoORM=0;
			Others.lFilasArchivoORF = 0;
			
			if((Others.qRegPro!=null && !Others.qRegPro.isEmpty()) || (Others.qRegPro4J!=null && !Others.qRegPro4J.isEmpty())){
				/************************************************************************************************************/
				/*RECORRIDO DEL CURSOR SCOM*/
				/************************************************************************************************************/

				for (Map<String, Object> reg : Others.qRegPro) {
					
					Others.vIdOrdTransfer=(Long) reg.get("ID_ORD_TRANSFER");
					Others.vNroRecepciones=((BigDecimal)reg.get("NRO_RECEPCIONES")).longValue() ;
					Others.vCodTipoOrdenEorder=(String) reg.get("COD_TIPO_ORDEN_EORDER");
					Others.vNroOrden=(String) reg.get("NRO_ORDEN_LEGACY");
					
					logger.info("vIdOrdTransfer: {}",Others.vIdOrdTransfer);
					logger.info("vNroRecepciones: {}",Others.vNroRecepciones);
					logger.info("vCodTipoOrdenEorder: {}",Others.vCodTipoOrdenEorder);
					logger.info("vNroOrden: {}",Others.vNroOrden);
							
					Others.vObservacion="";
					Others.iIndicaErr=0;
					Others.lFilasProc++;
					vLimpiarArreglos();
					Others.vCodOperacion=Others.vCodErrASY000;
					// Obtiene los datos de la orden
					logger.info("Llama ImprimirPlano:{}",Others.vCodOperacion);
					boolean imprimirPlano= printplanoSCOM_Service.imprimirPlano();
					if(!imprimirPlano){
						//System.exit(1); R.I. Correccion para continuar al siguiente registro 21/03/2022
						continue;
					}
					logger.info("Sale ImprimirPlano:{}",Others.vCodOperacion);
					
					logger.info("Sale ImprimirPlano:{}",Others.vCodTipoOrdenEorder);
					logger.info("Sale ImprimirPlano:{}",Others.gcPathOUT);
					logger.info("Sale ImprimirPlano:{}",Others.gcNum_Orden);
					logger.info("Sale ImprimirPlano:{}",Others.iIndicaErr);
				
					if(Others.iIndicaErr == 1)
					{
						Others.lFilasErr++;
						logger.info(String.format("Orden nro[%s] - %s  \n", Others.vNroOrden,Others.vObservacion));
						
						if(!printplanoSCOM_Service.bfnActualizaRegistro())
						  {	
							//System.exit(1); R.I. Correccion para continuar al siguiente registro 21/03/2022
						  }

					}
				}//End While SCOM
				
				logger.info(String.format("\n lFilasProc-SCOM:[%d]\n", Others.lFilasProc));
				
				if(Others.lFilasProc==0) {
					logger.info(String.format("NO se encontraron registros a procesar"));
					Others.printWriterinFile1.printf("NO se encontraron registros a procesar\n");
				}
				logger.info("--PROCESAMIENTO de las ORDENES DE SCOM-STATUS--");
				logger.info(String.format("Filas procesadas correctamente  :[%d]\n", Others.lFilasProc-Others.lFilasErr ));
				logger.info(String.format("Filas procesadas con error      :[%d]\n", Others.lFilasErr));
				logger.info(String.format("Total de Filas                  :[%d]\n", Others.lFilasProc));
				
				/********************************************FIN DEL CURSOR SCOM***************************************************************/
				
				/************************************************************************************************************/
				/*RECORRIDO DEL CURSOR 4J*/
				/************************************************************************************************************/
				Others.lFilasProc=0;
				Others.lFilasErr=0;
				for (Map<String, Object> reg4j : Others.qRegPro4J) {

					
					Others.vIdOrdTransfer=(Long) reg4j.get("ID_ORD_TRANSFER");
					Others.vNroRecepciones=((BigDecimal)reg4j.get("NRO_RECEPCIONES")).longValue() ;
					Others.vCodTipoOrdenEorder=(String) reg4j.get("COD_TIPO_ORDEN_EORDER");
					Others.vNroOrden=(String) reg4j.get("NRO_ORDEN_LEGACY");
					
					logger.info("vIdOrdTransfer: {}",Others.vIdOrdTransfer);
					logger.info("vNroRecepciones: {}",Others.vNroRecepciones);
					logger.info("vCodTipoOrdenEorder: {}",Others.vCodTipoOrdenEorder);
					logger.info("vNroOrden: {}",Others.vNroOrden);
							
					Others.vObservacion="";
					Others.iIndicaErr=0;
					Others.lFilasProc++;
					vLimpiarArreglos();
					Others.vCodOperacion=Others.vCodErrASY000;
					// Obtiene los datos de la orden
					logger.info("Llama ImprimirPlano:{}",Others.vCodOperacion);
					boolean imprimirPlano= printplano4J_Service.imprimirPlano();
					if(!imprimirPlano){
						//System.exit(1); R.I. Correccion para continuar al siguiente registro 21/03/2022
						continue;
					}
					logger.info("Sale ImprimirPlano:{}",Others.vCodOperacion);
					
					logger.info("Sale ImprimirPlano:{}",Others.vCodTipoOrdenEorder);
					logger.info("Sale ImprimirPlano:{}",Others.gcPathOUT);
					logger.info("Sale ImprimirPlano:{}",Others.gcNum_Orden);
					logger.info("Sale ImprimirPlano:{}",Others.iIndicaErr);
				
					if(Others.iIndicaErr == 1)
					{
						Others.lFilasErr++;
						logger.info(String.format("Orden nro[%s] - %s  \n", Others.vNroOrden,Others.vObservacion));
						
						if(!printplano4J_Service.bfnActualizaRegistro())
						  {	
							//System.exit(1); R.I. Correccion para continuar al siguiente registro 21/03/2022
						  }

					}
				}//End While 4J
				logger.info(String.format("\n lFilasProc-4J:[%d]\n", Others.lFilasProc));
				
				if(Others.lFilasProc==0) {
					logger.info(String.format("NO se encontraron registros a procesar"));
					Others.printWriterinFile1.printf("NO se encontraron registros a procesar\n");
				}
				logger.info("--PROCESAMIENTO de las ORDENES DE 4J-STATUS--");
				logger.info(String.format("Filas procesadas correctamente  :[%d]\n", Others.lFilasProc-Others.lFilasErr ));
				logger.info(String.format("Filas procesadas con error      :[%d]\n", Others.lFilasErr));
				logger.info(String.format("Total de Filas                  :[%d]\n", Others.lFilasProc));
				
				/*********************************************FIN DEL CURSOR 4J**************************************************************/
				
				Others.file2Writer.close(); // equivale a fclose(fArchivoMasivaORM);
				
				if(Others.lFilasArchivoORM>0) {
				
					bfnLanzaProcesoRespuestaMasivaORM(entradaDTO);
				}

				Fecha fechaDTOS = new Fecha();
				fechaDTOS.setGcFecha(services.getFechaHoy());
				
				logger.info("Fecha Hora Termino del Proceso  {}",fechaDTOS.getGcFecha());
				
				Others.file1Writer.close();  // equivale a fclose(fArchivoErr);
				
				logger.info("Ok");
				System.exit(0);
			}
			
			
		} catch (Exception e) {
			
			logger.error("Error en la app EOrderCNXRecepORMyFact");
			logger.error("Mensaje error: {}, Stack trace: {}", e.getMessage(), e.getStackTrace());
			logger.error("Interrupted!", e);
			System.exit(1);

			
			// Restore interrupted state...
			// Thread.currentThread().interrupt();
		}
	}


	private void ejecutarCorrectivo() {
		// INICIO CORRECTIVO
		logger.info("INICIO CORRECTIVO");
		// OBLIGATORIOS
		String selectQuery = "SELECT R.id_fac_requisito  AS id_fac_requisito, \r\n"
				+ "       COALESCE(O.VALOR_REQUISITO, '') AS Valor_requisito,\r\n"
				+ "       E.nro_orden_legacy AS Nro_orden_legacy\r\n"
				+ "FROM (\r\n"
				+ "    SELECT unnest(xpath('/item/codigo_requisito/text()', d.REG_XML))::text AS Codigo_requisito, \r\n"
				+ "           unnest(xpath('/item/valor_requisito/text()', d.REG_XML))::text AS Valor_requisito,\r\n"
				+ "           d.id_ord_transfer\r\n"
				+ "    FROM (\r\n"
				+ "        SELECT unnest(xpath('/Requisitos_Obligatorios/item', \r\n"
				+ "                    unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/factibilidad/Requisitos_Obligatorios', d.REG_XML)))) AS REG_XML,\r\n"
				+ "               d.id_ord_transfer\r\n"
				+ "        FROM SCHSCOM.EOR_ORD_TRANSFER_DET d  \r\n"
				+ "        WHERE d.id_ord_transfer IN (\r\n"
				+ "            SELECT id_ord_transfer \r\n"
				+ "            FROM eor_ord_transfer\r\n"
				+ "            WHERE cod_tipo_orden_legacy = 'MANT' \r\n"
				+ "              AND fec_operacion >= '03/08/2023'\r\n"
				+ "              AND cod_tipo_orden_eorder = 'NCX.05'\r\n"
				+ "              and cod_estado_orden in (5, 6, 8)\r\n"
				+ "              AND id_ord_transfer IN (\r\n"
				+ "                SELECT d.id_ord_transfer\r\n"
				+ "                FROM SCHSCOM.EOR_ORD_TRANSFER_DET d\r\n"
				+ "                WHERE d.accion = 'RECEPCION'\r\n"
				+ "                  AND EXISTS (\r\n"
				+ "                    SELECT 1\r\n"
				+ "                    FROM unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/actasConexion/Cod_Tipo_Acometida/text()', d.REG_XML)) AS x(Cod_Tipo_Acometida)\r\n"
				+ "                    WHERE x.Cod_Tipo_Acometida::text IN ('A', 'S', 'M')\r\n"
				+ "                  )\r\n"
				+ "              )\r\n"
				+ "        ) AND accion = 'RECEPCION'\r\n"
				+ "    ) d\r\n"
				+ ") O\r\n"
				+ "JOIN SCHSCOM.ORM_FAC_REQUISITO R \r\n"
				+ "    ON O.CODIGO_REQUISITO = RTRIM(R.COD_REQUISITO)\r\n"
				+ "JOIN eor_ord_transfer E\r\n"
				+ "    ON O.id_ord_transfer = E.id_ord_transfer\r\n"
				+ "WHERE R.EVALUAR = 'S'";

        String updateQuery = "UPDATE SYNERGIA.ORM_IN_FAC_DETALLE "
                + "SET VALOR_REQUISITO = ? "
                + "WHERE ID_FAC_REQUISITO = ? "
                + "AND ID_ORD_FACTI = (SELECT id_orden FROM ord_orden WHERE nro_orden = ? AND id_tipo_orden = 2)";

        // Ejecutar la consulta SELECT
        List<Map<String, Object>> result = jdbcPostgreTemplate.queryForList(selectQuery);

        // Variables para evitar crear objetos en el bucle
        String idFacRequisito, valorRequisito, nroOrdenLegacy;
        int valorActualizado;

        // Iterar sobre los resultados y actualizar en la base de datos de destino
        for (Map<String, Object> row : result) {
            idFacRequisito = row.get("id_fac_requisito").toString();
            valorRequisito = row.get("Valor_requisito").toString();
            nroOrdenLegacy = row.get("Nro_orden_legacy").toString();

            // Convertir valor_requisito a 1 o 0
            valorActualizado = valorRequisito.equals("true") ? 1 : 0;
            jdbcORCLTemplate.update(updateQuery, valorActualizado, idFacRequisito, nroOrdenLegacy);
        }

        // OPCIONALES
        selectQuery = "SELECT \r\n"
        		+ "    R.id_fac_requisito  AS id_fac_requisito, \r\n"
        		+ "    COALESCE(O.VALOR_REQUISITO, '') AS Valor_requisito,\r\n"
        		+ "    E.nro_orden_legacy AS Nro_orden_legacy\r\n"
        		+ "FROM (\r\n"
        		+ "    SELECT \r\n"
        		+ "        R.COD_REQUISITO AS Codigo_requisito,\r\n"
        		+ "        coalesce(O.VALOR_REQUISITO, '') AS Valor_requisito,\r\n"
        		+ "        O.id_ord_transfer\r\n"
        		+ "    FROM (\r\n"
        		+ "        SELECT \r\n"
        		+ "            unnest(xpath('/item/codigo_requisito/text()', d.REG_XML))::text AS Codigo_requisito,  \r\n"
        		+ "            unnest(xpath('/item/valor_requisito/text()', d.REG_XML))::text AS Valor_requisito,\r\n"
        		+ "            d.id_ord_transfer\r\n"
        		+ "        FROM (\r\n"
        		+ "            SELECT  \r\n"
        		+ "                unnest(xpath('/ListaRequisitosOpcionales/item', \r\n"
        		+ "                unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/factibilidad/OtrosRequisitos/ListaRequisitosOpcionales', d.REG_XML)))) AS REG_XML,\r\n"
        		+ "                d.id_ord_transfer\r\n"
        		+ "            FROM SCHSCOM.EOR_ORD_TRANSFER_DET d\r\n"
        		+ "            WHERE d.id_ord_transfer IN (\r\n"
        		+ "                SELECT id_ord_transfer \r\n"
        		+ "                FROM eor_ord_transfer\r\n"
        		+ "                WHERE cod_tipo_orden_legacy = 'MANT' \r\n"
        		+ "                  AND fec_operacion >= '03/08/2023'\r\n"
        		+ "                  AND cod_tipo_orden_eorder = 'NCX.05'\r\n"
        		+ "                  AND cod_estado_orden in (5, 6, 8)\r\n"
        		+ "                  AND id_ord_transfer IN (\r\n"
        		+ "                      SELECT d.id_ord_transfer\r\n"
        		+ "                      FROM SCHSCOM.EOR_ORD_TRANSFER_DET d\r\n"
        		+ "                      WHERE d.accion = 'RECEPCION'\r\n"
        		+ "                      AND EXISTS (\r\n"
        		+ "                          SELECT 1\r\n"
        		+ "                          FROM unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/actasConexion/Cod_Tipo_Acometida/text()', d.REG_XML)) AS x(Cod_Tipo_Acometida)\r\n"
        		+ "                          WHERE x.Cod_Tipo_Acometida::text IN ('A', 'S', 'M')\r\n"
        		+ "                      )\r\n"
        		+ "                  )\r\n"
        		+ "            ) \r\n"
        		+ "            AND accion = 'RECEPCION' \r\n"
        		+ "        ) d\r\n"
        		+ "    ) O\r\n"
        		+ "    JOIN SCHSCOM.ORM_FAC_REQUISITO R \r\n"
        		+ "        ON O.CODIGO_REQUISITO = RTRIM(R.COD_REQUISITO)\r\n"
        		+ "    WHERE R.EVALUAR = 'S'\r\n"
        		+ ") O\r\n"
        		+ "JOIN SCHSCOM.ORM_FAC_REQUISITO R \r\n"
        		+ "    ON RTRIM(O.CODIGO_REQUISITO) = RTRIM(R.COD_REQUISITO)\r\n"
        		+ "JOIN eor_ord_transfer E\r\n"
        		+ "    ON O.id_ord_transfer = E.id_ord_transfer\r\n"
        		+ "WHERE R.EVALUAR = 'S'";

        // Ejecutar la consulta SELECT
        result = jdbcPostgreTemplate.queryForList(selectQuery);

        // Iterar sobre los resultados y actualizar en la base de datos de destino
        for (Map<String, Object> row : result) {
            idFacRequisito = row.get("id_fac_requisito").toString();
            valorRequisito = row.get("Valor_requisito").toString();
            nroOrdenLegacy = row.get("Nro_orden_legacy").toString();

            // Convertir valor_requisito a 1 o 0
            valorActualizado = valorRequisito.equals("true") ? 1 : 0;
            jdbcORCLTemplate.update(updateQuery, valorActualizado, idFacRequisito, nroOrdenLegacy);
        }
        logger.info("FIN CORRECTIVO");
        System.exit(0);
		// FIN CORRECTIVO
	}


	private void bfnLanzaProcesoRespuestaMasivaORM(Entrada entradaDTO) throws IOException, InterruptedException {
		logger.info("bfnLanzaProcesoRespuestaMasivaORM");
		logger.info("entradaDTO: {}", entradaDTO);

		logger.info("{} {} {} {} {} {} {} {}", "java",
		"-jar",
		"/binarios/ejecutables/SCOM/eorder/conexiones/ORMRespuestaMasiva.jar",
		"EDEL",
		Others.gcNomArchivoMasivaORM_SR,
		entradaDTO.getGi_ODT(),
		entradaDTO.getGc_USER(),
		"EORDER");

		ProcessBuilder pb = new ProcessBuilder(
        		"java",
        		"-jar",
        		"/binarios/ejecutables/SCOM/eorder/conexiones/ORMRespuestaMasiva.jar",
        		"EDEL",
        		Others.gcNomArchivoMasivaORM_SR,
        		entradaDTO.getGi_ODT().toString(),
        		entradaDTO.getGc_USER(),
        		"EORDER");
		
        Process process = pb.start();

        InputStream is = process.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String line = null;

		while ((line = br.readLine()) != null) {
			logger.info(line);
		}
		process.waitFor();
        
        if (process.exitValue() != 0) {
        	logger.info("Proceso no se ejecutó con éxito");
        } else {
			logger.info("Proceso se ejecutó con éxito");
		}
	}


	private void vLimpiarArreglos() {
		if(Others.qFactReqObli!=null) {
			Others.qFactReqObli.clear();
		}
		if(Others.qFactReqOpci!=null) {
			Others.qFactReqOpci.clear();
		}
		if(Others.qFactInfoAdi!=null) {
			Others.qFactInfoAdi.clear();
		}
		if(Others.qFactOtrosSuministro!=null) {
			Others.qFactOtrosSuministro.clear();
		}
		if(Others.qTareas!=null) {
			Others.qTareas.clear();
		}
		if(Others.qRecursos!=null) {
			Others.qRecursos.clear();
		}
		if(Others.qMedidores!=null) {
			Others.qMedidores.clear();
		}
		if(Others.qDatoClave!=null) {
			Others.qDatoClave.clear();
		}
		if(Others.qOper!=null) {
			Others.qOper.clear();
		}
		if(Others.qLecturas!=null) {
			Others.qLecturas.clear();
		}
		if(Others.qSellos!=null) {
			Others.qSellos.clear();
		}
		if(Others.qActaRevision!=null) {
			Others.qActaRevision.clear();
		}
		if(Others.qORM!=null) {
			Others.qORM.clear();
		}
		if(Others.arrMedidorRet!=null) {
			Others.arrMedidorRet.clear();
		}else {
			Others.arrMedidorRet =new ArrayList<>();
		}
		if(Others.arrMedidorInst!=null) {
			Others.arrMedidorInst.clear();
		}else {
			Others.arrMedidorInst =new ArrayList<>();
		}
	}

	private boolean bfnAbrirArchivo() {
		 String nameFileCurrentExecution="";
			try {
				  nameFileCurrentExecution=Others.gcNomArchivoERR;
				  //Abro stream, crea el fichero si no existe
				  Others.file1Writer=new FileWriter(nameFileCurrentExecution);
				  Others.printWriterinFile1 = new PrintWriter(Others.file1Writer);			  
				  
				  nameFileCurrentExecution=Others.gcNomArchivoMasivaORM;
				  //Abro stream, crea el fichero si no existe
				  Others.file2Writer=new FileWriter(nameFileCurrentExecution);
				  Others.printWriterinFile2 = new PrintWriter(Others.file2Writer);
				  
				  return true;
		        } catch (IOException e) {
		        	logger.error(String.format("No se pudo generar el archivo de salida de Errores [%s]: %s",
		        			nameFileCurrentExecution, e.getMessage()));
		        	return false;
		        }
	}

}
