package com.enel.scom.api.eordercnxrecepormyfact.bean;
import lombok.Data;

@Data
public class Entrada {
    private String gc_EMPRESA;
    private String gc_USER;
    private String gc_CONEXION;
    private Long gi_ODT;
}
