package com.enel.scom.api.eordercnxrecepormyfact.bean;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Usuario {

	private Long id;
}
