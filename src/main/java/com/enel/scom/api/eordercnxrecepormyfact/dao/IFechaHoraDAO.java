package com.enel.scom.api.eordercnxrecepormyfact.dao;

import com.enel.scom.api.eordercnxrecepormyfact.bean.Fecha;


public interface IFechaHoraDAO {

	Fecha getFechaHoy(String formato);
	Fecha getFechaFormatted(String fecha,String formato);
}
