package com.enel.scom.api.eordercnxrecepormyfact.dao;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

public interface IOthersDAO {

	String bfnObtenerPath(String entidad, String key);

	String bfnGetPathORM_In();

	Integer bfnObtenerEstadoTransferencia(String key);

	Map<String, String> bfnObtenerErrores(String key);

	String bfnObtenerCuadrillaGen();

	String bfnObtenerParametros(String entidad, String key);

	List<Map<String, Object>> bfnFetchTagComun();

	List<Map<String, Object>> bfnFetchTagActaRevision();

	List<Map<String, Object>> bfnObtenerRegistrosProcesar(Integer vCodEstPendRecep);

	List<Map<String, Object>> bfnObtenerRegistrosProcesar4J(Integer vCodEstPendRecep);

	boolean validarFactorMedidor(String marcaMedidor, String modeloMedidor, String numMedidor, String factorMedidor);
	
}
