package com.enel.scom.api.eordercnxrecepormyfact.dao;

import java.util.List;
import java.util.Map;

public interface IPrintPlanoSCOMDAO {

	List<Map<String, Object>> getRequisitosObligatorios(Long vIdOrdTransfer, Long vNroRecepciones);

	List<Map<String, Object>> getRequisitosOpcionales(Long vIdOrdTransfer, Long vNroRecepciones);

	List<Map<String, Object>> getInformacionAdicional(Long vIdOrdTransfer, Long vNroRecepciones);

	List<Map<String, Object>> getSuministros(Long vIdOrdTransfer, Long vNroRecepciones);

	Map<String, Object> getDatosClaves(Long vIdOrdTransfer, Long vNroRecepciones);

	Map<String, Object> getColeccionOperaciones(Long vIdOrdTransfer, Long vNroRecepciones);

	List<Map<String, Object>> getColeccionTareas(Long vIdOrdTransfer, Long vNroRecepciones);

	Map<String, Object> getColeccionRecursosParaObtenerNombresdeTecnico(Long vIdOrdTransfer,
			Long vNroRecepciones);

	List<Map<String, Object>> getColeccionMedidores(Long vIdOrdTransfer, Long vNroRecepciones);

	List<Map<String, Object>> getColeccionLecturas(Long vIdOrdTransfer, Long vNroRecepciones);

	List<Map<String, Object>> getColeccionSellos(Long vIdOrdTransfer, Long vNroRecepciones);

	Map<String, Object> getDatosActaDeRevision(Long vIdOrdTransfer, Long vNroRecepciones);

	Map<String, Object> getMotivoTemayTrabajo(Long vIdOrdTransfer, Long vNroRecepciones);
	

	String verifyFechaFormatted(String pszFecha, String pszPatron);

	int searchIfORMexist(String c_Codigo_Externo_del_TdC);

	Long bfnObtenerNroCuenta(String pKey);

	int bfnValidarDominio(String pNomTabla, String pNomCampo, String pValorCampo);

	int bfnReqCambioMed(String pCodigo_Tarea);

	Map<String, Object> bfnGetCodEjeGen(String cCodCuadrillaGen);

	int bfnValidarCompRet(Long pNroCuenta, String pCodMarca, String pCodModelo, String pNroComp);

	int bfnValidarCompInstall(String pCodMarca, String pCodModelo, String pNroComp);

	String obtenerFechaDDMMYYY(String cFechaYYYYMMDD);

	int bfnValidarMedidaMedidor(String pCodMarca, String pCodModelo, String pNroComp, String pMedida);

	String bfnObtenerCodigoContratista(String cCodCuadrillaGen);

	Map<String, Object> bfnGetFechaHoraEje(String pFechaLarga);

	String getiIdMotivoH(String aCodMotivo);

	Map<String, Object> getiIdResponsableH(String aCodResponsable, String aCodTipoResponsable);

	String getiIdTrabajoH(String aCodMotivo);

	String getiIdPrioridadH(String aCodPrioridad);

	String getiIdTemaH(String aCodPrioridad);

	Map<String, Object> getiNroCuentaHandiIdServicioH(String aNroCuenta);

	String getSysDate();

	String getNroOrdenH(String aCodigo_Interno_del_TdC, String aCodigo_Tipo_de_TdC);

	Long getMaxIdWorkFlow();

	boolean bfnCrearIdWorkflow(Long maxValue_sqworkflow);

	Long getNextValSq_OrdenMantenimiento();
	
	Long getNextValSq_Orden();

	boolean bfnCrearOrdenGenerico(Long lNroOrdenH, Long iIdWorkflowH, Long iIdOrdenH, Long iIdMotivoH,
			Long iIdServicioH, Long iIdUsuarioH, Long iIdBuzonH);

	boolean bfnCrearOrdenMasiva(Long iIdOrdenH, Long iIdTrabajoH, Long iIdPrioridadH, Long iIdTemaH);

	boolean bfnCrearOrdenDerivada(Long iIdOrdenH, Long iIdResponsableH, Long iIdResponsableH2);

	boolean bfnActualizaNroOrdenLegacy(String cNroOrdenLegacyH, Long vIdOrdTransfer);

	int actualizaTablaTransferencia(String vCodOperacion, Integer vCodEstRecepError, String vObservacion, Long vIdOrdTransfer, Long vNroRecepciones);
}
