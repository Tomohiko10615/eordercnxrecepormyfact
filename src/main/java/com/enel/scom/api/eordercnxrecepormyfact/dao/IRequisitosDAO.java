package com.enel.scom.api.eordercnxrecepormyfact.dao;

import java.util.List;

import com.enel.scom.api.eordercnxrecepormyfact.validator.dto.Requisito;

public interface IRequisitosDAO {

	List<Requisito> obtenerRequisitosObligatorios(Long idOrdTransfer, Long nroRecepciones);

	List<Requisito> obtenerRequisitosOpcionales(Long idOrdTransfer, Long nroRecepciones);

	String obtenerTipoAcometida(Long idOrdTransfer, Long nroRecepciones);

	List<Requisito> obtenerValoresValidos(Integer idTipoAcometida, int idFacTipoRequisito);

	Integer obtenerIdTipoAcometida(String codTipoAcometida);

}
