package com.enel.scom.api.eordercnxrecepormyfact.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.eordercnxrecepormyfact.dao.IEmpresaDAO;
import com.enel.scom.api.eordercnxrecepormyfact.mapper.EmpresaMapper;

@Repository
public class EmpresaDaoImpl implements IEmpresaDAO{

	@Autowired
	@Qualifier("postgreTemplate")
	private JdbcTemplate jdbcPostgreTemplate;
	
	private static final Logger logger = LoggerFactory.getLogger(EmpresaDaoImpl.class);
	
	@Override
	public Integer bfnVerificaEmpresa(String gc_EMPRESA) {
		try 
		{
        	String sqlString = EmpresaMapper.SQL_SELECT_FOR_VERIFY_EMPRESA;
			logger.debug(sqlString);
        	return jdbcPostgreTemplate.queryForObject(sqlString,Integer.class,gc_EMPRESA);

        } catch (DataAccessException e) {
            logger.error("Error al obtener el ID de la empresa {}", e.getMessage());
            return null;
        }
	}

}
