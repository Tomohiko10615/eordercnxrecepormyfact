package com.enel.scom.api.eordercnxrecepormyfact.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.eordercnxrecepormyfact.bean.Fecha;
import com.enel.scom.api.eordercnxrecepormyfact.dao.IFechaHoraDAO;
import com.enel.scom.api.eordercnxrecepormyfact.mapper.FechaHoraMapper;

@Repository
public class FechaHoraDaoImpl implements IFechaHoraDAO {

	@Autowired
	@Qualifier("postgreTemplate")
	private JdbcTemplate jdbcPostgreTemplate;
	
	private static final Logger logger = LoggerFactory.getLogger(FechaHoraDaoImpl.class);


	@Override
	public Fecha getFechaHoy(String formato) {
		try 
		{
        	String sqlString = FechaHoraMapper.SQL_SELECT_FOR_FECHA_HORA_HOY;
			logger.debug("Executing query FechaHoraMapper.SQL_SELECT_FOR_FECHA_HORA_HOY");
        	return jdbcPostgreTemplate.queryForObject(sqlString, new FechaHoraMapper(),formato);

        } catch (DataAccessException e) {
            logger.error("Error al obtener fecha hora {}: {}", formato, e.getMessage());
            return null;
        }
	}
	
	
	@Override
	public Fecha getFechaFormatted(String fecha,String formato)
	{
		try 
		{        	
        	String sqlString = FechaHoraMapper.SQL_SELECT_FOR_FECHA_HORA_FORMATTED;
			logger.info(sqlString);
			Fecha oFechaHora=jdbcPostgreTemplate.queryForObject(sqlString, new FechaHoraMapper(),fecha,formato);
        	logger.info("fechaHora: {}", oFechaHora);
        	return oFechaHora;

        } catch (DataAccessException e) {
            logger.error("Error al obtener fecha hora {}: {}. {}.", formato, fecha, e.getMessage());
            return null;
        }
	}

}
