package com.enel.scom.api.eordercnxrecepormyfact.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.eordercnxrecepormyfact.dao.IOthersDAO;
import com.enel.scom.api.eordercnxrecepormyfact.mapper.OthersMapper;
import com.enel.scom.api.eordercnxrecepormyfact.mapper.TagMapper;
import com.enel.scom.api.eordercnxrecepormyfact.util.Constants;

@Repository
public class OthersDaoImpl implements IOthersDAO{

	@Autowired
	@Qualifier("postgreTemplate")
	private JdbcTemplate jdbcPostgreTemplate;
	
	@Autowired
	@Qualifier("orclTemplate")
	private JdbcTemplate jdbcORCLTemplate;
	
	private static final Logger logger = LoggerFactory.getLogger(OthersDaoImpl.class);
	
	@Override
	public String bfnObtenerPath(String entidad, String key) {
		String result=null;
		try 
		{
        	String sqlString = OthersMapper.SQL_SELECT_OBTENER_PATH;
			logger.debug("Executing query OthersMapper.SQL_SELECT_OBTENER_PATH");
        	result= jdbcPostgreTemplate.queryForObject(sqlString, String.class,entidad,key);

        } catch (DataAccessException e) {
            logger.error("Error al obtener path {}",e.getMessage());
            return null;
        }
		return result;
	}

	@Override
	public String bfnGetPathORM_In() {
		String result=null;
		try 
		{
        	String sqlString = OthersMapper.SQL_SELECT_OBTENER_PATH_ORM_IN;
			logger.debug("Executing query OthersMapper.SQL_SELECT_OBTENER_PATH_ORM_IN");
			result= jdbcPostgreTemplate.queryForObject(sqlString, String.class);

        } catch (DataAccessException e) {
            logger.error("Error al obtener path_orm_in {}",e.getMessage());
            return null;
        }
		return result;
	}

	@Override
	public Integer bfnObtenerEstadoTransferencia(String key) {
		Integer result=null;
		try 
		{
        	String sqlString = OthersMapper.SQL_SELECT_OBTENER_TRANSFERENCIA;
			logger.debug("Executing query OthersMapper.SQL_SELECT_OBTENER_TRANSFERENCIA");
			result= jdbcPostgreTemplate.queryForObject(sqlString, Integer.class,key);

        } catch (DataAccessException e) {
            logger.error("Error al obtener Estado de Transferencia {}: {}",key,e.getMessage());
            return null;
        }
		return result;
	}

	@Override
	public Map<String, String> bfnObtenerErrores(String key) {
		Map<String, String> map = new HashMap<>();
		try 
		{
        	String sqlString = OthersMapper.SQL_SELECT_OBTENER_ERRORES;
			logger.debug("Executing query OthersMapper.SQL_SELECT_OBTENER_ERRORES");
			List<Map<String, Object>> results = jdbcPostgreTemplate.queryForList(sqlString,key);
			for (Map<String, Object> map2 : results) {
				  map.put("VALOR_ALF", (String) map2.get("VALOR_ALF"));
				  map.put("DESCRIPCION", (String) map2.get("DESCRIPCION"));
			}

			return map;
        } catch (DataAccessException e) {
            logger.error("Error al obtener Estado de Transferencia {}: {}",key,e.getMessage());
            return null;
        }

	}

	@Override
	public String bfnObtenerCuadrillaGen() {
		String result=null;
		try 
		{
        	String sqlString = OthersMapper.SQL_SELECT_OBTENER_CUADRILLA_GENERICA;
			logger.debug("Executing query OthersMapper.SQL_SELECT_OBTENER_CUADRILLA_GENERICA");
			result= jdbcPostgreTemplate.queryForObject(sqlString, String.class);

        } catch (DataAccessException e) {
            logger.error("Error al obtener path_orm_in {}",e.getMessage());
            return null;
        }
		return result;
	}

	@Override
	public String bfnObtenerParametros(String entidad, String key) {
		String result=null;
		try 
		{
        	String sqlString = OthersMapper.SQL_SELECT_OBTENER_PARAMETROS;
			logger.debug("Executing query OthersMapper.SQL_SELECT_OBTENER_PARAMETROS");
			result= jdbcPostgreTemplate.queryForObject(sqlString, String.class,entidad,key);

        } catch (DataAccessException e) {
            logger.error("Error al obtener path_orm_in {}",e.getMessage());
            return null;
        }
		return result;
	}

	@Override
	public List<Map<String, Object>> bfnFetchTagComun() {
		List<Map<String, Object>> result= new ArrayList<>();
		try {
			String sqlString = TagMapper.SQL_SELECT_TAG_COMUN;
			logger.debug("Executing query TagMapper.SQL_SELECT_TAG_COMUN");
			result= jdbcPostgreTemplate.queryForList(sqlString);
		} catch (DataAccessException e) {
			logger.error("Error en declarar Tag Común: {}",e.getMessage());
			return null;
		}
		return result;
	}

	@Override
	public List<Map<String, Object>> bfnFetchTagActaRevision() {
		List<Map<String, Object>> result= new ArrayList<>();
		try {
			String sqlString = TagMapper.SQL_SELECT_TAG_COMUN_REVISION;
			logger.debug("Executing query TagMapper.SQL_SELECT_TAG_COMUN_REVISION");
			result= jdbcPostgreTemplate.queryForList(sqlString);
		} catch (DataAccessException e) {
			logger.error("Error en declarar Tag Común Revision: {}",e.getMessage());
			return null;
		}
		return result;
	}

	@Override
	public List<Map<String, Object>> bfnObtenerRegistrosProcesar(Integer vCodEstPendRecep) {
		List<Map<String, Object>> result= new ArrayList<>();
		try {
			String sqlString = OthersMapper.SQL_SELECT_REGISTROS_PROCESAR;
			logger.debug("Executing query OthersMapper.SQL_SELECT_REGISTROS_PROCESAR");
			result= jdbcPostgreTemplate.queryForList(sqlString,vCodEstPendRecep,Constants.TDC_ORM);
		} catch (DataAccessException e) {
			logger.error("Error en bfnObtenerRegistrosProcesar: {}",e.getMessage());
			return null;
		}
		return result;
	}

	@Override
	public List<Map<String, Object>> bfnObtenerRegistrosProcesar4J(Integer vCodEstPendRecep) {
		List<Map<String, Object>> result= new ArrayList<>();
		try {
			String sqlString = OthersMapper.SQL_SELECT_REGISTROS_PROCESAR_4J;
			logger.debug("Executing query OthersMapper.SQL_SELECT_REGISTROS_PROCESAR_4J");
			result= jdbcPostgreTemplate.queryForList(sqlString,vCodEstPendRecep,Constants.TDC_ORM,Constants.TDC_FACTIBILIDAD);
		} catch (DataAccessException e) {
			logger.error("Error en declarar Tag Común: {}",e.getMessage());
			return null;
		}
		return result;
	}

	@Override
	public boolean validarFactorMedidor(String marcaXml, String modeloXml, String medidorXml,
			String cfactor) {
		int count = 0;
		try {
			 String sqlString = OthersMapper.SQLPOSTGRESQL_COUNT_MEDIDORES;
			 logger.debug("Executing query PrintPlanoMapper.SQLPOSTGRESQL_SELECT_COUNT_MEDIDORES");
			 count = jdbcPostgreTemplate.queryForObject(sqlString, Integer.class,  medidorXml, modeloXml, marcaXml,
					 	modeloXml, marcaXml, cfactor, 
					 	medidorXml, modeloXml, marcaXml, 
						modeloXml, marcaXml, cfactor);
		} catch (EmptyResultDataAccessException ee) {
         logger.info("Error en Executing query MAPPERS.QUERY.SQLPOSTGRESQL_SELEC_COUNT_MEDIDORES: {}",ee.getMessage());
		}
		logger.info("Count factor:{}", count);
		logger.info("Validación del factor:{}", count != 0);
		return count != 0;
	}
}
