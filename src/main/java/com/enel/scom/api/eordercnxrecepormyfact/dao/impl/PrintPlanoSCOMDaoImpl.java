package com.enel.scom.api.eordercnxrecepormyfact.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enel.scom.api.eordercnxrecepormyfact.dao.IPrintPlanoSCOMDAO;
import com.enel.scom.api.eordercnxrecepormyfact.mapper.PrintPlanoSCOMMapper;
import com.enel.scom.api.eordercnxrecepormyfact.util.Constants;

@Repository
public class PrintPlanoSCOMDaoImpl implements IPrintPlanoSCOMDAO {


	@Autowired
	@Qualifier("postgreTemplate")
	private JdbcTemplate jdbcPostgreTemplate;
	
	private static final Logger logger = LoggerFactory.getLogger(PrintPlanoSCOMDaoImpl.class);
		
	@Override
	public List<Map<String, Object>> getRequisitosObligatorios(Long vIdOrdTransfer, Long vNroRecepciones) {
		List<Map<String, Object>> result= new ArrayList<>();
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_SELECT_REQUISITOS_OBLIGATORIOS;
			logger.debug("Executing query PrintPlanoMapper.SQL_SELECT_REQUISITOS_OBLIGATORIOS");
			result= jdbcPostgreTemplate.queryForList(sqlString,vIdOrdTransfer,vNroRecepciones);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_REQUISITOS_OBLIGATORIOS: {}",ee.getMessage());
			return null;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_REQUISITOS_OBLIGATORIOS: {}",e.getMessage());
			return null;
		}
		return result;
	}

	@Override
	public List<Map<String, Object>> getRequisitosOpcionales(Long vIdOrdTransfer, Long vNroRecepciones) {
		List<Map<String, Object>> result= new ArrayList<>();
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_SELECT_REQUISITOS_OPCIONALES;
			logger.debug("Executing query PrintPlanoMapper.SQL_SELECT_REQUISITOS_OPCIONALES");
			result= jdbcPostgreTemplate.queryForList(sqlString,vIdOrdTransfer,vNroRecepciones);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_REQUISITOS_OPCIONALES: {}",ee.getMessage());
			return null;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_REQUISITOS_OPCIONALES: {}",e.getMessage());
			return null;
		}
		return result;
	}

	@Override
	public List<Map<String, Object>> getInformacionAdicional(Long vIdOrdTransfer, Long vNroRecepciones) {
		List<Map<String, Object>> result= new ArrayList<>();
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_SELECT_INFORMACIÓN_ADICIONAL;
			logger.debug("Executing query PrintPlanoMapper.SQL_SELECT_INFORMACIÓN_ADICIONAL");
			result= jdbcPostgreTemplate.queryForList(sqlString,vIdOrdTransfer,vNroRecepciones);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_INFORMACIÓN_ADICIONAL: {}",ee.getMessage());
			return null;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_INFORMACIÓN_ADICIONAL: {}",e.getMessage());
			return null;
		}
		return result;
	}

	@Override
	public List<Map<String, Object>> getSuministros(Long vIdOrdTransfer, Long vNroRecepciones) {
		List<Map<String, Object>> result= new ArrayList<>();
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_SELECT_SUMINISTROS;
			logger.debug("Executing query PrintPlanoMapper.SQL_SELECT_SUMINISTROS");
			result= jdbcPostgreTemplate.queryForList(sqlString,vIdOrdTransfer,vNroRecepciones);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_SUMINISTROS: {}",ee.getMessage());
			return null;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_SUMINISTROS: {}",e.getMessage());
			return null;
		}
		return result;
	}

	@Override
	public Map<String, Object> getDatosClaves(Long vIdOrdTransfer, Long vNroRecepciones) {
		Map<String, Object> result= new HashMap<>();
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_SELECT_DATOS_CLAVES;
			logger.debug("Executing query PrintPlanoMapper.SQL_SELECT_DATOS_CLAVES");
			result= jdbcPostgreTemplate.queryForMap(sqlString,vIdOrdTransfer,vNroRecepciones);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_DATOS_CLAVES: {}",ee.getMessage());
			return null;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_DATOS_CLAVES: {}",e.getMessage());
			return null;
		}
		return result;
	}

	@Override
	public Map<String, Object> getColeccionOperaciones(Long vIdOrdTransfer, Long vNroRecepciones) {
		Map<String, Object> result= new HashMap<>();
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_SELECT_OPERACIONES;
			logger.debug("Executing query PrintPlanoMapper.SQL_SELECT_OPERACIONES");
			result= jdbcPostgreTemplate.queryForMap(sqlString,vIdOrdTransfer,vNroRecepciones);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_OPERACIONES: {}",ee.getMessage());
			return null;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_OPERACIONES: {}",e.getMessage());
			return null;
		}
		return result;
	}

	@Override
	public List<Map<String, Object>> getColeccionTareas(Long vIdOrdTransfer, Long vNroRecepciones) {
		List<Map<String, Object>> result = new ArrayList<>();
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_SELECT_TAREAS;
			logger.debug("Executing query PrintPlanoMapper.SQL_SELECT_TAREAS");
			result= jdbcPostgreTemplate.queryForList(sqlString,vIdOrdTransfer,vNroRecepciones);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_TAREAS: {}",ee.getMessage());
			return null;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_TAREAS: {}",e.getMessage());
			return null;
		}
		return result;
	}

	@Override
	public Map<String, Object> getColeccionRecursosParaObtenerNombresdeTecnico(Long vIdOrdTransfer,
			Long vNroRecepciones) {
		Map<String, Object> result= new HashMap<>();
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_SELECT_NOMBRES_DE_TECNICO;
			logger.debug("Executing query PrintPlanoMapper.SQL_SELECT_NOMBRES_DE_TECNICO");
			result= jdbcPostgreTemplate.queryForMap(sqlString,vIdOrdTransfer,vNroRecepciones);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_NOMBRES_DE_TECNICO: {}",ee.getMessage());
			return null;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_NOMBRES_DE_TECNICO: {}",e.getMessage());
			return null;
		}
		return result;
	}

	@Override
	public List<Map<String, Object>> getColeccionMedidores(Long vIdOrdTransfer, Long vNroRecepciones) {
		List<Map<String, Object>> result= new ArrayList<>();
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_SELECT_COLECCION_MEDIDORES;
			logger.debug("Executing query PrintPlanoMapper.SQL_SELECT_COLECCION_MEDIDORES");
			result= jdbcPostgreTemplate.queryForList(sqlString,vIdOrdTransfer,vNroRecepciones);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_COLECCION_MEDIDORES: {}",ee.getMessage());
			return null;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_COLECCION_MEDIDORES: {}",e.getMessage());
			return null;
		}
		return result;
	}

	@Override
	public List<Map<String, Object>> getColeccionLecturas(Long vIdOrdTransfer, Long vNroRecepciones) {
		List<Map<String, Object>> result= new ArrayList<>();
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_SELECT_COLECCION_LECTURAS;
			logger.debug("Executing query PrintPlanoMapper.SQL_SELECT_COLECCION_LECTURAS");
			result= jdbcPostgreTemplate.queryForList(sqlString,vIdOrdTransfer,vNroRecepciones);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_COLECCION_LECTURAS: {}",ee.getMessage());
			return null;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_COLECCION_LECTURAS: {}",e.getMessage());
			return null;
		}
		return result;
	}

	@Override
	public List<Map<String, Object>> getColeccionSellos(Long vIdOrdTransfer, Long vNroRecepciones) {
		List<Map<String, Object>> result= new ArrayList<>();
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_SELECT_COLECCION_SELLOS;
			logger.debug("Executing query PrintPlanoMapper.SQL_SELECT_COLECCION_LECTURAS");
			result= jdbcPostgreTemplate.queryForList(sqlString,vIdOrdTransfer,vNroRecepciones);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_COLECCION_SELLOS: {}",ee.getMessage());
			return null;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_COLECCION_LECTURAS: {}",e.getMessage());
			return null;
		}
		return result;
	}

	@Override
	public Map<String, Object> getDatosActaDeRevision(Long vIdOrdTransfer, Long vNroRecepciones) {
		Map<String, Object>result= new HashMap<>();
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_SELECT_DATOS_ACTA_DE_REVISION;
			logger.debug("Executing query PrintPlanoMapper.SQL_SELECT_DATOS_ACTA_DE_REVISION");
			result= jdbcPostgreTemplate.queryForMap(sqlString,vIdOrdTransfer,vNroRecepciones);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_DATOS_ACTA_DE_REVISION: {}",ee.getMessage());
			return null;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_DATOS_ACTA_DE_REVISION: {}",e.getMessage());
			return null;
		}
		return result;
	}

	@Override
	public Map<String, Object> getMotivoTemayTrabajo(Long vIdOrdTransfer, Long vNroRecepciones) {
		Map<String, Object>result= new HashMap<>();
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_SELECT_MOTIVO_TEMA_Y_TRABAJO;
			logger.debug("Executing query PrintPlanoMapper.SQL_SELECT_MOTIVO_TEMA_Y_TRABAJO");
			result= jdbcPostgreTemplate.queryForMap(sqlString,vIdOrdTransfer,vNroRecepciones);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_MOTIVO_TEMA_Y_TRABAJO: {}",ee.getMessage());
			return null;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_MOTIVO_TEMA_Y_TRABAJO: {}",e.getMessage());
			return null;
		}
		return result;
	}

	@Override
	public String verifyFechaFormatted(String pszFecha, String pszPatron) {
		String fechaReturn="";
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_VERIFY_FORMATTED_FECHA;
			logger.debug("Executing query PrintPlanoMapper.SQL_VERIFY_FORMATTED_FECHA");
			fechaReturn = jdbcPostgreTemplate.queryForObject(sqlString,String.class,pszFecha,pszPatron);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_MOTIVO_TEMA_Y_TRABAJO: {}",ee.getMessage());
			return "";
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_VERIFY_FORMATTED_FECHA: {}",e.getMessage());
			return "";
		}
		return fechaReturn;
	}

	@Override
	public int searchIfORMexist(String c_Codigo_Externo_del_TdC) {
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_SEARCH_IF_ORM_EXIST_;
			logger.debug("Executing query PrintPlanoMapper.SQL_SEARCH_IF_ORM_EXIST_");
			return jdbcPostgreTemplate.queryForObject(sqlString,Integer.class,c_Codigo_Externo_del_TdC);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SEARCH_IF_ORM_EXIST_: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SEARCH_IF_ORM_EXIST_: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public Long bfnObtenerNroCuenta(String pKey) {
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_OBTENER_NUMERO_DE_CUENTA;
			logger.debug("Executing query PrintPlanoMapper.SQL_OBTENER_NUMERO_DE_CUENTA");
			return jdbcPostgreTemplate.queryForObject(sqlString,Long.class,pKey);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_OBTENER_NUMERO_DE_CUENTA: {}",ee.getMessage());
			return 0L;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_OBTENER_NUMERO_DE_CUENTA: {}",e.getMessage());
			return 0L;
		}
	}

	@Override
	public int bfnValidarDominio(String pNomTabla, String pNomCampo, String pValorCampo) {
		logger.info("bfnValidarDominio scom");
		
		logger.info("pNomTabla: {}", pNomTabla);
		logger.info("pNomCampo: {}", pNomCampo);
		logger.info("pValorCampo: {}", pValorCampo);

		int iCount=0;
		try {
			String sqlString = "SELECT COUNT(1) from SCHSCOM."+ pNomTabla +" WHERE "+pNomCampo+" = TRIM(UPPER('"+pValorCampo+"')) ";
			logger.info("bfnValidarDominio script scom: {}", sqlString);
			logger.debug("Executing query PrintPlanoMapper.bfnValidarDominio");
			iCount= jdbcPostgreTemplate.queryForObject(sqlString,Integer.class);
		}  catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query PrintPlanoMapper.bfnValidarDominio: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.bfnValidarDominio: {}",e.getMessage());
			return 0;
		}
		return iCount;
	}

	@Override
	public int bfnReqCambioMed(String pCodigo_Tarea) {
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_COUNT_REQ_CAMBIO_MED;
			logger.debug("Executing query PrintPlanoMapper.SQL_COUNT_REQ_CAMBIO_MED");
			return jdbcPostgreTemplate.queryForObject(sqlString,Integer.class,pCodigo_Tarea);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_COUNT_REQ_CAMBIO_MED: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_COUNT_REQ_CAMBIO_MED: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public Map<String, Object> bfnGetCodEjeGen(String cCodCuadrillaGen) {
		Map<String, Object> result= new HashMap<>();
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_GET_COD_EJE_GEN;
			logger.debug("Executing query PrintPlanoMapper.SQL_GET_COD_EJE_GEN");
			result= jdbcPostgreTemplate.queryForMap(sqlString,cCodCuadrillaGen);
		}catch (EmptyResultDataAccessException ee) {
			logger.info("No se encontró datos de Código Ejecutor y Nombre de Técnico.");
			logger.error("EmptyResultDataAccessException->" + ee.getMessage());
			return null;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_GET_COD_EJE_GEN: {}",e.getMessage());
			return null;
		}
		return result;
	}

	@Override
	public int bfnValidarCompRet(Long pNroCuenta, String pCodMarca, String pCodModelo, String pNroComp) {
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_COUNT_VALIDAR_COMP_RET;
			logger.debug("Executing query PrintPlanoMapper.SQL_COUNT_VALIDAR_COMP_RET");
			return jdbcPostgreTemplate.queryForObject(sqlString,Integer.class,pNroCuenta,pCodMarca,pCodModelo,pNroComp);
		}  catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_COUNT_VALIDAR_COMP_RET: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_COUNT_VALIDAR_COMP_RET: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public int bfnValidarCompInstall(String pCodMarca, String pCodModelo, String pNroComp) {
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_COUNT_VALIDAR_COMP_INSTALL;
			logger.debug("Executing query PrintPlanoMapper.SQL_COUNT_VALIDAR_COMP_INSTALL");
			return jdbcPostgreTemplate.queryForObject(sqlString,Integer.class,pCodMarca,pCodModelo,pNroComp);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_COUNT_VALIDAR_COMP_INSTALL: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_COUNT_VALIDAR_COMP_INSTALL: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public String obtenerFechaDDMMYYY(String cFechaYYYYMMDD) {
		String fechaReturn="";
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_OBTENER_FECHA_DDMMYYY;
			logger.debug("Executing query PrintPlanoMapper.SQL_OBTENER_FECHA_DDMMYYY");
			fechaReturn = jdbcPostgreTemplate.queryForObject(sqlString,String.class,cFechaYYYYMMDD);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_OBTENER_FECHA_DDMMYYY: {}",ee.getMessage());
			return null;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_OBTENER_FECHA_DDMMYYY: {}",e.getMessage());
			return null;
		}
		return fechaReturn;
	}

	@Override
	public int bfnValidarMedidaMedidor(String pCodMarca, String pCodModelo, String pNroComp, String pMedida) {
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_VALIDAR_MEDIDA_MEDIDOR;
			logger.debug("Executing query PrintPlanoMapper.SQL_VALIDAR_MEDIDA_MEDIDOR");
			return jdbcPostgreTemplate.queryForObject(sqlString,Integer.class,pCodMarca,pCodModelo,pNroComp,pMedida);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_VALIDAR_MEDIDA_MEDIDOR: {}",ee.getMessage());
			return 0;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_VALIDAR_MEDIDA_MEDIDOR: {}",e.getMessage());
			return 0;
		}
	}

	@Override
	public String bfnObtenerCodigoContratista(String cCodCuadrillaGen) {
		String codContratista="";
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_OBTENER_COD_CONTRATISTA;
			logger.debug("Executing query PrintPlanoMapper.SQL_OBTENER_COD_CONTRATISTA");
			codContratista = jdbcPostgreTemplate.queryForObject(sqlString,String.class,cCodCuadrillaGen);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query PrintPlanoMapper.SQL_OBTENER_COD_CONTRATISTA: {}",ee.getMessage());
			return "";
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_OBTENER_COD_CONTRATISTA: {}",e.getMessage());
			return "";
		}
		return codContratista;
	}

	@Override
	public Map<String, Object> bfnGetFechaHoraEje(String pFechaLarga) {
		Map<String, Object> result= new HashMap<>();
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_SELECT_GET_FECHA_HORA_EJE;
			logger.debug("Executing query PrintPlanoMapper.SQL_SELECT_GET_FECHA_HORA_EJE");
			result= jdbcPostgreTemplate.queryForMap(sqlString,pFechaLarga,pFechaLarga,pFechaLarga);
		}catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_GET_FECHA_HORA_EJE: {}",ee.getMessage());
			return null;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_GET_FECHA_HORA_EJE: {}",e.getMessage());
			return null;
		}
		return result;
	}

	@Override
	public String getiIdMotivoH(String aCodMotivo) {
		String iIdResponsableH="";
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_OBTENER_ID_MOTIVO;
			logger.debug("Executing query PrintPlanoMapper.SQL_OBTENER_ID_RESPONSABLE");
			iIdResponsableH = jdbcPostgreTemplate.queryForObject(sqlString,String.class,aCodMotivo);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query PrintPlanoMapper.SQL_OBTENER_ID_MOTIVO: {}",ee.getMessage());
			return "";
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_OBTENER_ID_RESPONSABLE: {}",e.getMessage());
			return "";
		}
		return iIdResponsableH;
	}

	@Override
	public Map<String, Object> getiIdResponsableH(String aCodResponsable, String aCodTipoResponsable) {
		Map<String, Object> result= new HashMap<>();
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_SELECT_ID_RESPONSABLE_Y_BUZON;
			logger.debug("Executing query PrintPlanoMapper.SQL_SELECT_ID_RESPONSABLE_Y_BUZON");
			result= jdbcPostgreTemplate.queryForMap(sqlString,aCodResponsable,aCodTipoResponsable);
		}  catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_ID_RESPONSABLE_Y_BUZON: {}",ee.getMessage());
			return null;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_ID_RESPONSABLE_Y_BUZON: {}",e.getMessage());
			return null;
		}
		return result;
	}

	@Override
	public String getiIdTrabajoH(String aCodMotivo) {
		String iIdTrabajoH="";
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_OBTENER_ID_TRABAJO_H;
			logger.debug("Executing query PrintPlanoMapper.SQL_OBTENER_ID_TRABAJO_H");
			iIdTrabajoH = jdbcPostgreTemplate.queryForObject(sqlString,String.class,aCodMotivo);
		}  catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query PrintPlanoMapper.SQL_OBTENER_ID_TRABAJO_H: {}",ee.getMessage());
			return "";
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_OBTENER_ID_TRABAJO_H: {}",e.getMessage());
			return null;
		}
		return iIdTrabajoH;
	}

	@Override
	public String getiIdPrioridadH(String aCodPrioridad) {
		String iIdPrioridadH="";
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_OBTENER_ID_PRIORIDAD_H;
			logger.debug("Executing query PrintPlanoMapper.SQL_OBTENER_ID_PRIORIDAD_H");
			iIdPrioridadH = jdbcPostgreTemplate.queryForObject(sqlString,String.class,aCodPrioridad);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query PrintPlanoMapper.SQL_OBTENER_ID_PRIORIDAD_H: {}",ee.getMessage());
			return "";
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_OBTENER_ID_PRIORIDAD_H: {}",e.getMessage());
			return "";
		}
		return iIdPrioridadH;
	}

	@Override
	public String getiIdTemaH(String aCodPrioridad) {
		String iIdTemaH="";
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_OBTENER_ID_TEMA_H;
			logger.debug("Executing query PrintPlanoMapper.SQL_OBTENER_ID_TEMA_H");
			iIdTemaH = jdbcPostgreTemplate.queryForObject(sqlString,String.class,aCodPrioridad);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query PrintPlanoMapper.SQL_OBTENER_ID_TEMA_H: {}",ee.getMessage());
			return "";
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_OBTENER_ID_TEMA_H: {}",e.getMessage());
			return "";
		}
		return iIdTemaH;
	}

	@Override
	public Map<String, Object> getiNroCuentaHandiIdServicioH(String aNroCuenta) {
		Map<String, Object> result= new HashMap<>();
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_SELECT_SERVICIO_ELECTRICO_POR_NRO_DE_CUENTA;
			logger.debug("Executing query PrintPlanoMapper.SQL_SELECT_SERVICIO_ELECTRICO_POR_NRO_DE_CUENTA");
			result= jdbcPostgreTemplate.queryForMap(sqlString,aNroCuenta);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_SERVICIO_ELECTRICO_POR_NRO_DE_CUENTA: {}",ee.getMessage());
			return null;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_SERVICIO_ELECTRICO_POR_NRO_DE_CUENTA: {}",e.getMessage());
			return null;
		}
		return result;
	}

	@Override
	public String getSysDate() {
		String sysDate="";
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_SYS_DATE;
			logger.debug("Executing query PrintPlanoMapper.SQL_SYS_DATE");
			sysDate = jdbcPostgreTemplate.queryForObject(sqlString,String.class);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query PrintPlanoMapper.SQL_SELECT_SERVICIO_ELECTRICO_POR_NRO_DE_CUENTA: {}",ee.getMessage());
			return "";
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_SYS_DATE: {}",e.getMessage());
			return "";
		}
		return sysDate;
	}

	@Override
	public String getNroOrdenH(String aCodigo_Interno_del_TdC, String aCodigo_Tipo_de_TdC) {
		String sysDate="";
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_OBTENER_NRO_ORDEN_H;
			logger.debug("Executing query PrintPlanoMapper.SQL_OBTENER_NRO_ORDEN_H");
			sysDate = jdbcPostgreTemplate.queryForObject(sqlString,String.class,aCodigo_Interno_del_TdC,aCodigo_Tipo_de_TdC);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query PrintPlanoMapper.SQL_OBTENER_NRO_ORDEN_H: {}",ee.getMessage());
			return null;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_OBTENER_NRO_ORDEN_H: {}",e.getMessage());
			return null;
		}
		return sysDate;
	}

	@Override
	public Long getMaxIdWorkFlow() {
		Long id=0L;
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_NEX_ID_WORK_FLOW;
			logger.debug("Executing query PrintPlanoMapper.SQL_MAX_ID_WORK_FLOW");
			id = jdbcPostgreTemplate.queryForObject(sqlString,Long.class);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query PrintPlanoMapper.SQL_MAX_ID_WORK_FLOW: {}",ee.getMessage());
			return 0L;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_MAX_ID_WORK_FLOW: {}",e.getMessage());
			return 0L;
		}
		return id;
	}

	//@Transactional
	@Override
	public boolean bfnCrearIdWorkflow(Long maxValue_sqworkflow) {
		boolean insert=false;
		int rows=0;
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_CREAR_ID_WORK_FLOW;
			logger.debug("Executing query PrintPlanoMapper.SQL_CREAR_ID_WORK_FLOW");
			rows = jdbcPostgreTemplate.update(sqlString,maxValue_sqworkflow);
			logger.info("Método bfnCrearIdWorkflow, filas afectadas: {}", rows);
			if(rows>0) {
				insert= true;
			}
		} catch (Exception ex) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_CREAR_ID_WORK_FLOW: {}",ex.getMessage());
			throw ex;
		}
		return insert;
	}

	@Override
	public Long getNextValSq_OrdenMantenimiento() {
		Long id=0L;
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_NEX_VAL_ORDEN_MANTENIMIENTO;
			logger.debug("Executing query PrintPlanoMapper.SQL_NEX_VAL_ORDEN_MANTENIMIENTO");
			id = jdbcPostgreTemplate.queryForObject(sqlString,Long.class);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query PrintPlanoMapper.SQL_NEX_VAL_ORDEN_MANTENIMIENTO: {}",ee.getMessage());
			return 0L;
		}catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_NEX_VAL_ORDEN_MANTENIMIENTO: {}",e.getMessage());
			return 0L;
		}
		return id;
	}

	@Override
	public Long getNextValSq_Orden() {
		Long id=0L;
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_NEX_VAL_ORDEN;
			logger.debug("Executing query PrintPlanoMapper.SQL_NEX_VAL_ORDEN");
			id = jdbcPostgreTemplate.queryForObject(sqlString,Long.class);
		} catch (EmptyResultDataAccessException ee) {
		 	logger.error("Error en Executing query PrintPlanoMapper.SQL_NEX_VAL_ORDEN: {}",ee.getMessage());
			return 0L;
		} catch (DataAccessException e) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_NEX_VAL_ORDEN: {}",e.getMessage());
			return 0L;
		}
		return id;
	}

	//@Transactional
	@Override
	public boolean bfnCrearOrdenGenerico(Long lNroOrdenH, Long iIdWorkflowH, Long iIdOrdenH, Long iIdMotivoH,
			Long iIdServicioH, Long iIdUsuarioH, Long iIdBuzonH) {
		
		boolean insert=false;
		int rows=0;
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_CREAR_ORDEN_GENERICO;
			logger.debug("Executing query PrintPlanoMapper.SQL_CREAR_ORDEN_GENERICO");
			rows = jdbcPostgreTemplate.update(sqlString,lNroOrdenH,iIdWorkflowH,iIdOrdenH,iIdMotivoH,iIdServicioH,iIdUsuarioH,iIdBuzonH);
			logger.info("Método bfnCrearOrdenGenerico, filas afectadas: {}", rows);
			if(rows>0) {
				insert= true;
			}
		} catch (Exception ex) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_CREAR_ORDEN_GENERICO: {}",ex.getMessage());
			throw ex;
		}
		return insert;
	}

//	@Transactional
	@Override
	public boolean bfnCrearOrdenMasiva(Long iIdOrdenH, Long iIdTrabajoH, Long iIdPrioridadH, Long iIdTemaH) {
		boolean insert=false;
		int rows=0;
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_CREAR_ORDEN_MASIVA;
			logger.debug("Executing query PrintPlanoMapper.SQL_CREAR_ORDEN_MASIVA");
			rows = jdbcPostgreTemplate.update(sqlString,iIdOrdenH,iIdTrabajoH,iIdPrioridadH,iIdTemaH);
			logger.info("Método bfnCrearOrdenMasiva, filas afectadas: {}", rows);
			if(rows>0) {
				insert= true;
			}
		} catch (Exception ex) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_CREAR_ORDEN_MASIVA: {}",ex.getMessage());
			throw ex;
		}
		return insert;
	}

//	@Transactional
	@Override
	public boolean bfnCrearOrdenDerivada(Long iIdOrdenH, Long iIdResponsableH, Long iIdResponsableH2) {
		boolean insert=false;
		int rows=0;
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_CREAR_ORDEN_DERIVADA;
			logger.debug("Executing query PrintPlanoMapper.SQL_CREAR_ORDEN_DERIVADA");
			rows = jdbcPostgreTemplate.update(sqlString,iIdOrdenH,iIdResponsableH,iIdResponsableH2);
			logger.info("Método bfnCrearOrdenDerivada, filas afectadas: {}", rows);
			if(rows>0) {
				insert= true;
			}
		} catch (Exception ex) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_CREAR_ORDEN_DERIVADA: {}",ex.getMessage());
			throw ex;
		}
		return insert;
	}

//	@Transactional
	@Override
	public boolean bfnActualizaNroOrdenLegacy(String cNroOrdenLegacyH, Long vIdOrdTransfer) {
		boolean update=false;
		int rows=0;
		try {
			String sqlString = PrintPlanoSCOMMapper.SQL_ACTUALIZAR_NRO_ORDEN_LEGACY;
			logger.debug("Executing query PrintPlanoMapper.SQL_ACTUALIZAR_NRO_ORDEN_LEGACY");
			rows = jdbcPostgreTemplate.update(sqlString,cNroOrdenLegacyH,vIdOrdTransfer);
			logger.info("Método bfnActualizaNroOrdenLegacy, filas afectadas: {}", rows);
			if(rows>0) {
				update= true;
			}
		} catch (Exception ex) {
			logger.error("Error en Executing query PrintPlanoMapper.SQL_ACTUALIZAR_NRO_ORDEN_LEGACY: {}",ex.getMessage());
			throw ex;
		}
		return update;
	}
	
	@Transactional
	@Override
	public int actualizaTablaTransferencia(String vCodOperacion, Integer vCodEstRecepError, String vObservacion,
			Long vIdOrdTransfer,Long vNroRecepciones) {
		String sqlString = PrintPlanoSCOMMapper.SQLPOSTGRESQL_UPDATE_FOR_ORDTRANSFER;
		int rows = 0;
		try {
			rows = jdbcPostgreTemplate.update(sqlString, vCodOperacion,vCodEstRecepError,vObservacion,vIdOrdTransfer);
			logger.info("Método actualizaTablaTransferencia, trasnfer, filas afectadas: {}", rows);
			
			sqlString = PrintPlanoSCOMMapper.SQLPOSTGRESQL_UPDATE_FOR_ORDTRANSFER_DET;
			rows = jdbcPostgreTemplate.update(sqlString,vCodOperacion,vIdOrdTransfer,vNroRecepciones);
			logger.info("Método actualizaTablaTransferencia, transfer det, filas afectadas: {}", rows);
			
		}catch (Exception ex)
		{
			logger.info("Error al actualizar en Método actualizaTablaTransferencia");
			logger.error("Exception: {}", ex.getMessage(), ex);
			throw ex;
		}
		return rows;	
	}

}
