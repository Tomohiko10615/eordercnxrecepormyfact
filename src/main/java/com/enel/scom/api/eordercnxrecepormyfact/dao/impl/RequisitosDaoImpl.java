package com.enel.scom.api.eordercnxrecepormyfact.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.eordercnxrecepormyfact.dao.IRequisitosDAO;
import com.enel.scom.api.eordercnxrecepormyfact.mapper.RequisitosMapper;
import com.enel.scom.api.eordercnxrecepormyfact.validator.dto.Requisito;
import com.enel.scom.api.eordercnxrecepormyfact.validator.exception.GenericDataAccessException;

@Repository
public class RequisitosDaoImpl implements IRequisitosDAO {

	@Autowired
	@Qualifier("postgreTemplate")
	private JdbcTemplate jdbcPostgreTemplate;

	@Autowired
	@Qualifier("orclTemplate")
	private JdbcTemplate jdbcORCLTemplate;

	@Override
	public List<Requisito> obtenerRequisitosObligatorios(Long idOrdTransfer, Long nroRecepciones) {
		List<Requisito> requisitosObligatorios = new ArrayList<>();
		try {
			String sqlString = RequisitosMapper.SQL_SELECT_REQUISITOS_OBLIGATORIOS;
			requisitosObligatorios = jdbcPostgreTemplate.query(sqlString, new RequisitosMapper(), idOrdTransfer,
					nroRecepciones);
		} catch (DataAccessException e) {
			throw new GenericDataAccessException("Error al obtener RequisitosObligatorios", e);
		}
		return requisitosObligatorios;
	}

	@Override
	public List<Requisito> obtenerRequisitosOpcionales(Long idOrdTransfer, Long nroRecepciones) {
		List<Requisito> requisitosOpcionales = new ArrayList<>();
		try {
			String sqlString = RequisitosMapper.SQL_SELECT_REQUISITOS_OPCIONALES;
			requisitosOpcionales = jdbcPostgreTemplate.query(sqlString, new RequisitosMapper(), idOrdTransfer,
					nroRecepciones);
		} catch (DataAccessException e) {
			throw new GenericDataAccessException("Error al obtener RequisitosOpcionales", e);
		}
		return requisitosOpcionales;
	}

	@Override
	public String obtenerTipoAcometida(Long idOrdTransfer, Long nroRecepciones) {
		String tipoAcometida = "";
		try {
			String sqlString = RequisitosMapper.SQL_SELECT_TIPO_ACOMETIDA;
			tipoAcometida = jdbcPostgreTemplate.queryForObject(sqlString, String.class, idOrdTransfer, nroRecepciones);
		} catch (DataAccessException e) {
			throw new GenericDataAccessException("Error al obtener el tipoAcometida", e);
		}
		return tipoAcometida;
	}

	@Override
	public List<Requisito> obtenerValoresValidos(Integer idTipoAcometida, int idFacTipoRequisito) {
		List<Requisito> valoresValidos = new ArrayList<>();
		try {
			String sqlString = RequisitosMapper.SQL_SELECT_VALORES_VALIDOS;
			valoresValidos = jdbcORCLTemplate.query(sqlString, new RequisitosMapper(), idTipoAcometida, idFacTipoRequisito, idTipoAcometida, idFacTipoRequisito);
		} catch (DataAccessException e) {
			throw new GenericDataAccessException("Error al obtener valoresValidos", e);
		}
		return valoresValidos;
	}
	
	@Override
	public Integer obtenerIdTipoAcometida(String codTipoAcometida) {
		Integer idTipoAcometida = null;
		try {
			String sqlString = RequisitosMapper.SQL_SELECT_FOR_ID_TIPO_ACOMETIDA;
			idTipoAcometida = jdbcORCLTemplate.queryForObject(sqlString, Integer.class, codTipoAcometida);
		} catch (DataAccessException e) {
			throw new GenericDataAccessException("Error al obtener idTipoAcometida", e);
		}
		return idTipoAcometida;
	}
}
