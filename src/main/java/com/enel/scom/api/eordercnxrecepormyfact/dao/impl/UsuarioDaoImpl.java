package com.enel.scom.api.eordercnxrecepormyfact.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.eordercnxrecepormyfact.bean.Usuario;
import com.enel.scom.api.eordercnxrecepormyfact.dao.IUsuarioDAO;
import com.enel.scom.api.eordercnxrecepormyfact.mapper.UsuarioMapper;

@Repository
public class UsuarioDaoImpl implements IUsuarioDAO{

	@Autowired
	@Qualifier("postgreTemplate")
	private JdbcTemplate jdbcPostgreTemplate;
	
	private static final Logger logger = LoggerFactory.getLogger(UsuarioDaoImpl.class);
	
	@Override
	public Long bfnObtenerIdUsuario(String gc_USER) {
		Long id=null;
		try {
			String sqlScript= UsuarioMapper.SQLPOSTGRESQL_SELECT_FOR_USUARIO;		
			logger.debug("Executing query UsuarioMapper.SQLPOSTGRESQL_SELECT_FOR_USUARIO");
			Usuario user = jdbcPostgreTemplate.queryForObject(sqlScript, new UsuarioMapper(),gc_USER);
			id= user.getId();
		} catch (DataAccessException e) {
			  logger.error("Error al obtener Usuario - Error: {}",e.getMessage());
			  return null;
		}
		return id;
	}

}
