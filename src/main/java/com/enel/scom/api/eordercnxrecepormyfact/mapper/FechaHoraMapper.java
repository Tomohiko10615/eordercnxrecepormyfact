package com.enel.scom.api.eordercnxrecepormyfact.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepormyfact.bean.Fecha;

public class FechaHoraMapper implements RowMapper<Fecha>{

	public static final String SQL_SELECT_FOR_FECHA_HORA_FORMATTED = "SELECT " +
			" TO_CHAR(?, ?) FECHA_HORA";

	public static final String SQL_SELECT_FOR_FECHA_HORA_HOY = "SELECT " +
		" TO_CHAR(NOW(), ?) FECHA_HORA";
	
	@Override
	public Fecha mapRow(ResultSet rs, int rowNum) throws SQLException {
		Fecha oFechaHora= new Fecha();
		oFechaHora.setGcFecha(rs.getString("FECHA_HORA"));
		return oFechaHora;
	}

}
