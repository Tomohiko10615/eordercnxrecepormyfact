package com.enel.scom.api.eordercnxrecepormyfact.mapper;

public class OthersMapper {

	public static final String SQL_SELECT_OBTENER_PATH =  "SELECT valor_alf " +
            "FROM SCHSCOM.COM_PARAMETROS " +
            "WHERE sistema = 'EORDER' " +
            "AND entidad = ? " +
            "AND codigo  = ? ";
	
	public static final String SQL_SELECT_OBTENER_PATH_ORM_IN = "SELECT PATH "+
			"FROM SCHSCOM.COM_PATH "+
			"WHERE Key = 'ORM_REPORTES_IN'";

	public static final String SQL_SELECT_OBTENER_TRANSFERENCIA = "SELECT VALOR_NUM " +
            "FROM SCHSCOM.COM_PARAMETROS " +
            "WHERE SISTEMA = 'EORDER' " +
            "AND ENTIDAD = 'ESTADO_TRANSFERENCIA' " +
            "AND CODIGO  = ? ";

	public static final String SQL_SELECT_OBTENER_ERRORES = "SELECT VALOR_ALF, DESCRIPCION " +
            "FROM SCHSCOM.COM_PARAMETROS " +
            "WHERE SISTEMA = 'EORDER' " +
            "AND ENTIDAD = 'ESTADO_ERRSYN' " +
            "AND CODIGO  = ? ";

	public static final String SQL_SELECT_OBTENER_CUADRILLA_GENERICA = "SELECT VALOR_ALF "
			+ "FROM SCHSCOM.COM_PARAMETROS "
			+ "WHERE SISTEMA = 'EORDER' "
			+ "AND ENTIDAD = 'CUADRILLA_GEN' "
			+ "AND CODIGO =   'CNX_ORM' "
			+ "AND ACTIVO = 'S'";

	public static final String SQL_SELECT_OBTENER_PARAMETROS = "SELECT valor_alf "
			+ "FROM SCHSCOM.COM_PARAMETROS "
			+ "WHERE sistema = 'EORDER' "
			+ "AND entidad = ? "
			+ "AND codigo  = ? ";

	public static final String SQL_SELECT_REGISTROS_PROCESAR = 	""
			+ " SELECT "
			+ " OT.ID_ORD_TRANSFER, "
			+ " OT.NRO_RECEPCIONES ,"
			+ " OT.COD_TIPO_ORDEN_EORDER,"
			+ " OT.NRO_ORDEN_LEGACY "
			+ " FROM SCHSCOM.EOR_ORD_TRANSFER OT, "
			+ " SCHSCOM.EOR_ORD_TRANSFER_DET OTD, "
			+ " SCHSCOM.ORD_ORDEN OO "
			+ " WHERE COD_ESTADO_ORDEN = ? AND "
			+ " COD_TIPO_ORDEN_EORDER in (?) "
			+ " AND OTD.ID_ORD_TRANSFER = OT.ID_ORD_TRANSFER "
			+ " AND OO.NRO_ORDEN = OT.NRO_ORDEN_LEGACY "
			+ " AND OTD.ACCION= 'RECEPCION' "
			+ " AND OTD.NRO_EVENTO = OT.NRO_RECEPCIONES"
			// + " and OT.id_ord_transfer in (83105202)" //borrar/comentar
			;

	public static final String SQL_SELECT_REGISTROS_PROCESAR_4J = ""
			+ " SELECT OT.ID_ORD_TRANSFER, OT.NRO_RECEPCIONES, OT.COD_TIPO_ORDEN_EORDER, OT.NRO_ORDEN_LEGACY"
			+ " FROM"
			+ " SCHSCOM.EOR_ORD_TRANSFER_DET OTD"
			+ " , SCHSCOM.EOR_ORD_TRANSFER OT"
			+ " WHERE 1 = 1"
			+ " and COD_ESTADO_ORDEN = ?"
			+ " AND COD_TIPO_ORDEN_EORDER in (?, ?)"
			+ " AND OTD.ID_ORD_TRANSFER = OT.ID_ORD_TRANSFER"
			+ " AND OTD.ACCION = 'RECEPCION'"
			+ " AND OTD.NRO_EVENTO = OT.NRO_RECEPCIONES"
			+ " AND OT.NRO_ORDEN_LEGACY not in ("
			+ " select NRO_ORDEN"
			+ " from SCHSCOM.ORD_ORDEN aa, wkf_workflow ww"
			+ " where aa.id_workflow = ww.id"
			+ " and id_tipo_orden = 2"
			+ " and id_motivo = 1240"
			+ " and ww.id_state in ('Emitida','Creada','SEmitida', 'SCreada')"
			+ " )"
			// + " and OT.id_ord_transfer in (83105202)" //borrar/comentar
			;

	public static final String SQLPOSTGRESQL_COUNT_MEDIDORES = "SELECT count(*) from med_medida_medidor AS mmm,       "
    		+ "med_medida_modelo AS mmm1       "
    		+ "JOIN med_fac_med_mod AS mfm ON mmm1.id = mfm.id_medida_modelo       "
    		+ "JOIN med_factor AS mf ON mfm.id_factor = mf.id       "
    		+ "WHERE mmm.id_componente = (select mc.id from med_componente mc, med_modelo mmod, med_marca mmar          "
    		+ "  where mc.id_modelo = mmod.id          "
    		+ "  and mmod.id_marca = mmar.id          "
    		+ "  and mc.nro_componente = ?          "
    		+ "  and mmod.cod_modelo = ?       "
    		+ "  and mmar.cod_marca = ? )        "
    		+ "    AND mmm1.id_modelo = (select mmod.id        "
    		+ "from med_modelo mmod, med_marca mmar          "
    		+ "  where mmod.cod_modelo = ?           "
    		+ "  and mmar.cod_marca = ?          "
    		+ "  and mmod.id_marca = mmar.id)       "
    		+ "    AND mf.cod_factor = ?       "
    		+ "    AND mmm.id_medida = mmm1.id_medida       "
    		+ "    AND NOT EXISTS (       "
    		+ "        SELECT 1       "
    		+ "        FROM med_medida_medidor AS mm       "
    		+ "        WHERE mm.id_componente = (select mc.id from med_componente mc, med_modelo mmod, med_marca mmar          "
    		+ "		  where mc.id_modelo = mmod.id          "
    		+ "		  and mmod.id_marca = mmar.id          "
    		+ "		  and mc.nro_componente = ?          "
    		+ "		  and mmod.cod_modelo = ?       "
    		+ "		  and mmar.cod_marca = ? )        "
    		+ "        AND NOT EXISTS (       "
    		+ "            SELECT 1       "
    		+ "            FROM med_medida_modelo AS mmm2       "
    		+ "            JOIN med_fac_med_mod AS mfm2 ON mmm2.id = mfm2.id_medida_modelo       "
    		+ "            JOIN med_factor AS mf2 ON mfm2.id_factor = mf2.id       "
    		+ "            WHERE mmm2.id_modelo = (select mmod.id        "
    		+ "			from med_modelo mmod, med_marca mmar          "
    		+ "			  where mmod.cod_modelo = ?           "
    		+ "			  and mmar.cod_marca = ?          "
    		+ "			  and mmod.id_marca = mmar.id)       "
    		+ "            AND mf2.cod_factor = ?       "
    		+ "            AND mm.id_medida = mmm2.id_medida ))";
	


}
