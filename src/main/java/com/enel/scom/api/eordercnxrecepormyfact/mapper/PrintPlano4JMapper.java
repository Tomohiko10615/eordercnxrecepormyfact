package com.enel.scom.api.eordercnxrecepormyfact.mapper;

public class PrintPlano4JMapper {
	/*Obtener datos de factibilidad - requisitos obligatorios*/
	public static final String SQL_SELECT_REQUISITOS_OBLIGATORIOS= "SELECT R.COD_REQUISITO AS Codigo_requisito, "
			+ " coalesce(O.VALOR_REQUISITO, '') AS Valor_requisito, "
			+ " R.ACTIVO as Activo "
			+ " FROM "
			+ " (SELECT  "
			+ " unnest(xpath('/item/codigo_requisito/text()', d.REG_XML))::text AS Codigo_requisito, "
			+ " unnest(xpath('/item/valor_requisito/text()', d.REG_XML))::text AS Valor_requisito "
			+ " FROM (SELECT  unnest(xpath('/Requisitos_Obligatorios/item', "
			+ " unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/factibilidad/Requisitos_Obligatorios', d.REG_XML)))) AS REG_XML "
			+ " FROM SCHSCOM.EOR_ORD_TRANSFER_DET d  "
			+ " WHERE d.id_ord_transfer = ? AND "
			+ " accion = 'RECEPCION'  "
			+ " AND nro_evento = ? "
			+ " )d "
			// Correctivo R.I. 09/08/2023 INICIO
			+ " ) O, TMP_ORM_FAC_REQUISITO R "
			// Correctivo R.I. 09/08/2023 FIN
			+ " WHERE O.CODIGO_REQUISITO = RTRIM(R.COD_REQUISITO) "
			+ " AND R.EVALUAR = 'S'";

	/*Obtener datos de factibilidad - requisitos opcionales*/
	public static final String SQL_SELECT_REQUISITOS_OPCIONALES= "SELECT R.COD_REQUISITO AS Codigo_requisito,"
			+ "    coalesce(O.VALOR_REQUISITO, '') AS Valor_requisito,"
			+ "    R.ACTIVO as Activo"
			+ "    FROM "
			+ "   ("
			+ "		SELECT  "
			+ "			unnest(xpath('/item/codigo_requisito/text()', d.REG_XML))::text AS Codigo_requisito,  "
			+ "			unnest(xpath('/item/valor_requisito/text()', d.REG_XML))::text AS Valor_requisito"
			+ "			FROM (SELECT  unnest(xpath('/ListaRequisitosOpcionales/item', "
			+ "			unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/factibilidad/OtrosRequisitos/ListaRequisitosOpcionales', d.REG_XML)))) AS REG_XML  "
			+ "			FROM SCHSCOM.EOR_ORD_TRANSFER_DET d"
			+ "    	 WHERE d.id_ord_transfer = ?  "
			+ "    	 AND accion = 'RECEPCION' "
			+ "		 AND nro_evento = ? "
			+ "    ) d"
			// Correctivo R.I. 09/08/2023 INICIO
			+ "    )O, TMP_ORM_FAC_REQUISITO R "
			// Correctivo R.I. 09/08/2023 FIN
			+ "    WHERE O.CODIGO_REQUISITO = RTRIM(R.COD_REQUISITO)"
			+ "    AND R.EVALUAR = 'S'";
	
	/*Obtener datos de factibilidad - informacion adicional*/
	public static final String SQL_SELECT_INFORMACIÓN_ADICIONAL="SELECT"
			+ "    		unnest(xpath('/OtrosRequisitos/Suministro_Izquierdo/text()', d.REG_XML))::text AS Suministro_Izquierdo,"
			+ "    		unnest(xpath('/OtrosRequisitos/Suministro_Derecho/text()', d.REG_XML))::text AS Suministro_Derecho,  "
			+ "    		unnest(xpath('/OtrosRequisitos/Latitud_Foto/text()', d.REG_XML))::text AS Latitud_Foto,  "
			+ "    		unnest(xpath('/OtrosRequisitos/Longitud_Foto/text()', d.REG_XML))::text AS Longitud_Foto  "
			+ "			FROM (SELECT unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/factibilidad/OtrosRequisitos', o.REG_XML)) AS REG_XML"
			+ "			FROM SCHSCOM.EOR_ORD_TRANSFER_DET o"
			+ "            WHERE o.id_ord_transfer = ?  "
			+ "         AND accion = 'RECEPCION' "
			+ "			AND nro_evento = ?"
			+ "			) d ";

	/*Obtener datos de factibilidad - suministros*/
	public static final String SQL_SELECT_SUMINISTROS = "SELECT "
			+ "		 unnest(xpath('/suministro/text()', d.REG_XML))::text AS Suministro"
			+ "		 FROM (SELECT  unnest(xpath('/Otros_Suministros/suministro', "
			+ "				  unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/factibilidad/OtrosRequisitos/Otros_Suministros', O.REG_XML)))) AS REG_XML  "
			+ "				  FROM SCHSCOM.EOR_ORD_TRANSFER_DET O "
			+ "		       WHERE o.id_ord_transfer = ? "
			+ "			   AND accion = 'RECEPCION' "
			+ "			   AND nro_evento = ?"
			+ "			   ) d";

	/* Obtener Datos Clave de Procesos y Datos Comunes de Procesos */
	public static final String SQL_SELECT_DATOS_CLAVES = "SELECT  "
			+ "	 	unnest(xpath('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/Codigo_Distribuidora/text()', d.REG_XML))::text AS Codigo_Distribuidora,"
			+ "		unnest(xpath('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/Codigo_Sistema_Externo_de_Origen/text()', d.REG_XML))::text AS Codigo_Sistema_Externo_Origen,"
			+ "	 	unnest(xpath('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/Codigo_Tipo_de_TdC/text()', d.REG_XML))::text AS Codigo_Tipo_de_TdC,"
			+ "	 	unnest(xpath('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/Codigo_Externo_del_TdC/text()', d.REG_XML))::text AS Codigo_Externo_del_TdC,"
			+ "	 	unnest(xpath('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/Codigo_Interno_del_TdC/text()', d.REG_XML))::text AS Codigo_Interno_del_TdC,"
			+ "	 	unnest(xpath('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/Codigo_Proceso/text()', d.REG_XML))::text AS Codigo_Proceso,"
			+ "	 	unnest(xpath('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/Codigo_SubProceso/text()', d.REG_XML))::text AS Codigo_SubProceso,"
			+ "	 	unnest(xpath('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/TDC_creado_en_Eorder/text()', d.REG_XML))::text AS TDC_Creado_Eorder,"
			+ "		unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Codigo_Resultado/text()', d.REG_XML))::text AS Codigo_Resultado,"
			+ "		unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Codigo_Causal_Resultado/text()', d.REG_XML))::text AS Codigo_Causal_Resultado,"
			+ "		unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Codigo_Nota_Codificada/text()', d.REG_XML))::text AS Codigo_Nota_Codificada,"
			+ "		unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Inicio_TdC/text()', d.REG_XML))::text AS Inicio_TdC,"
			+ "		unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Fin_TdC/text()', d.REG_XML))::text AS Fin_TdC,"
			+ "		unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Duracion_Ejecucion/text()', d.REG_XML))::text AS Duracion_Ejecucion,"
			+ "		unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Codigo_Cuadrilla/text()', d.REG_XML))::text AS Codigo_Cuadrilla"
			+ "		FROM SCHSCOM.EOR_ORD_TRANSFER_DET d  "
			+ "		WHERE d.id_ord_transfer = ?  "
			+ "		AND accion = 'RECEPCION'  "
			+ "		AND nro_evento = ?";

	/* Colección Operaciones  */
	public static final String SQL_SELECT_OPERACIONES = "SELECT  "
			+ "	 	unnest(xpath('/item/Codigo_Operacion/text()', o.REG_XML))::text AS Codigo_Operacion,"
			+ "	 	unnest(xpath('/item/Codigo_Resultado/text()', o.REG_XML))::text AS Codigo_Resultado,"
			+ "	 	unnest(xpath('/item/Codigo_Causal_Resultado/text()', o.REG_XML))::text AS Codigo_Causal_Resultado,"
			+ "	 	unnest(xpath('/item/Codigo_Nota_Codificada/text()', o.REG_XML))::text AS Codigo_Nota_Codificada,"
			+ "	 	unnest(xpath('/item/Inicio_Operacion/text()', o.REG_XML))::text AS Inicio_Operacion,"
			+ "	 	unnest(xpath('/item/Fin_Operacion/text()', o.REG_XML))::text AS Fin_Operacion,"
			+ "	 	unnest(xpath('/item/Duracion_Operacion/text()', o.REG_XML))::text AS Duracion_Operacion,"
			+ "	 	replace(replace(replace(unnest(xpath('/item/Notas_Operacion/text()', o.REG_XML))::text, '#' , ' '),chr(13),' '),chr(10),' ') AS Notas_Operacion"
			+ "	 	FROM"
			+ "        (SELECT  unnest(xpath('/operaciones/item', "
			+ "				  unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/operaciones', d.REG_XML)))) AS REG_XML  "
			+ "		 FROM SCHSCOM.EOR_ORD_TRANSFER_DET d"
			+ "		 WHERE d.id_ord_transfer = ?  "
			+ "		 AND accion = 'RECEPCION'  "
			+ "		 AND nro_evento = ?"
			+ "		 ) o";
	
	/* Colección Tareas  */
	public static final String SQL_SELECT_TAREAS = "SELECT  "
			+ "	 	 unnest(xpath('/item/nroTarea/text()', o.REG_XML))::text AS Nro_Tarea,"
			+ "	 	 unnest(xpath('/item/identificador/text()', o.REG_XML))::text AS Identificador,"
			+ "	 	 unnest(xpath('/item/cantidad/text()', o.REG_XML))::text AS Cantidad,"
			+ "	 	 unnest(xpath('/item/ejecutado/text()', o.REG_XML))::text AS Inicio_Operacion"
			+ "		 FROM(SELECT  unnest(xpath('/tareasORM/item', "
			+ "				  unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/ORM/tareasORM', d.REG_XML)))) AS REG_XML  "
			+ "		 FROM SCHSCOM.EOR_ORD_TRANSFER_DET d"
			+ "		 WHERE d.id_ord_transfer = ? "
			+ "		 AND accion = 'RECEPCION'  "
			+ "		 AND nro_evento = ?"
			+ "		 )o";
	
	/* Colección Recursos */
	/* Utilizado para obtener nombre de tecnico*/
	public static final String SQL_SELECT_NOMBRES_DE_TECNICO = "SELECT  "
			+ "	 	   unnest(xpath('/item/Nombre_Operador/text()', o.REG_XML))::text AS NombreOperador,"
			+ "	 	   unnest(xpath('/item/Apellido_Operador/text()', o.REG_XML))::text AS ApellidoOperador,"
			+ "	 	   unnest(xpath('/item/Matricula_Operador/text()', o.REG_XML))::text AS CodEjecutor"
			+ "		 from(SELECT  unnest(xpath('/recursos/item', "
			+ "				  unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/recursos', d.REG_XML)))) AS REG_XML  "
			+ "		 FROM SCHSCOM.EOR_ORD_TRANSFER_DET d"
			+ "		 WHERE d.id_ord_transfer = ?  AND"
			+ "		 accion = 'RECEPCION'  "
			+ "		 AND nro_evento = ? "
			+ "		 )o";

	/* Colección Medidores */
	public static final String SQL_SELECT_COLECCION_MEDIDORES = "SELECT  "
			+ "	 	   unnest(xpath('/item/numeroMedidor/text()', o.REG_XML))::text AS numeroMedidor,"
			+ "	 	   unnest(xpath('/item/marcaMedidor/text()', o.REG_XML))::text AS marcaMedidor,"
			+ "	 	   unnest(xpath('/item/modeloMedidor/text()', o.REG_XML))::text AS modeloMedidor,"
			+ "	 	   unnest(xpath('/item/numeroFabricaMedidor/text()', o.REG_XML))::text AS numeroFabricaMedidor,"
			+ "	 	   unnest(xpath('/item/tipoMedidor/text()', o.REG_XML))::text AS tipoMedidor,"
			+ "	 	   unnest(xpath('/item/tecnologiaMedidor/text()', o.REG_XML))::text AS tecnologiaMedidor,"
			+ "	 	   unnest(xpath('/item/faseMedidor/text()', o.REG_XML))::text AS faseMedidor,"
			+ "	 	   unnest(xpath('/item/factorMedidor/text()', o.REG_XML))::text AS factorMedidor,"
			+ "	 	   unnest(xpath('/item/propiedadMedidor/text()', o.REG_XML))::text AS propiedadMedidor,"
			+ "	 	   unnest(xpath('/item/accionMedidor/text()', o.REG_XML))::text AS accionMedidor,"
			+ "	 	   unnest(xpath('/item/tipoMedicion/text()', o.REG_XML))::text AS tipoMedicion,"
			+ "	 	   unnest(xpath('/item/ubicacionMedidor/text()', o.REG_XML))::text AS ubicacionMedidor,"
			+ "	 	   unnest(xpath('/item/telemedido/text()', o.REG_XML))::text AS telemedido,"
			+ "	 	   unnest(xpath('/item/ElementoBorneroPrueba/text()', o.REG_XML))::text AS ElementoBorneroPrueba,"
			+ "	 	   unnest(xpath('/item/AnoFabricacion/text()', o.REG_XML))::text AS AnoFabricacion,"
			+ "	 	   unnest(xpath('/item/fechaInstalacion/text()', o.REG_XML))::text AS fechaInstalacion"
			+ "		 FROM"
			+ "		 (SELECT  unnest(xpath('/medidores/item', "
			+ "				  unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/medidores', d.REG_XML)))) AS REG_XML  "
			+ "		 FROM SCHSCOM.EOR_ORD_TRANSFER_DET d"
			+ "		 WHERE d.id_ord_transfer = ?  "
			+ "		 AND accion = 'RECEPCION'  "
			+ "		 AND nro_evento = ?"
			+ "		 )o";

	/* Colección Lecturas */
	public static final String SQL_SELECT_COLECCION_LECTURAS = " select"
			+ "		   unnest(xpath('/item/tipoLectura/text()', d.valuey))::text AS tipoLectura, "
			+ "	 	   unnest(xpath('/item/horarioLectura/text()', d.valuey))::text AS horariolectura, "
			+ "	 	   unnest(xpath('/item/estadoLeido/text()', d.valuey))::text AS estadoLeido, "
			+ "	 	   unnest(xpath('/item/fechaLectura/text()', d.valuey))::text AS fechalectura, "
			+ "	 	   unnest(xpath('/item/numeroMedidor/text()', d.valueb))::text AS numeroMedidor, "
			+ "	 	   unnest(xpath('/item/accionMedidor/text()', d.valueb))::text AS accionMedidor, "
			+ "	 	   unnest(xpath('/item/marcaMedidor/text()', d.valueb))::text AS marcaMedidor, "
			+ "	 	   unnest(xpath('/item/modeloMedidor/text()', d.valueb))::text AS modeloMedidor "
			+ "            FROM (SELECT unnest(xpath('/lecturas/item',"
			+ "                                        unnest(xpath('/item/lecturas',"
			+ "                                            unnest(xpath('/medidores/item',"
			+ "                                                unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/medidores',det.REG_XML))"
			+ "                                            ))"
			+ "                                        ))"
			+ "                                )) as valuey ,"
			+ "                                            unnest(xpath('/medidores/item',"
			+ "                                                unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/medidores',det.REG_XML))"
			+ "                                            )) as valueb"
			+ "         FROM SCHSCOM.EOR_ORD_TRANSFER_DET det"
			+ "		 WHERE det.id_ord_transfer = ?  "
			+ "		 and accion = 'RECEPCION'  "
			+ "		 AND nro_evento = ? "
			+ "            ) d";
		

	/* Colección Sellos */
	public static final String SQL_SELECT_COLECCION_SELLOS = "SELECT  "
			+ "	 	   unnest(xpath('/item/numeroSello/text()', o.REG_XML))::text AS numeroSello,"
			+ "	 	   unnest(xpath('/item/ubicacionSello/text()', o.REG_XML))::text AS ubicacionSello,"
			+ "	 	   unnest(xpath('/item/serieSello/text()', o.REG_XML))::text AS serieSello,"
			+ "	 	   unnest(xpath('/item/tipoSello/text()', o.REG_XML))::text AS tipoSello,"
			+ "	 	   unnest(xpath('/item/colorSello/text()', o.REG_XML))::text AS colorSello,"
			+ "	 	   unnest(xpath('/item/estadoSello/text()', o.REG_XML))::text AS estadoSello,"
			+ "	 	   unnest(xpath('/item/accionSello/text()', o.REG_XML))::text AS accionSello"
			+ "	 	 FROM"
			+ "		 (SELECT  unnest(xpath('/sellos/item', "
			+ "				  unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/sellos', d.REG_XML)))) AS REG_XML  "
			+ "		 FROM SCHSCOM.EOR_ORD_TRANSFER_DET d"
			+ "		 WHERE d.id_ord_transfer = ?  "
			+ "		 AND accion = 'RECEPCION'  "
			+ "		 AND nro_evento = ? "
			+ "		 )o";

	/*Datos de Ejecución de órdenes de Mantenimiento y Factibilidad*/
	/* Obtener Datos de Acta de Revisión */
	public static final String SQL_SELECT_DATOS_ACTA_DE_REVISION = "SELECT  "
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/ID_numero_acta_revision/text()', d.REG_XML))::text AS ID_numero_acta_revision,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/ID_Cod_Cliente_Vecino_izq_encontrado/text()', d.REG_XML))::text AS ID_Cod_Cli_Vec_izq_enc,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/ID_Cod_Cliente_Vecino_Derecho_encontrado/text()', d.REG_XML))::text AS ID_Cod_Cli_Vec_Der_enc,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/Direccion_Terreno/text()', d.REG_XML))::text AS Direccion_Terreno,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/Localizacion_Terreno/text()', d.REG_XML))::text AS Localizacion_Terreno,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/ID_Cod_Municipio/text()', d.REG_XML))::text AS ID_Cod_Municipio,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/Nombre_Persona_Atendio/text()', d.REG_XML))::text AS Nombre_Persona_Atendio,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/Cedula_Atendio/text()', d.REG_XML))::text AS Cedula_Atendio,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/Calidad_Atendio/text()', d.REG_XML))::text AS Calidad_Atendio,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/Cedula_Tecnico/text()', d.REG_XML))::text AS Cedula_Tecnico,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/Cantidad_Medidores_No_Instalados/text()', d.REG_XML))::text AS Cant_Medid_No_Instalados,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/Codigo_Description_deficienza/text()', d.REG_XML))::text AS Codigo_Description_deficienza,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/Email_de_contacto_con_cliente/text()', d.REG_XML))::text AS Email_de_contacto_con_cliente,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/Factibilidad_del_Servicio/text()', d.REG_XML))::text AS Factibilidad_del_Servicio,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/EXISTE_RED/text()', d.REG_XML))::text AS EXISTE_RED,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/TIPO_DE_RED/text()', d.REG_XML))::text AS TIPO_DE_RED,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/Tipo_de_Empalme/text()', d.REG_XML))::text AS Tipo_de_Empalme,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/Capacidad_del_Empalme_Encontrado/text()', d.REG_XML))::text AS Capacid_Empalme_Encontrado,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/Proteccion_de_Empalme/text()', d.REG_XML))::text AS Proteccion_de_Empalme,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/Tipo_de_caja/text()', d.REG_XML))::text AS Tipo_de_caja,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/Tipo_de_Conexion/text()', d.REG_XML))::text AS Tipo_de_Conexion,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/Potencia_Solicitada/text()', d.REG_XML))::text AS Potencia_Solicitada,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/Tarifa_Solicitada/text()', d.REG_XML))::text AS Tarifa_Solicitada,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/Numero_Medidor_Cliente_Vecino_Derecho/text()', d.REG_XML))::text AS Nro_Medid_Cli_Vec_Derecho,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/ID_Cod_Marca_Medidor_Vecino_Derecho/text()', d.REG_XML))::text AS ID_Cod_Marca_Medid_Vec_Der,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/Numero_Medidor_Cliente_Vecino_Izquierdo/text()', d.REG_XML))::text AS Nro_Medid_Cli_Vec_Izquierdo,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/ID_Cod_Marca_Medidor_Vecino_Izquierdo/text()', d.REG_XML))::text AS ID_Cod_Marca_Medid_Vec_Izq,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/numero_de_serie_encontrado/text()', d.REG_XML))::text AS numero_de_serie_encontrado,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/marca_encontrado/text()', d.REG_XML))::text AS marca_encontrado,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/estado_encontrado/text()', d.REG_XML))::text AS estado_encontrado,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/factor_encontrado/text()', d.REG_XML))::text AS factor_encontrado,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/amperaje_encontrado/text()', d.REG_XML))::text AS amperaje_encontrado,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/tipo_modelo_encontrado/text()', d.REG_XML))::text AS tipo_modelo_encontrado,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/fase_encontrado/text()', d.REG_XML))::text AS fase_encontrado,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/constantes_encontrado/text()', d.REG_XML))::text AS constantes_encontrado,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/sellos_candados_1/text()', d.REG_XML))::text AS sellos_candados_1,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/sellos_bornera_1/text()', d.REG_XML))::text AS sellos_bornera_1,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/sellos_candados_2/text()', d.REG_XML))::text AS sellos_candados_2,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/sellos_bornera_2/text()', d.REG_XML))::text AS sellos_bornera_2,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/numero_de_serie_instalado/text()', d.REG_XML))::text AS numero_de_serie_instalado,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/marca_instalado/text()', d.REG_XML))::text AS marca_instalado,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/estado_instalado/text()', d.REG_XML))::text AS estado_instalado,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/factor_instalado/text()', d.REG_XML))::text AS factor_instalado,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/Capacidad_instalado/text()', d.REG_XML))::text AS Capacidad_instalado,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/amperaje_isntalado/text()', d.REG_XML))::text AS amperaje_isntalado,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/tipo_modelo_instalado/text()', d.REG_XML))::text AS tipo_modelo_instalado,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/fase_instalado/text()', d.REG_XML))::text AS fase_instalado,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/constantes_instalado/text()', d.REG_XML))::text AS constantes_instalado,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/interruptor_termomagnetico_marca/text()', d.REG_XML))::text AS interrup_termomag_marca,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/interruptor_termomagnetico_fase/text()', d.REG_XML))::text AS interrup_termomag_fase,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/interruptor_termomagnetico_amperaje/text()', d.REG_XML))::text AS interrup_termomag_amperaje,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/carga_tomada_r/text()', d.REG_XML))::text AS carga_tomada_r,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/carga_tomada_s/text()', d.REG_XML))::text AS carga_tomada_s,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/carga_tomada_t/text()', d.REG_XML))::text AS carga_tomada_t,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/prueba_de_aislamiento/text()', d.REG_XML))::text AS prueba_de_aislamiento,"
			+ "	 	   unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/revision/valor/text()', d.REG_XML))::text AS valor"
			+ "		FROM SCHSCOM.EOR_ORD_TRANSFER_DET d"
			+ "		WHERE d.id_ord_transfer = ? "
			+ "		AND accion = 'RECEPCION'  "
			+ "		AND nro_evento = ?";

	public static final String SQL_SELECT_MOTIVO_TEMA_Y_TRABAJO = "SELECT "
			+ "		unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/ORM/motivo/text()', d.REG_XML))::text AS motivo,"
			+ "		unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/ORM/tema/text()', d.REG_XML))::text AS tema,"
			+ "		unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/ORM/trabajo/text()', d.REG_XML))::text AS trabajo,"
			+ "		unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/ORM/Codigo_Cliente/text()', d.REG_XML))::text AS nro_cuenta  "
			+ "		FROM SCHSCOM.EOR_ORD_TRANSFER_DET  d "
			+ "		WHERE d.id_ord_transfer = ? "
			+ "		AND accion = 'RECEPCION' "
			+ "		AND nro_evento = ? ";
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static final String SQL_VERIFY_FORMATTED_FECHA = "SELECT CAST(TO_DATE(?,?) AS TEXT) AS FECHA";

	public static final String SQL_SEARCH_IF_ORM_EXIST_ = "SELECT COUNT(1)"
			+ "	FROM ORD_ORDEN ORD, ORM_ORDEN ORM, WKF_WORKFLOW WORD"
			+ "	WHERE"
			+ "	ORD.ID_ORDEN = ORM.ID_ORDEN"
			+ "	AND ORD.NRO_ORDEN = ? "
			+ "	AND ORD.ID_WORKFLOW =  WORD.ID_WORKFLOW"
			+ "	AND WORD.ID_STATE IN ('Creada','Emitida','SCreada','SEmitida') ";

	public static final String SQL_OBTENER_NUMERO_DE_CUENTA = "SELECT NRO_CUENTA "
			+ " FROM ORD_ORDEN ORD, ORM_ORDEN ORM, SRV_ELECTRICO SE, "
			+ " NUC_SERVICIO NS,  NUC_CUENTA NC "
			+ " WHERE ORD.ID_ORDEN = ORM.ID_ORDEN "
			+ " AND ORD.ID_SERVICIO = SE.ID_SERVICIO "
			+ " AND SE.ID_SERVICIO = NS.ID_SERVICIO "
			+ " AND NS.TIPO= 'ELECTRICO' "
			+ " AND NS.ID_CUENTA = NC.ID_CUENTA "
			+ " AND ORD.NRO_ORDEN =  ? ";

	public static final String SQL_COUNT_REQ_CAMBIO_MED = " SELECT COUNT(*) "
			+ "	FROM ORM_TAREA "
			+ "	WHERE  CODE = ?"
			+ "	AND ESTADO = 'S' "
			+ "	AND REQ_CAMBIO_MEDIDOR = 'S' ";

	public static final String SQL_GET_COD_EJE_GEN = "SELECT EJE.COD_EJECUTOR, "
			+ "	TRIM(NP.NOMBRE) || ' ' || TRIM(NP.APELLIDO_PAT) || ' ' || TRIM(NP.APELLIDO_MAT) NOMBRE "
			+ "	FROM COM_CONTRATISTA CONT, SEG_MODULO MODU, COM_CONT_MOD CMOD, COM_CUADRILLA CUAD, COM_COORD_CUADRILLA CORD, COM_EJECUTOR EJE, NUC_PERSONA NP "
			+ "	WHERE CONT.ID_CONTRATISTA = CUAD.ID_CONTRATISTA "
			+ "	AND  CONT.ID_CONTRATISTA = CMOD.ID_CONTRATISTA "
			+ "	AND CMOD.ID_MODULO = MODU.ID_MODULO "
			+ "	AND MODU.COD_MODULO ='ORM' "
			+ "	AND CUAD.ID_COORD_CUADRILLA = CORD.ID_COORD_CUADRILLA "
			+ "	AND CORD.COD_COORD_CUADRILLA = EJE.COD_EJECUTOR "
			+ "	AND CUAD.ACTIVE ='S' "
			+ "	AND CORD.ACTIVO = 'S' "
			+ "	AND EJE.ACTIVO='S' "
			+ "	AND CORD.ID_PERSONA = NP.ID_PERSONA "
			+ "	AND CUAD.COD_CUADRILLA = ?";

	public static final String SQL_COUNT_VALIDAR_COMP_RET = "SELECT COUNT(1) FROM "
			+ "	SCHSCOM.MED_MARCA MAR, SCHSCOM.MED_MODELO MOD, SCHSCOM.MED_COMPONENTE COM, SCHSCOM.CLIENTE C  "
			+ "   WHERE MAR.ID = MOD.ID_MARCA  "
			+ "   AND MOD.ID = COM.ID_MODELO     "
			+ "   AND COM.TYPE_UBICACION = 'com.synapsis.synergia.nucleo.domain.interfaces.servicioelectrico.ServicioElectrico'    "
			+ "   AND C.NRO_CUENTA = ? "
			+ "   AND MAR.COD_MARCA = ? "
			+ "   AND MOD.COD_MODELO = ? "
			+ "   AND COM.NRO_COMPONENTE = ? "
			+ "   AND COM.ID_EST_COMPONENTE = 18 ";

	public static final String SQL_COUNT_VALIDAR_COMP_INSTALL = "  SELECT COUNT(1) "
			+ "   FROM SCHSCOM.MED_MARCA MAR, SCHSCOM.MED_MODELO MOD, SCHSCOM.MED_COMPONENTE COM "
			+ "   WHERE MAR.ID = MOD.ID_MARCA "
			+ "   AND MOD.ID = COM.ID_MODELO "
			+ "   AND MAR.COD_MARCA = ? "
			+ "   AND MOD.COD_MODELO = ? "
			+ "   AND COM.NRO_COMPONENTE = ? "
			+ "   AND COM.ID_EST_COMPONENTE = 17 ";

	public static final String SQL_OBTENER_FECHA_DDMMYYY = "SELECT TO_CHAR(TO_DATE(?,'YYYY-MM-DD'),'DD/MM/YYYY')";

	public static final String SQL_VALIDAR_MEDIDA_MEDIDOR = "SELECT COUNT(1)  "
			+ "	 FROM SCHSCOM.MED_MARCA MAR, SCHSCOM.MED_MODELO MOD, SCHSCOM.MED_COMPONENTE COM, SCHSCOM.MED_MEDIDA MED, SCHSCOM.MED_MEDIDA_MEDIDOR MMED   "
			+ "	 WHERE MAR.ID = MOD.ID_MARCA   "
			+ "	 AND MOD.ID = COM.ID_MODELO   "
			+ "	 AND COM.ID = MMED.ID_COMPONENTE   "
			+ "	 AND MED.ID = MMED.ID_MEDIDA   "
			+ "	 AND MAR.COD_MARCA = ?  "
			+ "	 AND MOD.COD_MODELO = ? "
			+ "	 AND COM.NRO_COMPONENTE = ? "
			+ "	 AND MED.COD_MEDIDA = ?";

	public static final String SQL_OBTENER_COD_CONTRATISTA = "SELECT CONT.COD_CONTRATISTA  "
			+ "	FROM COM_CUADRILLA CUAD, COM_CONTRATISTA CONT, COM_CONT_MOD CMOD, SEG_MODULO MOD"
			+ "	WHERE CUAD.ID_CONTRATISTA = CONT.ID_CONTRATISTA"
			+ "	AND CUAD.COD_CUADRILLA = ? "
			+ "	AND CONT.ID_CONTRATISTA = CMOD.ID_CONTRATISTA  "
			+ "	AND CMOD.ID_MODULO = MOD.ID_MODULO "
			+ "	AND MOD.COD_MODULO = 'ORM'   "
			+ "	AND CUAD.ACTIVE = 'S' "
			+ "	AND CONT.ACTIVO = 'S' ";

	public static final String SQL_SELECT_GET_FECHA_HORA_EJE = " SELECT "
			+ "	TO_CHAR(TO_DATE( ? ,'YYYY-MM-DD HH24:MI:SS'),'DD/MM/YYYY') as fechacorta,"
			+ "	TO_CHAR(TO_TIMESTAMP( ? ,'YYYY-MM-DD HH24:MI:SS'),'HH24') as hora,"
			+ "	TO_CHAR(TO_TIMESTAMP( ? ,'YYYY-MM-DD HH24:MI:SS'),'MI') as minutes";

	public static final String SQL_OBTENER_ID_MOTIVO = "	SELECT DISTINCT a.id_motivo "
			+ " FROM   orm_motivo a, ord_motivo_creacion b"
			+ " WHERE  a.id_motivo=id_motivo"
			+ " AND b.cod_motivo = ?";

	public static final String SQL_SELECT_ID_RESPONSABLE_Y_BUZON = "SELECT DISTINCT b.id_buzon_default as iIdBuzonH, b.id as iIdResponsableH"
			+ " FROM  seg_rol a,"
			+ " ord_responsable b,"
			+ "	ord_buzon c"
			+ " WHERE  a.id_rol=b.id_object"
			+ "	AND b.id_buzon_default = c.id_buzon"
			+ " AND a.code = ?"
			+ " AND b.tipo_responsable= ? ";

	public static final String SQL_OBTENER_ID_TRABAJO_H = "SELECT DISTINCT a.id_trabajo  "
			+ " FROM   orm_trabajo a "
			+ "	WHERE  a.code = ?";

	public static final String SQL_OBTENER_ID_PRIORIDAD_H = " SELECT DISTINCT a.id "
			+ " FROM   org_prioridad a"
			+ " WHERE  a.cod_prioridad = ?";

	public static final String SQL_OBTENER_ID_TEMA_H = " SELECT DISTINCT a.id"
			+ " FROM   orm_tema a"
			+ "	WHERE  a.code = ?";

	public static final String SQL_SELECT_SERVICIO_ELECTRICO_POR_NRO_DE_CUENTA = " SELECT nc.nro_cuenta, ns.id_servicio"
			+ " FROM   nuc_cuenta nc, nuc_servicio ns, srv_electrico se"
			+ " WHERE"
			+ "	nc.nro_cuenta = ?"
			+ " AND nc.id_cuenta= ns.id_cuenta "
			+ " AND ns.tipo = 'ELECTRICO' "
			+ " AND ns.id_servicio=se.id_servicio"
			+ " AND se.id_estado in(0,1,4,5) ";

	public static final String SQL_SYS_DATE = "SELECT to_char(now(),'dd/mm/yyyy')";

	public static final String SQL_OBTENER_NRO_ORDEN_H = " SELECT TRIM(coalesce(NRO_ORDEN_LEGACY,'0')) "
			+ "	FROM SCHSCOM.EOR_ORD_TRANSFER "
			+ "	WHERE NRO_ORDEN_EORDER = ?::INTEGER "
			+ "	AND  COD_TIPO_ORDEN_EORDER = (TRIM(?)) ";
			//+ "	AND ROWNUM = 1 ";

	public static final String SQL_NEX_ID_WORK_FLOW = "SELECT sqworkflow.NEXTval FROM   dual";

	public static final String SQL_CREAR_ID_WORK_FLOW = ""
			+ " INSERT INTO wkf_workflow"
			+ " (id_workflow,"
			+ " id_descriptor,"
			+ " version,"
			+ " id_state,"
			+ " has_parent,"
			+ " id_empresa,"
			+ " parent_type,"
			+ " id_old_state)"
			+ " VALUES"
			+ " ( ? ,"
			+ " 'ordenmantenimientoWF',"
			+ "  0,"
			+ " 'Creada',"
			+ " 'S',"
			+ "  ? ,"
			+ " 'com.synapsis.synergia.orm.domain.OrdenMantenimiento',"
			+ " 'Creada');";

	public static final String SQL_NEX_VAL_ORDEN_MANTENIMIENTO = "SELECT  sqordenmantenimiento.NEXTval FROM dual";

	public static final String SQL_NEX_VAL_ORDEN = "SELECT sqorden.nextval FROM dual";

	public static final String SQL_CREAR_ORDEN_GENERICO = "INSERT INTO ord_orden"
			+ " (nro_orden,"
			+ "  id_workflow,"
			+ "  id_tipo_orden,"
			+ "  id_orden,"
			+ "  id_empresa,"
			+ "  id_motivo,"
			+ "  fecha_ingreso_estado_actual,"
			+ "  fecha_creacion,"
			+ "  discriminador,"
			+ "  id_servicio,"
			+ "  id_usuario_creador,"
			+ "  id_buzon)"
			+ "                       VALUES"
			+ "  (?,"
			+ "   ?,"
			+ "    2,"
			+ "   ?,"
			+ "   ?,"
			+ "   ?,"
			+ "    now()::timestamp,"
			+ "    now()::timestamp,"
			+ "   'ORDEN_MANTENIMIENTO',"
			+ "   ?,"
			+ "   ?,"
			+ "   ?);";

	public static final String SQL_CREAR_ORDEN_MASIVA = "INSERT INTO orm_orden"
			+ "  (id_orden,"
			+ "   id_trabajo,"
			+ "   id_prioridad,"
			+ "   id_tema,"
			+ "   tipo_creacion,"
			+ "   cod_proceso)"
			+ "   VALUES"
			+ "   ( ? ,"
			+ "     ?,"
			+ "     ?,"
			+ "     ?,"
			+ "    'Masiva',"
			+ "    '')";

	public static final String SQL_CREAR_ORDEN_DERIVADA = "INSERT INTO ord_ord_deriv "
			+ " (id_orden, "
			+ "  id_Responsable, "
			+ "  id_ult_responsable) "
			+ "  VALUES "
			+ "  (?, "
			+ "   ?, "
			+ "   ?)";

	public static final String SQL_ACTUALIZAR_NRO_ORDEN_LEGACY = " UPDATE SCHSCOM.EOR_ORD_TRANSFER"
			+ " SET NRO_ORDEN_LEGACY = ?"
			+ " WHERE ID_ORD_TRANSFER = ?";

	public static final String SQLPOSTGRESQL_UPDATE_FOR_ORDTRANSFER = "  UPDATE SCHSCOM.EOR_ORD_TRANSFER"
			+ " SET fec_operacion = now()"
			+ " ,COD_OPERACION = ? "
			+ "	,COD_ESTADO_ORDEN_ANT = COD_ESTADO_ORDEN"
			+ " ,COD_ESTADO_ORDEN = ? "
			+ " ,OBSERVACIONES = ? "
			+ " WHERE ID_ORD_TRANSFER = ? ";

	public static final String SQLPOSTGRESQL_UPDATE_FOR_ORDTRANSFER_DET = "UPDATE SCHSCOM.EOR_ORD_TRANSFER_DET"
			+ "   SET cod_error = ? "
			+ "   WHERE ID_ORD_TRANSFER = ? "
			+ "   AND accion = 'RECEPCION'"
			+ "   AND NRO_EVENTO = ? ";

	public static final String SQL_OBTENER_ID_ORDEN = " SELECT ID_ORDEN "
			+"  FROM ORD_ORDEN "
			+ " WHERE DISCRIMINADOR = ?"
			+ " AND NRO_ORDEN =  ?";

	public static final String SQL_COUNT_ID_OR_FACTI = "SELECT COUNT(ID_ORD_FACTI) CANTIDAD "
			+ " FROM ORM_IN_FACTIBILIDAD "
			+ "	WHERE ID_ORD_FACTI = ? ";

	public static final String DELETE_ORM_IN_FAC_DETALLE = "DELETE ORM_IN_FAC_DETALLE "
			+ "	WHERE ID_ORD_FACTI = ? ";

	public static final String DELETE_ORM_IN_FAC_SUMINISTRO = "DELETE ORM_IN_FAC_SUMINISTRO "
			+ "	WHERE ID_ORD_FACTI = ? ";

	public static final String SQL_OBTENER_ID_REQUISITOS_FACTIBILIDAD = "SELECT ID_FAC_REQUISITO "
			+ " FROM ORM_FAC_REQUISITO"
			+ " WHERE TRIM(COD_REQUISITO) = ?";


	public static final String SQL_NEXT_VAL_SQORMINFACDETALLE = "SELECT SQORMINFACDETALLE.NEXTVAL FROM DUAL";

	public static final String INSERT_ORM_IN_FAC_DETALLE = "INSERT INTO ORM_IN_FAC_DETALLE"
			+ "	(ID_FAC_DETALLE,"
			+ "	ID_ORD_FACTI,"
			+ "	ID_FAC_REQUISITO,"
			+ "	VALOR_REQUISITO)"
			+ "	VALUES"
			+ "	("
			+ " ?,"
			+ " ?,"
			+ " ?,"
			+ " ?"
			+ " )";

	public static final String SQL_NEXT_VAL_SQORMINFACSUMINISTRO = "SELECT SQORMINFACSUMINISTRO.NEXTVAL FROM DUAL";

	public static final String UPDATE_ORM_IN_FACTIBILIDAD = "UPDATE ORM_IN_FACTIBILIDAD "
			+ "	SET NRO_ORD_FACTI = ?, "
			+ "	SUMINISTRO_IZQUIERDO = ?, "
			+ "	SUMINISTRO_DERECHO = ?, "
			+ "	LATITUD_FOTO = ?, "
			+ "	LONGITUD_FOTO = ?, "
			+ "	RESULTADO = ? "
			+ "	WHERE ID_ORD_FACTI = ? ";

	public static final String INSERT_ORM_IN_FACTIBILIDAD = "INSERT INTO ORM_IN_FACTIBILIDAD"
			+ "		("
			+ "			ID_ORD_FACTI,"
			+ "			NRO_ORD_FACTI,"
			+ "			SUMINISTRO_IZQUIERDO,"
			+ "			SUMINISTRO_DERECHO,"
			+ "			LATITUD_FOTO,"
			+ "			LONGITUD_FOTO,"
			+ "			RESULTADO"
			+ "		)"
			+ "		VALUES"
			+ "		("
			+ "			?,"
			+ "			?,"
			+ "			?,"
			+ "			?,"
			+ "			?,"
			+ "			?,"
			+ "			?"
			+ "		)";

	public static final String INSERT_ORM_IN_FAC_SUMINISTRO = "INSERT INTO ORM_IN_FAC_SUMINISTRO\r\n"
			+ "			(\r\n"
			+ "				ID_FAC_SUMINISTRO,\r\n"
			+ "				ID_ORD_FACTI,\r\n"
			+ "				SUMINISTRO\r\n"
			+ "			)\r\n"
			+ "			VALUES\r\n"
			+ "			(\r\n"
			+ "				?,\r\n"
			+ "				?,\r\n"
			+ "				?\r\n"
			+ "			)";

	// Correctivo R.I. 09/08/2023 INICIO
	public static final String CREATE_TMP_ORM_FAC_REQUISITO = "CREATE TEMPORARY TABLE TMP_ORM_FAC_REQUISITO (\r\n"
			+ "    COD_REQUISITO CHAR(50) NOT NULL,\r\n"
			+ "    EVALUAR CHAR(1) NOT NULL,\r\n"
			+ "    ACTIVO CHAR(1) NOT NULL\r\n"
			+ ")";

	public static final String SELECT_FOR_ORM_FAC_REQUISITO = "SELECT COD_REQUISITO, EVALUAR, ACTIVO FROM SYNERGIA.ORM_FAC_REQUISITO";
	
	public static final String INSERT_INTO_TMP_ORM_FAC_REQUISITO = "INSERT INTO TMP_ORM_FAC_REQUISITO (COD_REQUISITO, EVALUAR, ACTIVO) VALUES (?, ?, ?)";
	// Correctivo R.I. 09/08/2023 FIN
}
