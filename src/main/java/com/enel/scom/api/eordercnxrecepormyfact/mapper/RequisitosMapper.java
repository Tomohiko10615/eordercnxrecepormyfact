package com.enel.scom.api.eordercnxrecepormyfact.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepormyfact.validator.dto.Requisito;

public class RequisitosMapper implements RowMapper<Requisito>{

	public static final String SQL_SELECT_REQUISITOS_OBLIGATORIOS = "SELECT R.COD_REQUISITO AS Codigo_requisito, "
			+ " coalesce(O.VALOR_REQUISITO, '') AS Valor_requisito "
			+ " FROM "
			+ " (SELECT  "
			+ " unnest(xpath('/item/codigo_requisito/text()', d.REG_XML))::text AS Codigo_requisito, "
			+ " unnest(xpath('/item/valor_requisito/text()', d.REG_XML))::text AS Valor_requisito "
			+ " FROM (SELECT  unnest(xpath('/Requisitos_Obligatorios/item', "
			+ " unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/factibilidad/Requisitos_Obligatorios', d.REG_XML)))) AS REG_XML "
			+ " FROM SCHSCOM.EOR_ORD_TRANSFER_DET d  "
			+ " WHERE d.id_ord_transfer = ? AND "
			+ " accion = 'RECEPCION'  "
			+ " AND nro_evento = ? "
			+ " )d "
			// Correctivo R.I. 09/08/2023 INICIO
			+ " ) O, TMP_ORM_FAC_REQUISITO R "
			// Correctivo R.I. 09/08/2023 FIN
			+ " WHERE O.CODIGO_REQUISITO = RTRIM(R.COD_REQUISITO) "
			+ " AND R.EVALUAR = 'S'";
	
	public static final String SQL_SELECT_REQUISITOS_OPCIONALES= "SELECT R.COD_REQUISITO AS Codigo_requisito,"
			+ "    coalesce(O.VALOR_REQUISITO, '') AS Valor_requisito"
			+ "    FROM "
			+ "   ("
			+ "		SELECT  "
			+ "			unnest(xpath('/item/codigo_requisito/text()', d.REG_XML))::text AS Codigo_requisito,  "
			+ "			unnest(xpath('/item/valor_requisito/text()', d.REG_XML))::text AS Valor_requisito"
			+ "			FROM (SELECT  unnest(xpath('/ListaRequisitosOpcionales/item', "
			+ "			unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/factibilidad/OtrosRequisitos/ListaRequisitosOpcionales', d.REG_XML)))) AS REG_XML  "
			+ "			FROM SCHSCOM.EOR_ORD_TRANSFER_DET d"
			+ "    	 WHERE d.id_ord_transfer = ?  "
			+ "    	 AND accion = 'RECEPCION' "
			+ "		 AND nro_evento = ? "
			+ "    ) d"
			// Correctivo R.I. 09/08/2023 INICIO
			+ "    )O, TMP_ORM_FAC_REQUISITO R "
			// Correctivo R.I. 09/08/2023 FIN
			+ "    WHERE O.CODIGO_REQUISITO = RTRIM(R.COD_REQUISITO)"
			+ "    AND R.EVALUAR = 'S'";

	public static final String SQL_SELECT_TIPO_ACOMETIDA = "SELECT  \r\n"
			+ " unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/actasConexion/Cod_Tipo_Acometida/text()', d.REG_XML))::text AS Tipo_acometida\r\n"
			+ " FROM SCHSCOM.EOR_ORD_TRANSFER_DET d  \r\n"
			+ " WHERE d.id_ord_transfer = ? AND \r\n"
			+ " accion = 'RECEPCION'  \r\n"
			+ " AND nro_evento = ?";

	public static final String SQL_SELECT_VALORES_VALIDOS = "SELECT OFR.COD_REQUISITO AS Codigo_requisito,\r\n"
			+ "       CASE \r\n"
			+ "           WHEN OFRA.VALORES_APROBACION = '1' THEN 'TRUE'\r\n"
			+ "           WHEN OFRA.VALORES_APROBACION = '0' THEN 'FALSE'\r\n"
			+ "           ELSE 'TRUE'\r\n"
			+ "       END AS Valor_requisito\r\n"
			+ "FROM ORM_FAC_REQUISITO ofr \r\n"
			+ "INNER JOIN SYNERGIA.ORM_FAC_REQUISITO_TIP_ACOM OFRA ON OFR.ID_FAC_REQUISITO = OFRA.ID_FAC_REQUISITO\r\n"
			+ "WHERE OFRA.ID_TIPO_ACOMETIDA = ? \r\n"
			+ "  AND OFR.id_fac_tipo_requisito = to_char(?)\r\n"
			+ "UNION ALL\r\n"
			+ "SELECT OFR.COD_REQUISITO AS Codigo_requisito,\r\n"
			+ "       'FALSE' AS Valor_requisito\r\n"
			+ "FROM ORM_FAC_REQUISITO ofr \r\n"
			+ "INNER JOIN SYNERGIA.ORM_FAC_REQUISITO_TIP_ACOM OFRA ON OFR.ID_FAC_REQUISITO = OFRA.ID_FAC_REQUISITO\r\n"
			+ "WHERE OFRA.ID_TIPO_ACOMETIDA = ? \r\n"
			+ "  AND OFR.id_fac_tipo_requisito = to_char(?)\r\n"
			+ "  AND OFRA.VALORES_APROBACION = '0,1'";

	public static final String SQL_SELECT_FOR_ID_TIPO_ACOMETIDA = "SELECT ID_TIPO_ACOMETIDA  FROM SRV_TIPO_ACOMETIDA WHERE CODIGO = ?";
	
	@Override
	public Requisito mapRow(ResultSet rs, int rowNum) throws SQLException {
		return Requisito.builder()
				.codigoRequisito(rs.getString("Codigo_requisito").trim())
				.valorRequisito(Boolean.valueOf(rs.getString("Valor_requisito")))
				.build();
	}

}
