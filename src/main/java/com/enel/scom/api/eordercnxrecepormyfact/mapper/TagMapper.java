package com.enel.scom.api.eordercnxrecepormyfact.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class TagMapper{

	public static final String SQL_SELECT_TAG_COMUN = "SELECT A.NOM_TAG, A.NOM_COLUMNA, A.TIPO, A.EDELNOR, A.ENARCHIVO, A.OBLIGATORIO, A.POSARCHIVO "
			+ "	FROM SCHSCOM.EOR_PLANILLA A "
			+ "	WHERE A.COD_PROCESO = 'COMUN' "
			+ "	AND A.COD_TIPO = 'FR' "
			+ "	AND A.ACTIVO = 'S' "
			+ "	ORDER BY A.POSICION";
	public static final String SQL_SELECT_TAG_COMUN_REVISION = "SELECT A.NOM_TAG, A.NOM_COLUMNA, A.TIPO, A.EDELNOR, A.ENARCHIVO, A.OBLIGATORIO, A.POSARCHIVO "
			+ "	FROM SCHSCOM.EOR_PLANILLA A "
			+ "	WHERE A.COD_PROCESO = 'CNX' "
			+ "	 AND A.COD_TIPO = 'FR' "
			+ "	 AND A.COD_PLANILLA = 'R001' "
			+ "	 AND A.ACTIVO = 'S' "
			+ "	ORDER BY A.POSICION";

}
