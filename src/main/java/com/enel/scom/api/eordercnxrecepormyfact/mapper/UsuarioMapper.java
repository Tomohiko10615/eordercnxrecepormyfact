package com.enel.scom.api.eordercnxrecepormyfact.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepormyfact.bean.Usuario;

public class UsuarioMapper implements RowMapper<Usuario> {

	public static final String SQLPOSTGRESQL_SELECT_FOR_USUARIO= "SELECT " +
			" ID" +
			" FROM schscom.USUARIO " +
			" WHERE  USERNAME = ? "
			;

	@Override
	public Usuario mapRow(ResultSet rs, int rowNum) throws SQLException {
		Usuario oUsuario = Usuario.builder().build();
		oUsuario.setId(rs.getLong("ID"));
		return oUsuario;
	}

}
