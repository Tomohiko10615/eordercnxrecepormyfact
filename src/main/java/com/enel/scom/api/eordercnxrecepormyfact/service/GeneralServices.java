package com.enel.scom.api.eordercnxrecepormyfact.service;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.eordercnxrecepormyfact.bean.Fecha;
import com.enel.scom.api.eordercnxrecepormyfact.dao.IEmpresaDAO;
import com.enel.scom.api.eordercnxrecepormyfact.dao.IFechaHoraDAO;
import com.enel.scom.api.eordercnxrecepormyfact.dao.IOthersDAO;
import com.enel.scom.api.eordercnxrecepormyfact.dao.IUsuarioDAO;

@Service
public class GeneralServices {
	
	 @Autowired
	 IFechaHoraDAO fechaHoraDAO;
	
	 @Autowired
	 IEmpresaDAO empresaDAO;
	 
	 @Autowired
	 IUsuarioDAO usuarioDAO;
	 
	 @Autowired
	 IOthersDAO othersDAO;
	 
	 
	 public String getFechaHoy() {
		 Fecha fecha = fechaHoraDAO.getFechaHoy("yyyymmdd.hh24:mi:ss.MS");
		 return fecha.toString();
	 }

	public Integer bfnVerificaEmpresa(String gc_EMPRESA) {
		return empresaDAO.bfnVerificaEmpresa(gc_EMPRESA);
	}

	public Long bfnObtenerIdUsuario(String gc_USER) {
		return usuarioDAO.bfnObtenerIdUsuario(gc_USER);
	}

	public String bfnObtenerPath(String entidad , String key) {
		return othersDAO.bfnObtenerPath(entidad,key);
	}

	public String bfnGetPathORM_In() {
		return othersDAO.bfnGetPathORM_In();
	}

	public Integer bfnObtenerEstadoTransferencia(String key) {
		return othersDAO.bfnObtenerEstadoTransferencia(key);
	}

	public Map<String, String> bfnObtenerErrores(String key) {
		return othersDAO.bfnObtenerErrores(key);
	}

	public String bfnObtenerCuadrillaGen() {
		return othersDAO.bfnObtenerCuadrillaGen();
	}

	public String bfnObtenerParametros(String entidad , String key) {
		return othersDAO.bfnObtenerParametros(entidad,key);
	}

	public List<Map<String, Object>> bfnFetchTagComun() {
		return othersDAO.bfnFetchTagComun();
	}

	public List<Map<String, Object>> bfnFetchTagActaRevision() {
		return othersDAO.bfnFetchTagActaRevision();
	}

	public List<Map<String, Object>> bfnObtenerRegistrosProcesar(Integer vCodEstPendRecep) {
		return othersDAO.bfnObtenerRegistrosProcesar(vCodEstPendRecep);
	}

	public List<Map<String, Object>> bfnObtenerRegistrosProcesar4J(Integer vCodEstPendRecep) {
		return othersDAO.bfnObtenerRegistrosProcesar4J(vCodEstPendRecep);
	}

	public boolean validarFactorMedidor(String marcaMedidor, String modeloMedidor, String numMedidor, String factorMedidor) {
		return othersDAO.validarFactorMedidor(marcaMedidor, modeloMedidor, numMedidor, factorMedidor);
		
	}
}
