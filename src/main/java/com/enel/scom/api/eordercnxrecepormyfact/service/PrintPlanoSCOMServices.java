package com.enel.scom.api.eordercnxrecepormyfact.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.enel.scom.api.eordercnxrecepormyfact.dao.IPrintPlanoSCOMDAO;
import com.enel.scom.api.eordercnxrecepormyfact.util.Constants;
import com.enel.scom.api.eordercnxrecepormyfact.util.Others;

@Service
public class PrintPlanoSCOMServices {

	@Autowired
	IPrintPlanoSCOMDAO printplanoScomDAO;
	
	@Autowired
	private GeneralServices generalServices;

	private static final Logger logger = LoggerFactory.getLogger(PrintPlanoSCOMServices.class);
	
	public boolean imprimirPlano() {
		int x;
		Long lAcumulaTemp=0L, lTotDatoClave=0L;
		String cCadenaTareas="";
		String cCadenaCambioMedidor="";
		String cCadenaContrasteMedidor="";
		String cCadenaLecturasInst="";
		String cCadenaLecturasRet="";
		String cCadenaLecturasRetObs="";
		String cCadenaSellos="";
		int  iPal;
		int indTmp;
		int iNroMedidores=0;
		int iIndMedInstall=-1;
		int iIndMedRet=-1;
		int iReqCambioMed;
		/*roliva*/
		Others.iflagerror = -1;
		String cFactResultado=Constants.FAC_RESULTADO_RECHAZADO;
		int iFactRequisitosOpci = Constants.FAC_REQUISITO_VALIDO;
		int iFactRequisitosObli = Constants.FAC_REQUISITO_VALIDO;
		int iFactCount=0;//edwhampp
		int iContLectIns=0, iContLectRet=0; /* ANL_20210202 */
		String cCadenaMarReti="", cCadenaMarInst="";
		String cCadenaModReti="", cCadenaModInst="";
		String cCadenaNumReti="", cCadenaNumInst="";
		
		String factorMedidor = ""; // R.I. Agregado 31/10/2023
		
		logger.info("ImprimirPlano paso 1: {}",Others.vCodOperacion);
		/* Obtiene valores de las etiquetas XML */
		if(!bfnDeclararCursorImprimir()){
			logger.error("Error al declarar los cursores de impresión...Method bfnDeclararCursorImprimir()");
			return false;
		}
		
		logger.info("ImprimirPlano paso 2: {}",Others.vCodOperacion);
		if(Others.qTagActaRevision==null || Others.qTagActaRevision.isEmpty()){
			logger.error("Error al obtener los datos de Acta Revisión");
			return false;
		}
		
		Others.gcNum_Orden=Others.vNroOrden.trim();
		
		logger.info("ImprimirPlano paso 3: {}",Others.vCodOperacion);
		if(Others.qDatoClave==null || Others.qDatoClave.isEmpty()){
			logger.error("Error al obtener los datos clave");
			return false;
		}
		
		/* Validaciones de datos */
		if(!(bfnValidarDatos("Codigo_Distribuidora", (String) Others.qDatoClave.get("Codigo_Distribuidora"), "COMUN"))) return true;
		if(!(bfnValidarDatos("Codigo_Sistema_Externo_Origen", (String) Others.qDatoClave.get("Codigo_Sistema_Externo_Origen"), "COMUN"))) return true;
		if(!(bfnValidarDatos("Codigo_Tipo_de_TdC", (String) Others.qDatoClave.get("Codigo_Tipo_de_TdC"), "COMUN"))) return true;

		if(!((String)Others.qDatoClave.get("TDC_Creado_Eorder")).trim().equalsIgnoreCase("1"))
		{
			if(!(bfnValidarDatos("Codigo_Externo_del_TdC", (String) Others.qDatoClave.get("Codigo_Externo_del_TdC"), "COMUN"))) return true;
		}
		
		if(!(bfnValidarDatos("Codigo_Interno_del_TdC", (String) Others.qDatoClave.get("Codigo_Interno_del_TdC"), "COMUN"))) return true;
		if(!(bfnValidarDatos("Codigo_Resultado", (String) Others.qDatoClave.get("Codigo_Resultado"), "COMUN"))) return true;
		if(!(bfnValidarDatos("Codigo_Causal_Resultado", (String) Others.qDatoClave.get("Codigo_Causal_Resultado"), "COMUN"))) return true;
		if(!(bfnValidarDatos("Codigo_Nota_Codificada", (String) Others.qDatoClave.get("Codigo_Nota_Codificada"), "COMUN"))) return true;
		if(!(bfnValidarDatos("Inicio_TdC", (String) Others.qDatoClave.get("Inicio_TdC"), "COMUN"))) return true;
		if(!(bfnValidarDatos("Fin_TdC", (String) Others.qDatoClave.get("Fin_TdC"), "COMUN"))) return true;
		if(!(bfnValidarDatos("Duracion_Ejecucion", (String) Others.qDatoClave.get("Duracion_Ejecucion"), "COMUN"))) return true;
		if(!(bfnValidarDatos("Codigo_Cuadrilla", (String) Others.qDatoClave.get("Codigo_Cuadrilla"), "COMUN"))) return true;
	
		if(!((String)Others.qDatoClave.get("TDC_Creado_Eorder")).trim().equalsIgnoreCase("1"))
		{
			if (!bfnValidarORMExist((String) Others.qDatoClave.get("Codigo_Externo_del_TdC")))
			{
				setErrGen("No existe la Orden o no se encuentra en estado Creada");
				return true;
			}
		}
		Others.vNroCuenta=0L;

		/*Nro de Cuenta registrada en la orden*/
		// if(!((String)Others.qDatoClave.get("Codigo_Externo_del_TdC")).equals("")) {
		String numeroCuenta = Others.qDatoClave.get("Codigo_Externo_del_TdC") == null ? "" : (String) Others.qDatoClave.get("Codigo_Externo_del_TdC");
			
		if(!bfnObtenerNroCuenta(numeroCuenta))
		{ 
			logger.error(String.format("Error: No se encuentra configurado Nro de Cuenta para orden %s", (String) Others.qDatoClave.get("Codigo_Externo_del_TdC")));
			setErrGen(String.format("No se encuentra configurado Nro de Cuenta para orden %s", (String) Others.qDatoClave.get("Codigo_Externo_del_TdC")));
			return true;
		}
			
		// }
			
		logger.info("ImprimirPlano paso 4: {}",Others.vCodOperacion);
		/* Colección Operaciones  */
		if(Others.qOper==null || Others.qOper.isEmpty()) {
			logger.error("Error al obtener los datos de Operación");
			return false;
		}
			
		if(!(bfnValidarDatos("Notas_Operacion", (String) Others.qOper.get("Notas_Operacion"), "COMUN"))) return true;

		logger.info("ImprimirPlano paso 5: {}",Others.vCodOperacion); /*OMITIDO PARA SCOM, 513 - 599 (Archivo C) 
		 																PORCION DE CÓDIGO OMITIDA POR QUE NO EXISTEN LAS TABLAS:
																		--ORM_IN_FACTIBILIDAD
																		--ORM_IN_FAC_DETALLE
																		--ORM_IN_FAC_SUMINISTRO
		 																*/ 
		logger.info("ImprimirPlano paso 6: {}",Others.vCodOperacion); 
		/* Colección Tareas */
		Others.iConTareas=0;
		cCadenaTareas="";
		iReqCambioMed =0;
		for (Map<String, Object> mapTareas : Others.qTareas) {
			if(!bfnValidarTareas((String)mapTareas.get("Nro_Tarea"),(String)mapTareas.get("Identificador"),
					(String)mapTareas.get("Cantidad"),(String)mapTareas.get("Inicio_Operacion"))) {
				Others.iIndicaErr=1;
				return true;
			}
			
			if(iReqCambioMed==0) {
				iReqCambioMed=bfnReqCambioMed((String)mapTareas.get("Nro_Tarea"));
			}
			if(Others.iIndicaErr==0) {
				String vEJECUTADO= (String)mapTareas.get("Inicio_Operacion");
				cCadenaTareas= String.format("%s%s%s%s%s%s%s%s%s",
						cCadenaTareas,mapTareas.get("Nro_Tarea"),Constants.SC,
						(String)mapTareas.get("Identificador"),Constants.SC,
						(String)mapTareas.get("Cantidad"),Constants.SC,
						vEJECUTADO,Constants.ST
						);
			}else {
				return true;
			}
		}
		
		if(cCadenaTareas.length()>0) {
			 StringBuilder newString = new StringBuilder(cCadenaTareas);
		        newString.setCharAt(cCadenaTareas.length()-1, '\0');
		        cCadenaTareas=newString.toString();
		}else {
			setErrGen("Faltan, tareas la Orden debe incluir por lo menos una tarea ");
			return true;
		}
		logger.info("ImprimirPlano paso 6.1: {}",Others.vCodOperacion); 
		/* Colección Recursos */
		Others.vTecnico="";
		Others.vCodEjecutor="";
			
		if (!bfnGetCodEjeGen(Others.cCodCuadrillaGen))
		{
			setErrGen("No existe técnico para cuadrilla generica:"+Others.cCodCuadrillaGen);
			return true;
		}
		
		
		/* Colección Medidores */
		logger.info("ImprimirPlano paso 7 scom: {}",Others.vCodOperacion); 
		cCadenaCambioMedidor="";
		
		
		
		logger.info("arrOperacion_c_Notas_Operacion: {}", Others.qOper.get("Notas_Operacion"));
		String arrOperacion_c_Notas_Operacion = "";
		if (Others.qOper.get("Notas_Operacion") == null) {
			arrOperacion_c_Notas_Operacion = "";
		}
		else {
			arrOperacion_c_Notas_Operacion=((String) Others.qOper.get("Notas_Operacion")).trim();
		}
		
		String vMarcaMedidorRetirar="";
		String vModeloMedidorRetirar="";
		String vNumeroMedidorRetirar="";
		String vMarcaMedidorInstalar="";
		String vModeloMedidorInstalar="";
		String vNumeroMedidorInstalar="";
		String vPropiedadMedidorInstalar="";
		String vFechaInstalacion="";
		
		String vFechaInstalacionInstalar="";
		
		if(arrOperacion_c_Notas_Operacion.length()==0) {
			arrOperacion_c_Notas_Operacion="N";
		}
		
		if(!((String) Others.qDatoClave.get("TDC_Creado_Eorder")).trim().equalsIgnoreCase("1")) {
		if (iReqCambioMed ==1)
		{
			indTmp =0;
			for (Map<String, Object> mapMedidores : Others.qMedidores) {
				if(!(bfnValidarDatos("accionMedidor", (String) mapMedidores.get("accionMedidor"), "COMUN"))) return true;
				
				if (((String) mapMedidores.get("accionMedidor")).trim().equalsIgnoreCase("Retirado")) continue; //Se descarta los medidores retirados
						
				if(!(bfnValidarDatos("marcaMedidor", (String) mapMedidores.get("marcaMedidor"), "COMUN"))) return true;
				if(!(bfnValidarDatos("modeloMedidor", (String) mapMedidores.get("modeloMedidor"), "COMUN"))) return true;
				if(!(bfnValidarDatos("numeroMedidor", (String) mapMedidores.get("numeroMedidor"), "COMUN"))) return true;
				if(!(bfnValidarDatos("propiedadMedidor", (String) mapMedidores.get("propiedadMedidor"), "COMUN"))) return true;
				if(!(bfnValidarDatos("fechaInstalacion", (String) mapMedidores.get("fechaInstalacion"), "COMUN"))) return true;
				
				if(!bfnValidarDominio("MED_MARCA", "COD_MARCA", (String) mapMedidores.get("marcaMedidor")))
				  {
					Others.lDatosErr++;
					return false;
				  }
				if(!bfnValidarDominio("MED_MODELO", "COD_MODELO", (String) mapMedidores.get("modeloMedidor")))
				  {
					Others.lDatosErr++;
					return false;
				  }
				if (Others.iIndicaErr == 0)
				{
					if(((String) mapMedidores.get("accionMedidor")).equalsIgnoreCase("Encontrado")) {
						iIndMedRet++;
						iNroMedidores++;
						
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("marcaMedidor", (String) mapMedidores.get("marcaMedidor"));
						map.put("modeloMedidor", (String) mapMedidores.get("modeloMedidor"));
						map.put("numeroMedidor", (String) mapMedidores.get("numeroMedidor"));
						map.put("fechaInstalacion", (String) mapMedidores.get("fechaInstalacion"));
						
						Map<String, Object> mapOfCharge= new HashMap<String, Object>();
						for (Map.Entry<String, Object> entry : map.entrySet()) {
							String key=entry.getKey();
							String value=entry.getValue().toString();
							mapOfCharge.put(key, value);
						}
						
						Others.arrMedidorRet.add(iIndMedRet,mapOfCharge);
						
						if(!ObtenerFechaDDMMYYY((String)Others.arrMedidorRet.get(iIndMedRet).get("fechaInstalacion"))) {
							setErrGen("Error en Fecha Retiro "+ Others.arrMedidorRet.get(iIndMedRet).get("fechaInstalacion") +" (formato YYYY-MM-DD)");
							return true;
						}
						Others.arrMedidorRet.get(iIndMedRet).put("fechaInstalacion", Others.fechaObtenida);
						
						logger.info("marcaMedidor: {}", Others.arrMedidorRet.get(iIndMedRet).get("marcaMedidor"));
						logger.info("modeloMedidor: {}", Others.arrMedidorRet.get(iIndMedRet).get("modeloMedidor"));
						logger.info("numeroMedidor: {}", Others.arrMedidorRet.get(iIndMedRet).get("numeroMedidor"));
						logger.info("vNroCuenta: {}", Others.vNroCuenta);

						if(!((String)Others.qDatoClave.get("TDC_Creado_Eorder")).trim().equalsIgnoreCase("1"))
						{
							if (!bfnValidarCompRet((String)Others.arrMedidorRet.get(iIndMedRet).get("marcaMedidor"),
									(String)Others.arrMedidorRet.get(iIndMedRet).get("modeloMedidor"),
									(String)Others.arrMedidorRet.get(iIndMedRet).get("numeroMedidor"),
									Others.vNroCuenta)) {
								setErrGen("Medidor a retirar:"+(String)Others.arrMedidorRet.get(iIndMedRet).get("marcaMedidor")+"-"
										+ (String)Others.arrMedidorRet.get(iIndMedRet).get("modeloMedidor")+"-"
										+ (String)Others.arrMedidorRet.get(iIndMedRet).get("numeroMedidor")+" "
										+ "no existe o no está instalado en el servicio de la cuenta "
										+ Others.vNroCuenta);
								return true;
							}
									
						}
					}

					logger.info("Paso K.33");
					logger.info("ImprimirPlano paso 7.1 scom: {}",Others.vCodOperacion); 
					logger.info("accionMedidor scom: {}", mapMedidores.get("accionMedidor"));

					if(((String) mapMedidores.get("accionMedidor")).equalsIgnoreCase("Instalado")) {
						iIndMedInstall++;
						iNroMedidores++;
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("marcaMedidor", (String) mapMedidores.get("marcaMedidor"));
						map.put("modeloMedidor", (String) mapMedidores.get("modeloMedidor"));
						map.put("numeroMedidor", (String) mapMedidores.get("numeroMedidor"));
						map.put("propiedadMedidor", (String) mapMedidores.get("propiedadMedidor"));
						map.put("fechaInstalacion", (String) mapMedidores.get("fechaInstalacion"));
						
						map.put("factorMedidor", (String) mapMedidores.get("factorMedidor")); // R.I. Agregado 31/10/2023
						
						if ((String) mapMedidores.get("factorMedidor") != null
								&& !((String) mapMedidores.get("factorMedidor")).equals("")) {
							if (!generalServices.validarFactorMedidor(
									(String) mapMedidores.get("marcaMedidor"),
									(String) mapMedidores.get("modeloMedidor"),
									(String) mapMedidores.get("numeroMedidor"),
									(String) mapMedidores.get("factorMedidor")
									)) {
								setErrGen("El factor de facturación del medidor no es válido.");
								return true;
							}
						}
						
						logger.info("ImprimirPlano paso 7.2 scom: {}",Others.vCodOperacion); 

						
						
						Map<String, Object> mapOfCharge= new HashMap<String, Object>();
						for (Map.Entry<String, Object> entry : map.entrySet()) {
							logger.info("entry.getKey() scom: {}",entry.getKey()); 
							logger.info("entry.getValue() scom: {}",entry.getValue()); 

							String key=entry.getKey();
							String value=entry.getValue()==null ? "" : entry.getValue().toString();
							mapOfCharge.put(key, value);
						}
						logger.info("ImprimirPlano paso 7.3 scom: {}",Others.vCodOperacion); 
						
						Others.arrMedidorInst.add(iIndMedInstall,mapOfCharge);
						logger.info("ImprimirPlano paso 7.4 scom: {}",Others.vCodOperacion); 
						
						if(!ObtenerFechaDDMMYYY((String)Others.arrMedidorInst.get(iIndMedInstall).get("fechaInstalacion"))) {
							setErrGen("Error en Fecha Retiro "+ Others.arrMedidorInst.get(iIndMedInstall).get("fechaInstalacion") +" (formato YYYY-MM-DD)");
							return true;
						}
						Others.arrMedidorInst.get(iIndMedInstall).put("fechaInstalacion", Others.fechaObtenida);

						logger.info("ImprimirPlano paso 7.5 scom: {}",Others.vCodOperacion); 
						
							if (!bfnValidarCompInstall((String)Others.arrMedidorInst.get(iIndMedInstall).get("marcaMedidor"),
									(String)Others.arrMedidorInst.get(iIndMedInstall).get("modeloMedidor"),
									(String)Others.arrMedidorInst.get(iIndMedInstall).get("numeroMedidor"))) {
								setErrGen("Medidor a Instalar:"+(String)Others.arrMedidorInst.get(iIndMedInstall).get("marcaMedidor")+"-"
										+ (String)Others.arrMedidorInst.get(iIndMedInstall).get("modeloMedidor")+"-"
										+ (String)Others.arrMedidorInst.get(iIndMedInstall).get("numeroMedidor")+" "
										+ "no existe o no está disponible");
								return true;
							}
						logger.info("ImprimirPlano paso 7.6 scom: {}",Others.vCodOperacion); 

						
					}
				}else {
					logger.info("ImprimirPlano paso 7.7 scom: {}",Others.vCodOperacion); 

					return true;
				} 
				indTmp++;
			}//End While Medidores
			logger.info("ImprimirPlano paso 7.8 scom: {}",Others.vCodOperacion); 
			
			logger.info("iIndMedInstall scom: {}", iIndMedInstall);
			logger.info("iIndMedRet scom: {}", iIndMedRet);
			logger.info("iIndMedInstall==-1 scom: {}", iIndMedInstall==-1);
			logger.info("iIndMedRet==-1 scom: {}", iIndMedRet==-1);

			if (iIndMedInstall==-1 || iIndMedRet==-1)
			{
				logger.info("ImprimirPlano paso 7.9 scom: {}",Others.vCodOperacion); 
				
				setErrGen("Faltan datos de cambio de medidor para la orden "+ (String) Others.qDatoClave.get("Codigo_Externo_del_TdC"));
				return true;
			}
			
			/* Colección Lecturas */
			logger.info("ImprimirPlano paso 8: {}",Others.vCodOperacion); 
			int iContLecturas=0;
			cCadenaLecturasRet="";
			cCadenaLecturasInst="";
			indTmp =0;
			for (Map<String, Object> mapLecturas : Others.qLecturas) {
				
				if(((String) mapLecturas.get("accionMedidor")).equalsIgnoreCase("Instalado")) {
					iContLecturas++;

					if(!(bfnValidarDatos("tipoLectura", (String) mapLecturas.get("tipoLectura"), "COMUN"))) return true;
					if(!(bfnValidarDatos("horarioLectura", (String) mapLecturas.get("horariolectura"), "COMUN"))) return true;
					if(!(bfnValidarDatos("lectura", (String) mapLecturas.get("estadoLeido"), "COMUN"))) return true;
					if(!(bfnValidarDatos("fechalectura", (String) mapLecturas.get("fechalectura"), "COMUN"))) return true;
					
					if(!ObtenerFechaDDMMYYY((String) mapLecturas.get("fechalectura"))) {
						setErrGen("Error en Fecha Lectura "+ (String) mapLecturas.get("fechalectura") +" (formato YYYY-MM-DD)");
						return true;
					}
					mapLecturas.put("fechalectura", Others.fechaObtenida);

					if (!bfnValidarDominio("MED_TIP_MEDIDA", "COD_TIP_MEDIDA", (String) mapLecturas.get("tipoLectura")))
					{
						Others.lDatosErr++;
						return true;
					}
					if (!bfnValidarDominio("MED_MEDIDA", "COD_MEDIDA", (String) mapLecturas.get("horariolectura")))
					{
						Others.lDatosErr++;
						return true;
					}
					
					for(x=0; x<=iIndMedInstall; x++) 
					{
						if(Others.arrMedidorInst.get(x).get("numeroMedidor").equals(mapLecturas.get("numeroMedidor"))
							&& 	Others.arrMedidorInst.get(x).get("marcaMedidor").equals(mapLecturas.get("marcaMedidor"))
							&& Others.arrMedidorInst.get(x).get("modeloMedidor").equals(mapLecturas.get("modeloMedidor"))){
							
							if(! bfnValidarMedidaMedidor((String)Others.arrMedidorInst.get(x).get("marcaMedidor"),
														(String)Others.arrMedidorInst.get(x).get("modeloMedidor"),
														(String) mapLecturas.get("numeroMedidor"),
														(String) mapLecturas.get("horariolectura"))) {
								
								String vstrTmp = String.format("El medidor %s-%s-%s No soporta la medida %s (1)", Others.arrMedidorInst.get(x).get("marcaMedidor"), Others.arrMedidorInst.get(x).get("modeloMedidor"), mapLecturas.get("numeroMedidor"), mapLecturas.get("horariolectura"));
								setErrGen(vstrTmp);
								return true;
							}
							
							if (Others.iIndicaErr == 0)
							{
								iContLectIns++;
								if (iContLectIns == 1)
								{
									cCadenaLecturasInst= String.format("%s%s%s",(String) mapLecturas.get("horarioLectura"),Constants.SCL,(String) mapLecturas.get("estadoLeido"));
									
									cCadenaMarInst=String.format("%s",(String) Others.arrMedidorInst.get(x).get("marcaMedidor"));
									cCadenaModInst=String.format("%s",(String) Others.arrMedidorInst.get(x).get("modeloMedidor"));
									cCadenaNumInst=String.format("%s",(String) Others.arrMedidorInst.get(x).get("numeroMedidor"));
									
									factorMedidor = String.format("%s",
											(String) Others.arrMedidorInst.get(x).get("factorMedidor")); // R.I. Agregado 31/10/2023
									
								}else {
									
									cCadenaLecturasInst= String.format("%s%s%s%s%s",cCadenaLecturasInst,Constants.SL,(String) mapLecturas.get("horarioLectura"),Constants.SCL,(String) mapLecturas.get("estadoLeido"));
									
									cCadenaMarInst=String.format("%s%s%s",cCadenaMarInst,Constants.SL,(String) Others.arrMedidorInst.get(x).get("marcaMedidor"));
									cCadenaModInst=String.format("%s%s%s",cCadenaModInst,Constants.SL,(String) Others.arrMedidorInst.get(x).get("modeloMedidor"));
									cCadenaNumInst=String.format("%s%s%s",cCadenaNumInst,Constants.SL,(String) Others.arrMedidorInst.get(x).get("numeroMedidor"));
									
									factorMedidor = String.format("%s",
											(String) Others.arrMedidorInst.get(x).get("factorMedidor")); // R.I. Agregado 31/10/2023
									
								}
							}else {
								return true;
							}
							vPropiedadMedidorInstalar=(String)Others.arrMedidorInst.get(x).get("propiedadMedidor");
							vFechaInstalacionInstalar=(String)Others.arrMedidorInst.get(x).get("fechaInstalacion");
						}
					}
					vMarcaMedidorInstalar=cCadenaMarInst;
					vModeloMedidorInstalar=cCadenaModInst;
					vNumeroMedidorInstalar=cCadenaNumInst;
				}
				
				if(((String) mapLecturas.get("accionMedidor")).equalsIgnoreCase("Encontrado")) {
					
					iContLecturas++;
					if(!(bfnValidarDatos("tipoLectura", (String) mapLecturas.get("tipoLectura"), "COMUN"))) return true;
					if(!(bfnValidarDatos("horarioLectura", (String) mapLecturas.get("horariolectura"), "COMUN"))) return true;
					if(!(bfnValidarDatos("lectura", (String) mapLecturas.get("estadoLeido"), "COMUN"))) return true;
					if(!(bfnValidarDatos("fechalectura", (String) mapLecturas.get("fechalectura"), "COMUN"))) return true;
				
					if (!bfnValidarDominio("MED_TIP_MEDIDA", "COD_TIP_MEDIDA", (String) mapLecturas.get("tipoLectura")))
					{
						Others.lDatosErr++;
						return true;
					}
					if (!bfnValidarDominio("MED_MEDIDA", "COD_MEDIDA", (String) mapLecturas.get("horariolectura")))
					{
						Others.lDatosErr++;
						return true;
					}
					for(x=0; x<=iIndMedRet; x++) 
					{
						if(Others.arrMedidorRet.get(x).get("numeroMedidor").equals(mapLecturas.get("numeroMedidor"))
								&& 	Others.arrMedidorRet.get(x).get("marcaMedidor").equals(mapLecturas.get("marcaMedidor"))
								&& Others.arrMedidorRet.get(x).get("modeloMedidor").equals(mapLecturas.get("modeloMedidor"))){
							
							if(! bfnValidarMedidaMedidor((String)Others.arrMedidorRet.get(x).get("marcaMedidor"),
									(String)Others.arrMedidorRet.get(x).get("modeloMedidor"),
									(String) mapLecturas.get("numeroMedidor"),
									(String) mapLecturas.get("horariolectura"))) {
								String vstrTmp = String.format("El medidor %s-%s-%s No soporta la medida %s (2)", Others.arrMedidorRet.get(x).get("marcaMedidor"), Others.arrMedidorRet.get(x).get("modeloMedidor"), mapLecturas.get("numeroMedidor"), mapLecturas.get("horariolectura"));
								setErrGen(vstrTmp);
								return true;
							}
							if (Others.iIndicaErr == 0)
							{
								iContLectRet++;
								if (iContLectRet == 1)
								{
									cCadenaLecturasRet= String.format("%s%s%s",(String) mapLecturas.get("horarioLectura"),Constants.SCL,(String) mapLecturas.get("estadoLeido"));
									
									cCadenaMarReti=String.format("%s",(String) Others.arrMedidorRet.get(x).get("marcaMedidor"));
									cCadenaModReti=String.format("%s",(String) Others.arrMedidorRet.get(x).get("modeloMedidor"));
									cCadenaNumReti=String.format("%s",(String) Others.arrMedidorRet.get(x).get("numeroMedidor"));
									
								}else {
									cCadenaLecturasRet= String.format("%s%s%s%s%s",cCadenaLecturasRet,Constants.SL,(String) mapLecturas.get("horarioLectura"),Constants.SCL,(String) mapLecturas.get("estadoLeido"));
									
									cCadenaMarReti=String.format("%s%s%s",cCadenaMarReti,Constants.SL,(String) Others.arrMedidorRet.get(x).get("marcaMedidor"));
									cCadenaModReti=String.format("%s%s%s",cCadenaModReti,Constants.SL,(String) Others.arrMedidorRet.get(x).get("modeloMedidor"));
									cCadenaNumReti=String.format("%s%s%s",cCadenaNumReti,Constants.SL,(String) Others.arrMedidorRet.get(x).get("numeroMedidor"));
									
								}
							}else {
								return true;
							}
						
						}
					}
					vMarcaMedidorRetirar  = cCadenaMarReti;
					vModeloMedidorRetirar = cCadenaModReti;
					vNumeroMedidorRetirar = cCadenaNumReti;
				}
				indTmp++;
			}//End While Lecturas
			
			if (iContLecturas<iNroMedidores)
			{
				setErrGen("Faltan lecturas de cambio de medidor para la orden "+ (String) Others.qDatoClave.get("Codigo_Externo_del_TdC"));
				return true;
			}
			
		/* Colección Sellos */
			logger.info("ImprimirPlano paso 9: {}",Others.vCodOperacion); /*OMITIDO PARA SCOM, 1002 - 1064 (Archivo C) 
																			PORCION DE CÓDIGO OMITIDA POR QUE NO SE USAN SELLOS.
																			*/  
			
			logger.info("ImprimirPlano paso 10: {}",Others.vCodOperacion);
			if (Others.iIndicaErr == 0)
			{
				cCadenaCambioMedidor=String.format("%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s", // R.I. Agregado %s 31/10/2023
						vFechaInstalacionInstalar,Constants.SC,
						vMarcaMedidorRetirar,Constants.SC,vModeloMedidorRetirar,Constants.SC,
						vNumeroMedidorRetirar,Constants.SC,cCadenaLecturasRet,Constants.SC,
						vMarcaMedidorInstalar,Constants.SC,vModeloMedidorInstalar,Constants.SC,
						vNumeroMedidorInstalar,Constants.SC,
						factorMedidor, Constants.SC, // R.I. Agregado 31/10/2023
						cCadenaLecturasInst,Constants.SC,
						vPropiedadMedidorInstalar,cCadenaSellos);
			}else {
				return true;
			}
		}//end if CAMBIO DE MEDIDOR
		else {
			indTmp=0;
			for (Map<String, Object> mapMedidores : Others.qMedidores) {
				if(!(bfnValidarDatos("accionMedidor", (String) mapMedidores.get("accionMedidor"), "COMUN"))) return true;
				
				if (((String) mapMedidores.get("accionMedidor")).trim().equalsIgnoreCase("Retirado")) continue; //Se descarta los medidores retirados
				if (((String) mapMedidores.get("accionMedidor")).trim().equalsIgnoreCase("Instalado")) continue; //Se descarta los medidores Instalados
				
				
				if(!(bfnValidarDatos("marcaMedidor", (String) mapMedidores.get("marcaMedidor"), "COMUN"))) return true;
				if(!(bfnValidarDatos("modeloMedidor", (String) mapMedidores.get("modeloMedidor"), "COMUN"))) return true;
				if(!(bfnValidarDatos("numeroMedidor", (String) mapMedidores.get("numeroMedidor"), "COMUN"))) return true;
				if(!(bfnValidarDatos("propiedadMedidor", (String) mapMedidores.get("propiedadMedidor"), "COMUN"))) return true;
				if(!(bfnValidarDatos("fechaInstalacion", (String) mapMedidores.get("fechaInstalacion"), "COMUN"))) return true;
				
				if(!bfnValidarDominio("MED_MARCA", "COD_MARCA", (String) mapMedidores.get("marcaMedidor")))
				  {
					Others.lDatosErr++;
					return false;
				  }
				if(!bfnValidarDominio("MED_MODELO", "COD_MODELO", (String) mapMedidores.get("modeloMedidor")))
				  {
					Others.lDatosErr++;
					return false;
				  }
				if (Others.iIndicaErr == 0)
				{
					if(((String) mapMedidores.get("accionMedidor")).equalsIgnoreCase("Encontrado")) {
						
						iIndMedRet = indTmp;
						iNroMedidores++;
						
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("marcaMedidor", (String) mapMedidores.get("marcaMedidor"));
						map.put("modeloMedidor", (String) mapMedidores.get("modeloMedidor"));
						map.put("numeroMedidor", (String) mapMedidores.get("numeroMedidor"));
						
						Map<String, Object> mapOfCharge= new HashMap<String, Object>();
						for (Map.Entry<String, Object> entry : map.entrySet()) {
							String key=entry.getKey();
							String value=entry.getValue().toString();
							mapOfCharge.put(key, value);
						}
						
						Others.arrMedidorRet.add(iIndMedRet,mapOfCharge);
					}
				}else {
					
					return true;
				}
				indTmp++;
			}
			
		logger.info("ImprimirPlano paso 6: {}",Others.vCodOperacion);
		int iContLecturas=0;
		cCadenaLecturasRet="";	
		
		indTmp =0;
		for (Map<String, Object> mapLecturas : Others.qLecturas) {
			iContLecturas++;
			
			if(!(bfnValidarDatos("tipoLectura", (String) mapLecturas.get("tipoLectura"), "COMUN"))) return true;
			if(!(bfnValidarDatos("horarioLectura", (String) mapLecturas.get("horariolectura"), "COMUN"))) return true;
			if(!(bfnValidarDatos("lectura", (String) mapLecturas.get("estadoLeido"), "COMUN"))) return true;
			if(!(bfnValidarDatos("fechalectura", (String) mapLecturas.get("fechalectura"), "COMUN"))) return true;

			
			if (!bfnValidarDominio("MED_TIP_MEDIDA", "COD_TIP_MEDIDA", (String) mapLecturas.get("tipoLectura")))
			{
				Others.lDatosErr++;
				return true;
			}
			if (!bfnValidarDominio("MED_MEDIDA", "COD_MEDIDA", (String) mapLecturas.get("horariolectura")))
			{
				Others.lDatosErr++;
				return true;
			}
			
			for(x=0; x<=iIndMedRet; x++) 
			{

				if(Others.arrMedidorRet.get(x).get("numeroMedidor").equals(mapLecturas.get("numeroMedidor"))
					&& 	Others.arrMedidorRet.get(x).get("marcaMedidor").equals(mapLecturas.get("marcaMedidor"))
					&& Others.arrMedidorRet.get(x).get("modeloMedidor").equals(mapLecturas.get("modeloMedidor"))){
					
					if(! bfnValidarMedidaMedidor((String)Others.arrMedidorRet.get(x).get("marcaMedidor"),
												(String)Others.arrMedidorRet.get(x).get("modeloMedidor"),
												(String) mapLecturas.get("numeroMedidor"),
												(String) mapLecturas.get("horariolectura"))) {
						
						String vstrTmp = String.format("El medidor %s-%s-%s No soporta la medida %s (3)", Others.arrMedidorRet.get(x).get("marcaMedidor"), Others.arrMedidorRet.get(x).get("modeloMedidor"), mapLecturas.get("numeroMedidor"), mapLecturas.get("horariolectura"));
						setErrGen(vstrTmp);
						return true;
					}
					
					if (Others.iIndicaErr == 0)
					{
						cCadenaLecturasRetObs=String.format("%sLectura Medidor %s:%s-%s ",cCadenaLecturasRetObs,(String) mapLecturas.get("numeroMedidor"),(String) mapLecturas.get("horariolectura"),(String) mapLecturas.get("estadoLeido"));
						
					}else {
						return true;
					}
				}
			}
		}//End While Lecturas
		if (iContLecturas<iNroMedidores)
			{
				setErrGen("Faltan lecturas de medidor Encontrado para la orden "+ (String) Others.qDatoClave.get("Codigo_Externo_del_TdC"));
				return true;
			}
		}
	  }//end if orden de mantenimiento no proviene de campo/*roliva*/
		
	   /* Obtener Datos de Acta Revision */
	  if(!(bfnValidarDatos("ID_numero_acta_revision", (String) Others.qActaRevision.get("ID_numero_acta_revision"), "REV"))) return true;
	  if(!(bfnValidarDatos("ID_Cod_Cliente_Vecino_izq_encontrado", (String) Others.qActaRevision.get("ID_Cod_Cliente_Vecino_izq_encontrado"), "REV"))) return true;
      if(!(bfnValidarDatos("ID_Cod_Cliente_Vecino_Derecho_encontrado", (String) Others.qActaRevision.get("ID_Cod_Cliente_Vecino_Derecho_encontrado"), "REV"))) return true;
      if(!(bfnValidarDatos("Direccion_Terreno", (String) Others.qActaRevision.get("Direccion_Terreno"), "REV"))) return true;
      if(!(bfnValidarDatos("Localizacion_Terreno", (String) Others.qActaRevision.get("Localizacion_Terreno"), "REV"))) return true;
      if(!(bfnValidarDatos("ID_Cod_Municipio", (String) Others.qActaRevision.get("ID_Cod_Municipio"), "REV"))) return true;
      if(!(bfnValidarDatos("Nombre_Persona_Atendio", (String) Others.qActaRevision.get("Nombre_Persona_Atendio"), "REV"))) return true;
      if(!(bfnValidarDatos("Cedula_Atendio", (String) Others.qActaRevision.get("Cedula_Atendio"), "REV"))) return true;
      if(!(bfnValidarDatos("Calidad_Atendio", (String) Others.qActaRevision.get("Calidad_Atendio"), "REV"))) return true;
      if(!(bfnValidarDatos("Cantidad_Medidores_No_Instalados", (String) Others.qActaRevision.get("Cant_Medid_No_Instalados"), "REV"))) return true;
      if(!(bfnValidarDatos("Codigo_Description_deficienza", (String) Others.qActaRevision.get("Codigo_Description_deficienza"), "REV"))) return true;
      if(!(bfnValidarDatos("Email_de_contacto_con_cliente", (String) Others.qActaRevision.get("Email_de_contacto_con_cliente"), "REV"))) return true;
      if(!(bfnValidarDatos("Factibilidad_del_Servicio", (String) Others.qActaRevision.get("Factibilidad_del_Servicio"), "REV"))) return true;
      if(!(bfnValidarDatos("EXISTE_RED", (String) Others.qActaRevision.get("EXISTE_RED"), "REV"))) return true;
      if(!(bfnValidarDatos("TIPO_DE_RED", (String) Others.qActaRevision.get("TIPO_DE_RED"), "REV"))) return true;
      if(!(bfnValidarDatos("Tipo_de_Empalme", (String) Others.qActaRevision.get("Tipo_de_Empalme"), "REV"))) return true;
      if(!(bfnValidarDatos("Capacidad_del_Empalme_Encontrado", (String) Others.qActaRevision.get("Capacidad_del_Empalme_Encontrado"), "REV"))) return true;
      if(!(bfnValidarDatos("Proteccion_de_Empalme", (String) Others.qActaRevision.get("Proteccion_de_Empalme"), "REV"))) return true;
      if(!(bfnValidarDatos("Tipo_de_caja", (String) Others.qActaRevision.get("Tipo_de_caja"), "REV"))) return true;
      if(!(bfnValidarDatos("Tipo_de_Conexion", (String) Others.qActaRevision.get("Tipo_de_Conexion"), "REV"))) return true;
      if(!(bfnValidarDatos("Potencia_Solicitada", (String) Others.qActaRevision.get("Potencia_Solicitada"), "REV"))) return true;
      if(!(bfnValidarDatos("Tarifa_Solicitada", (String) Others.qActaRevision.get("Tarifa_Solicitada"), "REV"))) return true;
      if(!(bfnValidarDatos("Numero_Medidor_Cliente_Vecino_Derecho", (String) Others.qActaRevision.get("Nro_Medid_Cli_Vec_Derecho"), "REV"))) return true;
      if(!(bfnValidarDatos("ID_Cod_Marca_Medidor_Vecino_Derecho", (String) Others.qActaRevision.get("ID_Cod_Marca_Medid_Vec_Der"), "REV"))) return true;
      if(!(bfnValidarDatos("Numero_Medidor_Cliente_Vecino_Izquierdo",(String) Others.qActaRevision.get("Nro_Medid_Cli_Vec_Izquierdo"), "REV"))) return true;
      if(!(bfnValidarDatos("ID_Cod_Marca_Medidor_Vecino_Izquierdo", (String) Others.qActaRevision.get("ID_Cod_Marca_Medid_Vec_Izq"), "REV"))) return true;
      if(!(bfnValidarDatos("numero_de_serie_encontrado", (String) Others.qActaRevision.get("numero_de_serie_encontrado"), "REV"))) return true;
      if(!(bfnValidarDatos("marca_encontrado", (String) Others.qActaRevision.get("marca_encontrado"), "REV"))) return true;
      if(!(bfnValidarDatos("estado_encontrado", (String) Others.qActaRevision.get("estado_encontrado"), "REV"))) return true;
      if(!(bfnValidarDatos("factor_encontrado", (String) Others.qActaRevision.get("factor_encontrado"), "REV"))) return true;
      if(!(bfnValidarDatos("amperaje_encontrado", (String) Others.qActaRevision.get("amperaje_encontrado"), "REV"))) return true;
      if(!(bfnValidarDatos("tipo_modelo_encontrado", (String) Others.qActaRevision.get("tipo_modelo_encontrado"), "REV"))) return true;
      if(!(bfnValidarDatos("fase_encontrado", (String) Others.qActaRevision.get("fase_encontrado"), "REV"))) return true;
      if(!(bfnValidarDatos("constantes_encontrado", (String) Others.qActaRevision.get("constantes_encontrado"), "REV"))) return true;
      if(!(bfnValidarDatos("sellos_candados_1", (String) Others.qActaRevision.get("sellos_candados_1"), "REV"))) return true;
      if(!(bfnValidarDatos("sellos_bornera_1", (String) Others.qActaRevision.get("sellos_bornera_1"), "REV"))) return true;
      if(!(bfnValidarDatos("sellos_candados_2", (String) Others.qActaRevision.get("sellos_candados_2"), "REV"))) return true;
      if(!(bfnValidarDatos("sellos_bornera_2", (String) Others.qActaRevision.get("sellos_bornera_2"), "REV"))) return true;
      if(!(bfnValidarDatos("numero_de_serie_instalado", (String) Others.qActaRevision.get("numero_de_serie_instalado"), "REV"))) return true;
      if(!(bfnValidarDatos("marca_instalado", (String) Others.qActaRevision.get("marca_instalado"), "REV"))) return true;
      if(!(bfnValidarDatos("estado_instalado", (String) Others.qActaRevision.get("estado_instalado"), "REV"))) return true;
      if(!(bfnValidarDatos("factor_instalado", (String) Others.qActaRevision.get("factor_instalado"), "REV"))) return true;
      if(!(bfnValidarDatos("Capacidad_instalado", (String) Others.qActaRevision.get("Capacidad_instalado"), "REV"))) return true;
      if(!(bfnValidarDatos("amperaje_isntalado", (String) Others.qActaRevision.get("amperaje_isntalado"), "REV"))) return true;
      if(!(bfnValidarDatos("tipo_modelo_instalado", (String) Others.qActaRevision.get("tipo_modelo_instalado"), "REV"))) return true;
      if(!(bfnValidarDatos("fase_instalado", (String) Others.qActaRevision.get("fase_instalado"), "REV"))) return true;
      if(!(bfnValidarDatos("constantes_instalado", (String) Others.qActaRevision.get("constantes_instalado"), "REV"))) return true;   
      if(!(bfnValidarDatos("interruptor_termomagnetico_marca", (String) Others.qActaRevision.get("interrup_termomag_marca"), "REV"))) return true;
      if(!(bfnValidarDatos("interruptor_termomagnetico_fase", (String) Others.qActaRevision.get("interrup_termomag_fase"), "REV"))) return true;
      if(!(bfnValidarDatos("interruptor_termomagnetico_amperaje", (String) Others.qActaRevision.get("interrup_termomag_amperaje"), "REV"))) return true;
      if(!(bfnValidarDatos("carga_tomada_r", (String) Others.qActaRevision.get("carga_tomada_r"), "REV"))) return true;
      if(!(bfnValidarDatos("carga_tomada_s", (String) Others.qActaRevision.get("carga_tomada_s"), "REV"))) return true;
      if(!(bfnValidarDatos("carga_tomada_t", (String) Others.qActaRevision.get("carga_tomada_t"), "REV"))) return true;
      if(!(bfnValidarDatos("prueba_de_aislamiento", (String) Others.qActaRevision.get("prueba_de_aislamiento"), "REV"))) return true;
      if(!(bfnValidarDatos("valor", (String) Others.qActaRevision.get("valor"), "REV"))) return true;
	
	  logger.info("ImprimirPlano paso 11: {}",Others.vCodOperacion);
	   if(Others.iIndicaErr == 0) {
		   
		   Others.vCodigoContratista="";
		   //Se cambia valor por codigo de cuadrilla generica 
		   Others.qDatoClave.put("Codigo_Cuadrilla", Others.cCodCuadrillaGen);
		   
		 if(!((String)Others.qDatoClave.get("Codigo_Cuadrilla")).equalsIgnoreCase(""))
		 {
			 if(!bfnObtenerCodigoContratista(Others.cCodCuadrillaGen))
		      {	
				 logger.error(String.format("Error: No se encuentra configurado codigo contratista para cuadrilla o no está asignado a ORM %s", (String)Others.qDatoClave.get("Codigo_Cuadrilla")));
				 setErrGen("No se encuentra configurado codigo contratista para cuadrilla o no está asignado a ORM "+ (String)Others.qDatoClave.get("Codigo_Cuadrilla"));
				 return true;
		      }
		 }else {
			 logger.error(String.format("Error: No se encuentra configurado codigo contratista para cuadrilla o no está asignado a ORM %s", (String)Others.qDatoClave.get("Codigo_Cuadrilla")));
			 setErrGen("Se necesita valor de forma obligatoria Cod de Cuadrilla"+(String)Others.qDatoClave.get("Codigo_Cuadrilla"));
			 return true;
		 }

		 logger.info("ImprimirPlano paso 11.a: {}",Others.vCodOperacion);

		 
		//  logger.info("Calidad_Atendio: {}", Others.qActaRevision.get("Calidad_Atendio"));
		 logger.info("scom_Calidad_Atendio_p1: {}", Others.qActaRevision.get("Calidad_Atendio"));

		 if((Others.qActaRevision.get("Calidad_Atendio")==null?"":(String)Others.qActaRevision.get("Calidad_Atendio")).length()>0)
			{
				logger.info("Entro a la validacion de parentesco EOrderCNXRecepORMyFact scom");
			 
			 if(!((String) Others.qActaRevision.get("Calidad_Atendio")).equalsIgnoreCase("1") &&
					!((String) Others.qActaRevision.get("Calidad_Atendio")).equalsIgnoreCase("2") &&
					  !((String) Others.qActaRevision.get("Calidad_Atendio")).equalsIgnoreCase("3") &&
					     !((String) Others.qActaRevision.get("Calidad_Atendio")).equalsIgnoreCase("4") &&
					        !((String) Others.qActaRevision.get("Calidad_Atendio")).equalsIgnoreCase("5") &&
					        	!((String) Others.qActaRevision.get("Calidad_Atendio")).equalsIgnoreCase("6")) {
				 
				 setErrGen(String.format("Código de Parentesco [%s] No Válido.\n", (String) Others.qActaRevision.get("Calidad_Atendio")));
				 return true;
			 }else {
				 if(((String) Others.qActaRevision.get("Calidad_Atendio")).equalsIgnoreCase("1")) {Others.qActaRevision.put("Calidad_Atendio", "T");}
				 if(((String) Others.qActaRevision.get("Calidad_Atendio")).equalsIgnoreCase("2")) {Others.qActaRevision.put("Calidad_Atendio", "I");}
				 if(((String) Others.qActaRevision.get("Calidad_Atendio")).equalsIgnoreCase("3")) {Others.qActaRevision.put("Calidad_Atendio", "V");}
				 if(((String) Others.qActaRevision.get("Calidad_Atendio")).equalsIgnoreCase("4")) {Others.qActaRevision.put("Calidad_Atendio", "P");}
				 if(((String) Others.qActaRevision.get("Calidad_Atendio")).equalsIgnoreCase("5")) {Others.qActaRevision.put("Calidad_Atendio", "M");}
				 if(((String) Others.qActaRevision.get("Calidad_Atendio")).equalsIgnoreCase("6")) {Others.qActaRevision.put("Calidad_Atendio", "N");}
			 }
			}
			else
			{
				Others.qActaRevision.put("Calidad_Atendio", "N");
			}

			logger.info("scom_Calidad_Atendio_p2: {}", Others.qActaRevision.get("Calidad_Atendio"));
			logger.info("ImprimirPlano paso 11.b: {}",Others.vCodOperacion);
		 Others.vFecha="";
		 Others.vHora="";
		 Others.vMinuto="";
		 
		 if(((String)Others.qDatoClave.get("Inicio_TdC")).length()>0) {
			 if(!bfnGetFechaHoraEje((String)Others.qDatoClave.get("Inicio_TdC")))
			  {
				 logger.error(String.format("Error: No se puede obtener fecha-hora-minuto desde [%s]", (String)Others.qDatoClave.get("Inicio_TdC")));
				 Others.lDatosErr++;
				 Others.iIndicaErr=1;
			  }
		 }
		 logger.info("ImprimirPlano paso 11.c: {}",Others.vCodOperacion);
		 if(((String)Others.qDatoClave.get("TDC_Creado_Eorder")).trim().equalsIgnoreCase("1") && 
				 ((String)Others.qDatoClave.get("TDC_Creado_Eorder")).equalsIgnoreCase("NRE")) {
			 
			 logger.error("El Codigo_Resultado no puede ser NRE para una orden creada en campo.");
			 setErrGen("El Codigo_Resultado no puede ser NRE para una orden creada en campo.");
			 return true;
		 }
		 logger.info("ImprimirPlano paso 11.d: {}",Others.vCodOperacion);
		 Others.vCodigoResultado="";
		 if(((String)Others.qDatoClave.get("Codigo_Resultado")).equalsIgnoreCase("REA")) {
			 Others.vCodigoResultado="SI";
			 Others.qDatoClave.put("Codigo_Resultado", "");
		 }
		 logger.info("ImprimirPlano paso 11.e: {}",Others.vCodOperacion);
		 if(((String)Others.qDatoClave.get("Codigo_Resultado")).equalsIgnoreCase("NRE")) {
			 Others.vCodigoResultado="NO";
		 }
		 logger.info("ImprimirPlano paso 11.f: {}",Others.vCodOperacion);
		 if(Others.qActaRevision.get("ID_numero_acta_revision") == null || ((String)Others.qActaRevision.get("ID_numero_acta_revision")).equalsIgnoreCase("")) {
			 logger.error("Error: El campo ID_numero_acta_revision no puede ser nulo en {}",Others.vIdOrdTransfer);
			 setErrGen("Error: El campo ID_numero_acta_revision no puede ser nulo en "+Others.vIdOrdTransfer);
			 return true;
		 }
		 logger.info("ImprimirPlano paso 11.g: {}",Others.vCodOperacion);
		 if(((String)Others.qDatoClave.get("TDC_Creado_Eorder")).trim().equalsIgnoreCase("1")) { //ordenes creadas en campo
			 /* Datos ORM*/
			 logger.info("ImprimirPlano paso 11.1: {}",Others.vCodOperacion);
			 if(Others.qORM==null || Others.qORM.isEmpty()) {
					logger.error("Error al obtener los datos motivo,tema y trabajo ORM en {}",Others.vIdOrdTransfer);
					return false;
				}
				logger.info("ImprimirPlano paso 11.h: {}",Others.vCodOperacion);
			 if((Others.qORM.get("motivo")==null?"":(String)Others.qORM.get("motivo")).equalsIgnoreCase("")) {
				 logger.error("Error: No se encuentra configurado el Motivo de ORM en {}",Others.vIdOrdTransfer);
				 setErrGen("No se encuentra configurado el Motivo de ORM en "+Others.vIdOrdTransfer);
				 return true;
			 }
			 logger.info("ImprimirPlano paso 11.i: {}",Others.vCodOperacion);
			 if((Others.qORM.get("tema")==null?"":(String)Others.qORM.get("tema")).equalsIgnoreCase("")) {
				 logger.error("Error: No se encuentra configurado el Tema de ORM en {}",Others.vIdOrdTransfer);
				 setErrGen("No se encuentra configurado el Tema de ORM en "+Others.vIdOrdTransfer);
				 return true;
			 }
			 logger.info("ImprimirPlano paso 11.j: {}",Others.vCodOperacion);
			 if((Others.qORM.get("trabajo")==null?"":(String)Others.qORM.get("trabajo")).equalsIgnoreCase("")) {
				 logger.error("Error: No se encuentra configurado el codigo de trabajo de ORM en {}",Others.vIdOrdTransfer);
				 setErrGen("No se encuentra configurado el codigo de trabajo de ORM en "+Others.vIdOrdTransfer);
				 return true;
			 }
			 logger.info("ImprimirPlano paso 11.k: {}",Others.vCodOperacion);
			 if((Others.qORM.get("nro_cuenta")==null?"":(String)Others.qORM.get("nro_cuenta")).equalsIgnoreCase("")) {
				 logger.error("Error: No se encuentra configurado el Nro de Cuenta de ORM en {}",Others.vIdOrdTransfer);
				 setErrGen("No se encuentra configurado el Nro de Cuenta de ORM en "+Others.vIdOrdTransfer);
				 return true;
			 }
			 
			 logger.info("ImprimirPlano paso 11.2: {}",Others.vCodOperacion);
			 //MVC se ha reemplzado nrode servicio por Nro cuenta --1490 a 1505 Archivo C
			 Others.arrParamORM=new HashMap<>();
			 if(!bfnValidarDatosORM(Others.vTipoRespon,Others.vRolRespon,
					 			(String)Others.qORM.get("motivo"),(String)Others.qORM.get("nro_cuenta"),(String)Others.qORM.get("trabajo"),Others.vCodPrior,
					 			(String)Others.qORM.get("tema"),(String)Others.qDatoClave.get("Codigo_Tipo_de_TdC"),(String)Others.qDatoClave.get("Codigo_Interno_del_TdC"))){
				 
				 if(Others.iflagerror == 0) {
					 logger.error("Error de base de datos al validar ORM");
					 return false;
				 }else if(Others.iflagerror == 1) {
					 logger.error("Error: No se pudo validar ORM");
					 setErrGen("No se pudo validar ORM"+Others.vIdOrdTransfer);
					 return true;
				 } 
			 }

			  //Si la orden creada en campo NO fue procesada previamente
			 if((Others.arrParamORM.get("lNroOrden")==null?"":Others.arrParamORM.get("lNroOrden").toString()).equalsIgnoreCase("0"))
			  {
				//bloque transaccional - si no se cumple se ejecuta un rollback
				  executeProcesosTrans();
			  }
		 
			 Others.qDatoClave.put("Codigo_Externo_del_TdC", "");
			 Others.qDatoClave.put("Codigo_Externo_del_TdC", Others.arrParamORM.get("lNroOrden")==null?"":Others.arrParamORM.get("lNroOrden").toString());
			 Others.vNroCuenta = Long.parseLong((String)Others.arrParamORM.get("iNroCuenta"));
		 } /*Condicion adicional si la orden es creada en campo - roliva*/
		 
		 String c_Observaciones2="";
		 
		 if(cCadenaLecturasRetObs.length()>0) {
				c_Observaciones2=String.format("%s.%s", cCadenaLecturasRetObs, (String) Others.qOper.get("Notas_Operacion"));
		 }else {
				c_Observaciones2=(String) Others.qOper.get("Notas_Operacion");
		 }
		 logger.info("ImprimirPlano paso 11.3: {}",Others.vCodOperacion);


		 logger.info("scom_c_Observaciones2: {}", c_Observaciones2);
		 
		 logger.info(String.format("\n %s-%d-%s-%s-%s-%s-%s-%s-%s-DNI-%s-%s-%s-%s-%s-%s-%s-%s\n",
				 (String) Others.qDatoClave.get("Codigo_Externo_del_TdC"),
			      Others.vNroCuenta,
			      (String) Others.qActaRevision.get("ID_numero_acta_revision"),
			      Others.vCodigoContratista,
			      Others.vCodEjecutor,
			      Others.vFecha,
			      Others.vHora,
			      Others.vMinuto,
			      (String) Others.qActaRevision.get("Nombre_Persona_Atendio"),
			      (String) Others.qActaRevision.get("Cedula_Atendio"),
			      (String) Others.qActaRevision.get("Calidad_Atendio"),
			      Others.vCodigoResultado,
			      (String) Others.qDatoClave.get("Codigo_Causal_Resultado"),
			      c_Observaciones2, //arrOperacion.c_Notas_Operacion,
				  cCadenaTareas,
				  cCadenaCambioMedidor.equals("")?"null":cCadenaCambioMedidor,
				  cCadenaContrasteMedidor.equals("")?"null":cCadenaContrasteMedidor));
		 
		 		  cCadenaTareas=cCadenaTareas.replaceAll("#", "");
		 		  
				   logger.info("Se procedera a imprimir la linea en el archivo");

		 		 Others.printWriterinFile2.printf("%s%s%s%s%d%s%s%s%s%s%s%s%s%s%s%s%s%s%s%sDNI%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n",
		 				"flag_SCOM",
		 				  Constants.SC,
		 				 (String) Others.qDatoClave.get("Codigo_Externo_del_TdC"),
		 				  Constants.SC,
		 				 Others.vNroCuenta,
		 			     Constants.SC,
		 			    (String) Others.qActaRevision.get("ID_numero_acta_revision"),
		 			     Constants.SC,
		 			    Others.vCodigoContratista,
		 			     Constants.SC,
		 			    Others.vCodEjecutor,
		 				  //vTecnico,
		 			     Constants.SC,
		 			    Others.vFecha,
		 			     Constants.SC,
		 			    Others.vHora,
		 			     Constants.SC,
		 			    Others.vMinuto,
		 			     Constants.SC,
		 			    (String) Others.qActaRevision.get("Nombre_Persona_Atendio"),
		 			     Constants.SC,
		 			    Constants.SC,
		 			   (String) Others.qActaRevision.get("Cedula_Atendio"),
		 			     Constants.SC,
		 			    (String) Others.qActaRevision.get("Calidad_Atendio"),
		 			     Constants.SC,
		 			    Others.vCodigoResultado,
		 			     Constants.SC,
		 			    (String) Others.qDatoClave.get("Codigo_Causal_Resultado"),
		 			     Constants.SC,
		 			      c_Observaciones2, //arrOperacion.c_Notas_Operacion,
		 			     Constants.SG,
		 				  cCadenaTareas,
		 				 Constants.SG,
		 				  cCadenaCambioMedidor.equals("")?"null":cCadenaCambioMedidor,
		 				 Constants.SG,
		 				  cCadenaContrasteMedidor.equals("")?"null":cCadenaContrasteMedidor);

						  logger.info("Linea en archivo impreso");
		 		 
		 		Others.lFilasArchivoORM++;
	   }
		return true;
	}
	
	@Transactional
	private boolean executeProcesosTrans() {
		boolean var= true;
		 /*Crear registro de Workflow*/
		  if (!bfnCrearIdWorkflow())
		  {
			  logger.error("Error al Crear registro de workflow");
		      return false;
		  }
		  /*Crear orden Generica*/
		  if (!bfnCrearOrdenGenerico())
		  {
			  logger.error("Error al Crear Orden Generica");
		      return false;
		  }
		  /*Crear orden Masiva*/
		  if (!bfnCrearOrdenMasiva())
		  {
			  logger.error("Error al Crear Orden Masiva");
		      return false;
		  }
		  /*Crear orden Derivada*/
		  if (!bfnCrearOrdenDerivada())
		  {
			  logger.error("Error al Crear Orden Derivada");
		      return false;
		  }
		  
		  if (!bfnActualizaNroOrdenLegacy())
		  {
			  logger.error("Error: No se pudo actualizar Nro Orden Legacy para orden {}"+(String) Others.qDatoClave.get("Codigo_Externo_del_TdC"));
			  setErrGen("Error: No se pudo actualizar Nro Orden Legacy para orden "+(String) Others.qDatoClave.get("Codigo_Externo_del_TdC"));
			  return true;
		  }
		  return var;
	}

	private boolean bfnValidarDatosORM(String aCodTipoResponsable, String aCodResponsable, String aCodMotivo, String aNroCuenta,
			String aCodTrabajo, String aCodPrioridad, String aCodTema, String aCodigo_Tipo_de_TdC, String aCodigo_Interno_del_TdC) {

		int iflagerrorH=0;
		String iIdUsuarioH=String.valueOf(Others.gIdUsuario);
		
		String iIdMotivoH=printplanoScomDAO.getiIdMotivoH(aCodMotivo);
		if(iIdMotivoH == null || iIdMotivoH.length() == 0) {
			logger.error("No se encontro el Id Motivo");
			iflagerrorH=1;
			Others.iflagerror=iflagerrorH;
			return false;
		}
		
		String iIdBuzonH="";
		String iIdResponsableH="";
		if(aCodTipoResponsable.equalsIgnoreCase("Rol")) {
			Map<String, Object> map = printplanoScomDAO.getiIdResponsableH(aCodResponsable,aCodTipoResponsable);
			if(map==null || map.isEmpty()) {
				logger.error("No se encontro el Id Buzon y Id Responsable");
				iflagerrorH=1;
				Others.iflagerror=iflagerrorH;		
				return false;
			}else {
				iIdBuzonH=map.get("iIdBuzonH").toString();
				iIdResponsableH=map.get("iIdResponsableH").toString();
			}	
		}
		
		String iIdTrabajoH=printplanoScomDAO.getiIdTrabajoH(aCodTrabajo);
		if(iIdTrabajoH == null || iIdTrabajoH.length() == 0) {
			logger.error("No se encontro el Id Trabajo");
			iflagerrorH=1;
			Others.iflagerror=iflagerrorH;
			return false;
		}
		
		String iIdPrioridadH=printplanoScomDAO.getiIdPrioridadH(aCodPrioridad);
		if(iIdPrioridadH == null || iIdPrioridadH.length() == 0) {
			logger.error("No se encontro el Id Prioridad");
			iflagerrorH=1;
			Others.iflagerror=iflagerrorH;
			return false;
		}
		
		String iIdTemaH=printplanoScomDAO.getiIdTemaH(aCodTema);
		if(iIdTemaH == null || iIdTemaH.length() == 0) {
			logger.error("No se encontro el Id Tema");
			iflagerrorH=1;
			Others.iflagerror=iflagerrorH;
			return false;
		}
		
		String iNroCuentaH="";
		String iIdServicioH="";
		
		Map<String, Object> mapX = printplanoScomDAO.getiNroCuentaHandiIdServicioH(aNroCuenta);
		if(mapX==null || mapX.isEmpty()) {
			logger.error("No se encontro servicio electrico para la Cuenta");
			iflagerrorH=1;
			Others.iflagerror=iflagerrorH;		
			return false;
		}else {
			iNroCuentaH=mapX.get("nro_cuenta").toString();
			iIdServicioH=mapX.get("id_servicio").toString();
		}	
		
		String aFecha=printplanoScomDAO.getSysDate();
		if(aFecha == null || aFecha.length() == 0) {
			logger.error("Error en extraer fecha actual");
			Others.iflagerror=iflagerrorH;
			return false;
		}
		
		String aNroOrdenH="0";
		aNroOrdenH=printplanoScomDAO.getNroOrdenH(aCodigo_Interno_del_TdC,aCodigo_Tipo_de_TdC);
		if(aNroOrdenH == null) {
			logger.info("Orden Creada en Campo No se ha recepcionado aún");
			Others.iflagerror=iflagerrorH;
			aNroOrdenH="0";
			return false;
		}

		Others.arrParamORM.put("iIdResponsable", iIdResponsableH);
		Others.arrParamORM.put("iIdBuzon", iIdBuzonH);
		Others.arrParamORM.put("iIdMotivo", iIdMotivoH);
		Others.arrParamORM.put("iIdUsuario", iIdUsuarioH);
		Others.arrParamORM.put("iIdServicio", iIdServicioH);
		Others.arrParamORM.put("iIdTrabajo", iIdTrabajoH);
		Others.arrParamORM.put("iIdPrioridad", iIdPrioridadH);
		Others.arrParamORM.put("iIdTema", iIdTemaH);
		Others.arrParamORM.put("iNroCuenta", iNroCuentaH);
		Others.arrParamORM.put("lNroOrden", Long.parseLong(aNroOrdenH));
		
		return true;
	}


	private boolean bfnActualizaNroOrdenLegacy() {
		String cNroOrdenLegacyH = Others.arrParamORM.get("lNroOrden")==null?"":Others.arrParamORM.get("lNroOrden").toString();
		if(!cNroOrdenLegacyH.equals("")) {
			boolean update_eord_ord_transfer=printplanoScomDAO.bfnActualizaNroOrdenLegacy(cNroOrdenLegacyH,Others.vIdOrdTransfer);
			if(update_eord_ord_transfer) {
				logger.info("Nro Orden Legacy = {} para {}",cNroOrdenLegacyH,Others.vIdOrdTransfer);
				return true;
			}
		}
		return false;
	}


	private boolean bfnCrearOrdenDerivada() {
		Long iIdOrdenH=(Long) Others.arrParamORM.get("iIdOrden");
		Long iIdResponsableH=(Long) Others.arrParamORM.get("iIdResponsable");
		
		boolean saveOrdenDerivada=printplanoScomDAO.bfnCrearOrdenDerivada(iIdOrdenH,iIdResponsableH,iIdResponsableH);
		if(saveOrdenDerivada) {
			logger.info("Orden derivada: IdOrden = {} -- IdResponsable = {}",iIdOrdenH,iIdResponsableH);
			return true;
		}
		
		return false;
	}


	private boolean bfnCrearOrdenMasiva() {
		Long iIdOrdenH=(Long) Others.arrParamORM.get("iIdMotivo");
		Long iIdTrabajoH=(Long) Others.arrParamORM.get("iIdServicio");
		Long iIdPrioridadH=(Long) Others.arrParamORM.get("iIdUsuario");
		Long iIdTemaH=(Long) Others.arrParamORM.get("iIdBuzon");
		
		boolean saveOrdenMasiva=printplanoScomDAO.bfnCrearOrdenMasiva(iIdOrdenH,iIdTrabajoH,iIdPrioridadH,iIdTemaH);
		if(saveOrdenMasiva) {
			logger.info("Orden masiva: IdOrden = {} -- IdTrabajo = {} -- IdPrioridad = {} -- IdTema = {}",iIdOrdenH,iIdTrabajoH,iIdPrioridadH,iIdTemaH);;
			return true;
		}
		return false;
	}


	private boolean bfnCrearOrdenGenerico() {
		Long iIdWorkflowH=(Long) Others.arrParamORM.get("iIdWorkflow");
		Long lNroOrdenH=0L;
		Long iIdOrdenH=0L;
		Long iIdMotivoH=(Long) Others.arrParamORM.get("iIdMotivo");
		Long iIdServicioH=(Long) Others.arrParamORM.get("iIdServicio");
		Long iIdUsuarioH=(Long) Others.arrParamORM.get("iIdUsuario");
		Long iIdBuzonH=(Long) Others.arrParamORM.get("iIdBuzon");
		
		lNroOrdenH=printplanoScomDAO.getNextValSq_OrdenMantenimiento();
		
		iIdOrdenH=printplanoScomDAO.getNextValSq_Orden();
		
		if((lNroOrdenH==0L && lNroOrdenH==null)|| (iIdOrdenH==0L && lNroOrdenH==null)) {
			return false;
		}
		
		boolean saveOrd_Orden=printplanoScomDAO.bfnCrearOrdenGenerico(lNroOrdenH,iIdWorkflowH,iIdOrdenH,iIdMotivoH,iIdServicioH,iIdUsuarioH,iIdBuzonH);
		if(saveOrd_Orden) {
			Others.arrParamORM.put("lNroOrden",lNroOrdenH);
			Others.arrParamORM.put("iIdOrden",iIdOrdenH);
			logger.info("Orden generica: NroOrden = {} -- IdOrden = {}",lNroOrdenH,iIdOrdenH);
			return true;
		}
		return false;
	}

	
	private boolean bfnCrearIdWorkflow() {
		Long nextValue_sqworkflow=printplanoScomDAO.getMaxIdWorkFlow();
		if(nextValue_sqworkflow==0L || nextValue_sqworkflow==null) {
			return false;
		}
		
		boolean saveIdWorkflow=printplanoScomDAO.bfnCrearIdWorkflow(nextValue_sqworkflow);
		if(saveIdWorkflow) { 
			logger.info("iIdWorkflow = {}",nextValue_sqworkflow);
			Others.arrParamORM.put("iIdWorkflow",nextValue_sqworkflow);
			return true;
		}
		return false;
	}


	private boolean bfnGetFechaHoraEje(String pFechaLarga) {
		
		Map<String, Object> resultFecha=printplanoScomDAO.bfnGetFechaHoraEje(pFechaLarga);
		if(resultFecha==null || resultFecha.isEmpty()) {
			return false;
		}else{
			Others.vFecha=(String)resultFecha.get("fechacorta");
			 Others.vHora=(String)resultFecha.get("hora");
			 Others.vMinuto=(String)resultFecha.get("minutes");
			return true;
		}
	}


	private boolean bfnObtenerCodigoContratista(String cCodCuadrillaGen) {
		String codContratista=printplanoScomDAO.bfnObtenerCodigoContratista(cCodCuadrillaGen);
		if(codContratista == null || codContratista.length() == 0) {
			return false;
		}else {
			Others.vCodigoContratista=codContratista;
			return true;
		}
	}


	private boolean bfnValidarMedidaMedidor(String pCodMarca, String pCodModelo, String pNroComp, String pMedida) {
		int lCount=0;
		
		lCount =printplanoScomDAO.bfnValidarMedidaMedidor(pCodMarca,pCodModelo,pNroComp,pMedida);
		if(lCount==0) {
			return false;
		}
		if(lCount>0) {
			return true;
		}else {
			return false;
		}
	}


	private boolean bfnValidarCompInstall(String pCodMarca, String pCodModelo, String pNroComp) {
		boolean bResult=true;
		int lCount=0;
		
		lCount =printplanoScomDAO.bfnValidarCompInstall(pCodMarca,pCodModelo,pNroComp);
		if(lCount==0) {
			bResult=false;
		}
		return bResult;
	}


	private boolean bfnValidarCompRet(String pCodMarca, String pCodModelo, String pNroComp, Long pNroCuenta) {
		boolean bResult=true;
		int lCount=0;
		
		lCount =printplanoScomDAO.bfnValidarCompRet(pNroCuenta,pCodMarca,pCodModelo,pNroComp);
		logger.info("bfnValidarCompRet lCount scom: {}", lCount);
		if(lCount==0) {
			bResult=false;
		}
		return bResult;
	}



	private boolean ObtenerFechaDDMMYYY(String cFechaYYYYMMDD) {
		logger.info("ObtenerFechaDDMMYYY({})", cFechaYYYYMMDD);
		Others.fechaObtenida = printplanoScomDAO.obtenerFechaDDMMYYY(cFechaYYYYMMDD);
		logger.info("Others.fechaObtenida: {}", Others.fechaObtenida);
		return Others.fechaObtenida != null;
	}



	private boolean bfnGetCodEjeGen(String cCodCuadrillaGen) {
		Map<String, Object>mapCodEje= new HashMap<>();
		mapCodEje=printplanoScomDAO.bfnGetCodEjeGen(cCodCuadrillaGen);
		if(mapCodEje==null || mapCodEje.isEmpty()) {
			return false;
		}else {
			Others.vCodEjecutor=(String)mapCodEje.get("cod_ejecutor");
			Others.vTecnico=(String)mapCodEje.get("nombre");
			return true;
		}
	}



	private int bfnReqCambioMed(String pCodigo_Tarea) {
		return printplanoScomDAO.bfnReqCambioMed(pCodigo_Tarea);
	}

	private boolean bfnValidarTareas(String pCodigo_Tarea, String pCondicion_Tarea, String pCantidad, String pCodigo_Resultado) {
		logger.info("pCodigo_Tarea: {}", pCodigo_Tarea);
		logger.info("pCondicion_Tarea: {}", pCondicion_Tarea);
		logger.info("pCantidad: {}", pCantidad);
		logger.info("pCodigo_Resultado: {}", pCodigo_Resultado);


		if(
			pCodigo_Tarea==null || pCondicion_Tarea==null || pCantidad==null || pCodigo_Resultado==null ||
			pCodigo_Tarea.length()==0 || pCondicion_Tarea.length()==0 || pCantidad.length()==0 || pCodigo_Resultado.length()==0
		) {
			setErrGen("No se ha ingresado datos de tareas ");
			return false;
		}
		if(!bfnValidarDominio("ORM_TAREA", "CODE", pCodigo_Tarea))
		  {
			Others.lDatosErr++;
			return false;
		  }
		if(!pCondicion_Tarea.startsWith("C") && !pCondicion_Tarea.startsWith("P") && !pCondicion_Tarea.startsWith("I") && !pCondicion_Tarea.startsWith("R")){
			setErrGen("Valor No válido de Identificador de Tarea "+pCondicion_Tarea);
			return false;
		}
		if(Long.parseLong(pCantidad)<1) {
			setErrGen("Valor No válido de Cantidad de Tarea "+pCantidad);
			return false;
		}
		if(!pCodigo_Resultado.equalsIgnoreCase("S") && !pCodigo_Resultado.equalsIgnoreCase("N")) {
			setErrGen("Valor No válido de Resultado de ejecucion "+pCodigo_Resultado);
			return false;
		}
		return true;
	}

	private boolean bfnValidarDominio(String pNomTabla, String pNomCampo, String pValorCampo) {
		
		logger.info("bfnValidarDominio inicio");
		boolean bResult=true;
		int iCount=0;
		
		if(pValorCampo == null || pValorCampo.length()==0) {
		logger.info("bfnValidarDominio bResult: {}", bResult);

			logger.info("bfnValidarDominio fin");

			return bResult;
		}
		
		iCount =printplanoScomDAO.bfnValidarDominio(pNomTabla,pNomCampo,pValorCampo);
		if(iCount==0) {
			bResult=false;
		}
		
		if(!bResult){
			Others.vCodOperacion=Others.vCodErrASY045;
			Others.vObservacion=Others.vDesErrASY045 + pNomCampo + pValorCampo ;
		}
		logger.info("bfnValidarDominio bResult: {}", bResult);
		logger.info("bfnValidarDominio fin");

		return bResult;
	}

	private boolean bfnObtenerNroCuenta(String pKey) {
		Others.vNroCuenta=printplanoScomDAO.bfnObtenerNroCuenta(pKey);
		if(Others.vNroCuenta!=0L) {
			return true;
		}
		return false;
	}

	private boolean bfnValidarORMExist(String c_Codigo_Externo_del_TdC) {
		int searchIfORMExist=printplanoScomDAO.searchIfORMexist(c_Codigo_Externo_del_TdC);
		if(searchIfORMExist>0) {
			return true;
		}
		return false;
	}

	private void setErrGen(String pErrorNCat) {
		
		Others.vCodOperacion=Others.vCodErrASY099;
		Others.vObservacion=Others.vDesErrASY099+" "+pErrorNCat ;
		
		Others.printWriterinFile1.printf("%s Línea %d %s Error - <%s>%n", Others.gcNum_Orden, Others.lFilasProc, Others.vCodTipoOrdenEorder,pErrorNCat);
		logger.info(String.format("%s Línea %d %s Error - <%s>%n", Others.gcNum_Orden, Others.lFilasProc, Others.vCodTipoOrdenEorder,pErrorNCat));
		
		Others.iIndicaErr=1;
		Others.lDatosErr++;
	}

	private boolean bfnValidarDatos(String pTag, String pDato, String pTipoTag) {
		int x;
		if(pDato==null) {
			pDato="";
		}else {
			pDato=pDato.trim();
		}
		if(pTipoTag.equalsIgnoreCase("COMUN")) {
			for(x=0; x<Others.iTotTagComun; x++)
			{
				if(((String)Others.qTagComun.get(x).get("NOM_TAG")).equalsIgnoreCase(pTag))
				{
					if(((String)Others.qTagComun.get(x).get("EDELNOR")).equalsIgnoreCase("S"))
					{
						if(((String)Others.qTagComun.get(x).get("OBLIGATORIO")).equalsIgnoreCase("S"))
						{
							if(pDato.equalsIgnoreCase("") || pDato.length()<1) {
								Others.printWriterinFile1.printf("%s Línea %d %s Error - <%s> necesita valor de forma obligatoria %s%n", 
										Others.gcNum_Orden, Others.lFilasProc, Others.vCodTipoOrdenEorder, (String)Others.qTagComun.get(x).get("NOM_TAG"), pDato);
								logger.info(String.format("%s Línea %d %s Error - <%s> necesita valor de forma obligatoria %s%n", 
										Others.gcNum_Orden, Others.lFilasProc, Others.vCodTipoOrdenEorder, (String)Others.qTagComun.get(x).get("NOM_TAG"), pDato));
								
								Others.vCodOperacion=Others.vCodErrASY013;
								Others.vObservacion=Others.vDesErrASY013 +" "+ (String)Others.qTagComun.get(x).get("NOM_TAG");
								Others.lDatosErr++;
								Others.iIndicaErr=1;
								return false;
							}else {
								if(valida_formato(pDato,(String)Others.qTagComun.get(x).get("TIPO"))==false) {
									Others.printWriterinFile1.printf("%s Línea %d %s Error - <%s> necesita valor de forma obligatoria %s%n", 
											Others.gcNum_Orden, Others.lFilasProc, Others.vCodTipoOrdenEorder, (String)Others.qTagComun.get(x).get("NOM_TAG"), pDato);
									logger.info(String.format("%s Línea %d %s Error - <%s> necesita valor de forma obligatoria %s%n", 
											Others.gcNum_Orden, Others.lFilasProc, Others.vCodTipoOrdenEorder, (String)Others.qTagComun.get(x).get("NOM_TAG"), pDato));
									
									Others.vCodOperacion=Others.vCodErrASY024;
									Others.vObservacion=Others.vDesErrASY024 +" "+ (String)Others.qTagComun.get(x).get("NOM_TAG");
									Others.lDatosErr++;
									Others.iIndicaErr=1;
									return false;
								}
								break;
							}
						}else
						{	break;
						}
					}
				}
				
			}
		}else if(pTipoTag.equalsIgnoreCase("REV")){
			for(x=0; x<Others.iTotTagActaRevision; x++)
			{
				if(((String)Others.qTagActaRevision.get(x).get("NOM_TAG")).equalsIgnoreCase(pTag))
				{	
					if(((String)Others.qTagActaRevision.get(x).get("EDELNOR")).equalsIgnoreCase("S"))
					{
						if(((String)Others.qTagActaRevision.get(x).get("OBLIGATORIO")).equalsIgnoreCase("S"))
						{
							if(pDato.equalsIgnoreCase("") || pDato.length()<1) {
								Others.printWriterinFile1.printf("%s Línea %d %s Error - <%s> necesita valor de forma obligatoria %s%n", 
										Others.gcNum_Orden, Others.lFilasProc, Others.vCodTipoOrdenEorder, (String)Others.qTagActaRevision.get(x).get("NOM_TAG"), pDato);
								logger.info(String.format("%s Línea %d %s Error - <%s> necesita valor de forma obligatoria %s%n", 
										Others.gcNum_Orden, Others.lFilasProc, Others.vCodTipoOrdenEorder, (String)Others.qTagActaRevision.get(x).get("NOM_TAG"), pDato));
								
								Others.vCodOperacion=Others.vCodErrASY013;
								Others.vObservacion=Others.vDesErrASY013 +" "+ (String)Others.qTagActaRevision.get(x).get("NOM_TAG");
								Others.lDatosErr++;
								Others.iIndicaErr=1;
								return false;
							}else {
								if(valida_formato(pDato,(String)Others.qTagActaRevision.get(x).get("TIPO"))==false) {
									Others.printWriterinFile1.printf("%s Línea %d %s Error - <%s> necesita valor de forma obligatoria %s%n", 
											Others.gcNum_Orden, Others.lFilasProc, Others.vCodTipoOrdenEorder, (String)Others.qTagActaRevision.get(x).get("NOM_TAG"), pDato);
									logger.info(String.format("%s Línea %d %s Error - <%s> necesita valor de forma obligatoria %s%n", 
											Others.gcNum_Orden, Others.lFilasProc, Others.vCodTipoOrdenEorder, (String)Others.qTagActaRevision.get(x).get("NOM_TAG"), pDato));
									
									Others.vCodOperacion=Others.vCodErrASY024;
									Others.vObservacion=Others.vDesErrASY024 +" "+ (String)Others.qTagActaRevision.get(x).get("NOM_TAG");
									Others.lDatosErr++;
									Others.iIndicaErr=1;
									return false;
								}
							}						
						}else
						{	break;
						}
					}
				}
			}
		}
		
		return true;
	}

	private boolean valida_formato(String campo, String tipo) {
		int i=0;
		if(tipo.equalsIgnoreCase("alfanumerico") || tipo.equalsIgnoreCase("texto")) {
			return true;
		}else {
			if(tipo.equalsIgnoreCase("numerico")){ /* Numerico */
				int x= 0;
				for (i = 0; i < campo.length(); i++) {
					if (Character.isDigit(campo.charAt(i))){
						// Tiene caracter numerico
			        }else{
			        	// No es caracter numerico
			           x++;
			        }
				}
				if(x==0) {
					return true;
				}else {
					return false;
				}
			}else {
				if(tipo.equalsIgnoreCase("fecha")){ /* Fecha */
					if(campo.length()>10) {
				        StringBuilder newString = new StringBuilder(campo);
				        newString.setCharAt(10, ' ');
				        campo=newString.toString();
//				        
//				        StringBuilder newString2 = new StringBuilder(campo);
//				        newString2.setCharAt(19, '0');
//				        campo=newString2.toString();
					}
					if((VerificaFormatoFecha(campo, "YYYY-MM-DD HH24:MI:SS") == false) && (VerificaFormatoFecha(campo, "YYYY-MM-DD") == false)) {
						return false;
					}
				}
			}
		}
		return true;
	}

	private boolean VerificaFormatoFecha(String pszFecha, String pszPatron) {
		String fechaVerificada=printplanoScomDAO.verifyFechaFormatted(pszFecha,pszPatron);
		if(fechaVerificada.equals("") && fechaVerificada!="") {
			return false;
		}
		return true;
	}

	private boolean bfnDeclararCursorImprimir() {
		
		/*Obtener datos de factibilidad - requisitos obligatorios*/
//		Others.qFactReqObli=printplanoScomDAO.getRequisitosObligatorios(Others.vIdOrdTransfer,Others.vNroRecepciones);
		
		/*Obtener datos de factibilidad - requisitos opcionales*/
//		Others.qFactReqOpci=printplanoScomDAO.getRequisitosOpcionales(Others.vIdOrdTransfer,Others.vNroRecepciones);
		
		/*Obtener datos de factibilidad - informacion adicional*/
//		Others.qFactInfoAdi=printplanoScomDAO.getInformacionAdicional(Others.vIdOrdTransfer,Others.vNroRecepciones);
		
		/*Obtener datos de factibilidad - suministros*/
//		Others.qFactOtrosSuministro=printplanoScomDAO.getSuministros(Others.vIdOrdTransfer,Others.vNroRecepciones);
		
		/* Obtener Datos Clave de Procesos y Datos Comunes de Procesos */
		Others.qDatoClave=printplanoScomDAO.getDatosClaves(Others.vIdOrdTransfer,Others.vNroRecepciones);
			
		/* Colección Operaciones  */
		Others.qOper=printplanoScomDAO.getColeccionOperaciones(Others.vIdOrdTransfer,Others.vNroRecepciones);
		
		/* Colección Anexos -- Parte Comentada en el archivo .pc*/ 
		
		/* Colección Tareas */
		Others.qTareas=printplanoScomDAO.getColeccionTareas(Others.vIdOrdTransfer,Others.vNroRecepciones);
		
//		/* Colección Recursos */
//		/* Utilizado para obtener nombre de tecnico*/
//		Others.qRecursos=printplanoScomDAO.getColeccionRecursosParaObtenerNombresdeTecnico(Others.vIdOrdTransfer,Others.vNroRecepciones);
		
		/* Colección Materiales Insumidos */
		/* Mostrar en Archivo = "NO" para todos, no se coloca lógica */
		
		
		/* Colección Medidores */
		Others.qMedidores=printplanoScomDAO.getColeccionMedidores(Others.vIdOrdTransfer,Others.vNroRecepciones);

		/* Colección Lecturas */
		Others.qLecturas=printplanoScomDAO.getColeccionLecturas(Others.vIdOrdTransfer,Others.vNroRecepciones);
		
//		/* Colección Sellos */
//		Others.qSellos=printplanoScomDAO.getColeccionSellos(Others.vIdOrdTransfer,Others.vNroRecepciones);
		
		/*Datos de Ejecución de órdenes de Mantenimiento y Factibilidad*/
		/* Obtener Datos de Acta de Revisión */
		Others.qActaRevision=printplanoScomDAO.getDatosActaDeRevision(Others.vIdOrdTransfer,Others.vNroRecepciones);
		
		/* Obtencion motivo,tema y trabajo para ORM*/
		Others.qORM=printplanoScomDAO.getMotivoTemayTrabajo(Others.vIdOrdTransfer,Others.vNroRecepciones);
		
		return true;
	}

	public boolean bfnActualizaRegistro() {
		boolean updated=false;
		int x=0;
		try
         {
			x= printplanoScomDAO.actualizaTablaTransferencia(Others.vCodOperacion,Others.vCodEstRecepError,Others.vObservacion,Others.vIdOrdTransfer,Others.vNroRecepciones);
			if(x>0) {
				updated= true;
			}
         }
         catch (Exception ex)
         {
        	 logger.error("Error en bfnActualizaRegistro - No se pudo actualizar EOR_ORD_TRANSFER - EOR_ORD_TRANSFER_DET"); 
        	 return false;
         }
		return updated;
	}

	public void probarTmpOrmFacRequisito() {
		logger.info("Probando obli scom");
		List<Map<String, Object>> reqOb = printplanoScomDAO.getRequisitosObligatorios(85073543L, 1L);
		for (Map<String, Object> req : reqOb) {
		    logger.info("Requisito Obligatorio: {}", req.toString());
		}
		logger.info("Probando opci scom");
		List<Map<String, Object>> reqOp = printplanoScomDAO.getRequisitosOpcionales(85073543L, 1L);
		for (Map<String, Object> req : reqOp) {
		    logger.info("Requisito Opcional: {}", req.toString());
		}
	}
	
}
