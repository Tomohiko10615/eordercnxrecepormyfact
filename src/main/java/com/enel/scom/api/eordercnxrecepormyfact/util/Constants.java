package com.enel.scom.api.eordercnxrecepormyfact.util;

public final class Constants {
	
	/* DEFINES PUBLICOS ***********************************************************/
	 public static final String PROCESO = "EOrderCNXRecepORMyFact";
	 public static final Integer NODATAFOUND = 1403;
	 public static final String PAIS = "EDEL";
	 public static final String SIST_SYN = "SYNERGIA";
	 public static final Integer MAX_TAGS = 1403;
	 public static final Integer MAX_REGS = 1403;
	 
	 /*PARAMETROS ARCHIVOS*/
	 public static final String SG = "#";
	 public static final String ST = "&";
	 public static final String SC = "\t";
	 public static final String SL = "|";
	 public static final String SCL = "*";
	 
	 /*PARAMETROS TDC*/
	 public static final String TDC_FACTIBILIDAD = "NCX.05";
	 public static final String TDC_ORM = "NCX.03";
	 public static final Integer FAC_REQUISITO_VALIDO = 1;
	 public static final Integer FAC_REQUISITO_NO_VALIDO = 0;
	 public static final String FAC_RESULTADO_APROBADO = "APROBADO";
	 public static final String FAC_RESULTADO_RECHAZADO = "RECHAZADO";

	 public static final String RUTA_LINUX_DES_LOCAL = "D:/cfdiaze/indra/linux";

	 
	 
}
