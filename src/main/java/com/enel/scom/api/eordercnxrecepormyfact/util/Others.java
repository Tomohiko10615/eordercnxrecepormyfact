package com.enel.scom.api.eordercnxrecepormyfact.util;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

public final class Others {

	public static int lAcumulaTemp;
	public static int lFilasProc;
	public static int lFilasErr;
	public static int lFilasCorrectas;
	public static int lDatosErr;
	
	public static Integer gi_Empresa;
	public static Long gIdUsuario;
	public static String gcPathOUT;
	public static String gcPathORMIn;
	public static Integer vCodEstPendRecep;
	public static Integer vCodEstEnProceso;
	public static Integer vCodEstRecep;
	public static Integer vCodEstRecepError;
	public static String vCodErrASY000;
	public static String vDesErrASY000;
	public static String vCodErrASY002;
	public static String vDesErrASY002;
	public static String vCodErrASY013;
	public static String vDesErrASY013;
	public static String vCodErrASY024;
	public static String vDesErrASY024;
	public static String vCodErrASY045;
	public static String vDesErrASY045;
	public static String vCodErrASY099;
	public static String vDesErrASY099;
	public static String cCodCuadrillaGen;
	public static String vTipoRespon;
	public static String vRolRespon;
	public static String vCodPrior;
	public static String gcNomArchivoERR;
	public static String gcNomArchivoMasivaORM_SR;
	public static String gcNomArchivoMasivaORM;
	public static String gcNomArchivoMasivaFACT;
	public static List<Map<String, Object>> qTagComun;
	public static int iTotTagComun;
	public static List<Map<String, Object>> qTagActaRevision;
	public static int iTotTagActaRevision;
	public static List<Map<String, Object>> qRegPro;
	public static int iIndicaErr;
	public static String vObservacion;
	public static String vCodOperacion;
	
	public static Long vIdOrdTransfer;
	public static Long vNroRecepciones;
	public static String vCodTipoOrdenEorder;
	public static String vNroOrden;
	
	public static List<Map<String, Object>> qFactReqObli;
	public static List<Map<String, Object>> qFactReqOpci;
	public static Map<String, Object> qFactInfoAdi;
	public static List<Map<String, Object>> qFactOtrosSuministro;
	public static Map<String, Object> qDatoClave;
	public static Map<String, Object> qOper;
	public static List<Map<String, Object>> qTareas;
	public static Map<String, Object> qRecursos;
	public static List<Map<String, Object>> qMedidores;
	public static List<Map<String, Object>> qLecturas;
	public static List<Map<String, Object>> qSellos;
	public static Map<String, Object> qActaRevision;
	public static Map<String, Object> qORM;
	
    public static FileWriter file1Writer;
    public static PrintWriter printWriterinFile1;
    
    public static FileWriter file2Writer;
    public static PrintWriter printWriterinFile2;
    
	public static String gcNum_Orden;
	public static Long vNroCuenta;
	public static int iConTareas;
	public static String vTecnico;
	public static String vCodEjecutor;
	
//	public static String vMarcaMedidorRetirar;
//	public static String vModeloMedidorRetirar;
//	public static String vNumeroMedidorRetirar;
//	public static String vMarcaMedidorInstalar;
//	public static String vModeloMedidorInstalar;
//	public static String vNumeroMedidorInstalar;
//	public static String vPropiedadMedidorInstalar;
//	public static String vFechaInstalacion;
	
	public static List<Map<String, Object>> arrMedidorRet;
	public static List<Map<String, Object>> arrMedidorInst;
	public static String vCodigoContratista;
	public static String vFecha;
	public static String vMinuto;
	public static String vHora;
	public static String vCodigoResultado;
	
	
	public static Map<String, Object> arrParamORM;
	public static int lFilasArchivoORM;
	public static int lFilasArchivoORF;
	public static int iflagerror;
	

	public static List<Map<String, Object>> qRegPro4J;
	public static long vIdOrden;
	
	public static String fechaObtenida;


}
