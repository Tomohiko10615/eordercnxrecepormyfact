package com.enel.scom.api.eordercnxrecepormyfact.validator;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.eordercnxrecepormyfact.dao.IRequisitosDAO;
import com.enel.scom.api.eordercnxrecepormyfact.validator.dto.Requisito;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FactAcometidaValidator {

	@Autowired
	private IRequisitosDAO requisitosDAO;

	private static final int ID_FAC_TIPO_REQUISITO_OBLIGATORIO = 1;
	private static final int ID_FAC_TIPO_REQUISITO_OPCIONAL = 2;

	public int validarRequisitosObligatorios(Long idOrdTransfer, Long nroRecepciones, Integer idTipoAcometida) {
		List<Requisito> requisitosAEvaluar = requisitosDAO.obtenerRequisitosObligatorios(idOrdTransfer, nroRecepciones);
		log.info("Requisitos a evaluar: {}", requisitosAEvaluar.stream().sorted(Comparator.comparing(Requisito::getCodigoRequisito)).collect(Collectors.toList()).toString());
		List<Requisito> requisitosValidos = requisitosDAO.obtenerValoresValidos(idTipoAcometida,
				ID_FAC_TIPO_REQUISITO_OBLIGATORIO);
		log.info("Requisitos validos: {}", requisitosValidos.toString());
		boolean requisitosValidosContenidos = requisitosAEvaluar.containsAll(requisitosValidos);
		boolean requisitosAEvaluarContenidos = requisitosValidos.stream().allMatch(reqValido ->
		    requisitosAEvaluar.stream().anyMatch(reqAEvaluar ->
		        reqAEvaluar.getCodigoRequisito().equals(reqValido.getCodigoRequisito()) &&
		        reqAEvaluar.getValorRequisito().equals(reqValido.getValorRequisito())
		    )
		);

		return requisitosValidosContenidos && requisitosAEvaluarContenidos ? 1 : 0;
	}

	public int validarRequisitosOpcionales(Long idOrdTransfer, Long nroRecepciones, Integer idTipoAcometida) {
		List<Requisito> requisitosAEvaluar = requisitosDAO.obtenerRequisitosOpcionales(idOrdTransfer, nroRecepciones);
		log.info("Requisitos a evaluar: {}", requisitosAEvaluar.stream().sorted(Comparator.comparing(Requisito::getCodigoRequisito)).collect(Collectors.toList()).toString());
		List<Requisito> requisitosValidos = requisitosDAO.obtenerValoresValidos(idTipoAcometida,
				ID_FAC_TIPO_REQUISITO_OPCIONAL);
		log.info("Requisitos validos: {}", requisitosValidos.toString());
		// Iterar la lista de requisitos válidos
		for (Requisito requisitoValido : requisitosValidos) {
		    // Buscar el requisito válido en la lista de requisitos a evaluar
		    for (Requisito requisitoAEvaluar : requisitosAEvaluar) {
		        // Verificar si el requisito válido existe en la lista de requisitos a evaluar
		        if (requisitoValido.getCodigoRequisito().equals(requisitoAEvaluar.getCodigoRequisito())) {
		            // Buscar entre los requisitos válidos si hay algún requisito con el mismo código y valor
		            boolean requisitoEncontrado = false;
		            for (Requisito requisito : requisitosValidos) {
		                if (requisito.getCodigoRequisito().equals(requisitoAEvaluar.getCodigoRequisito()) &&
		                        requisito.getValorRequisito().equals(requisitoAEvaluar.getValorRequisito())) {
		                    requisitoEncontrado = true;
		                    break;
		                }
		            }
		            // Si no se encuentra el requisito válido en los requisitos a evaluar, retornar 0
		            if (!requisitoEncontrado) {
		                return 0;
		            }
		            // Continuar con el siguiente requisito válido
		            break;
		        }
		    }
		}
		// Si no se cumple ninguna de las condiciones anteriores, retornar 1
		return 1;
		
		/*Implementación con streams
		boolean tienenMismoValor = requisitosValidos.stream() // Itera sobre la lista de requisitos válidos
		        .filter(requisitoValido -> requisitosAEvaluar.stream() // Filtra los requisitos a evaluar
		                .anyMatch(requisitoAEvaluar -> requisitoAEvaluar.getCodigoRequisito().equals(requisitoValido.getCodigoRequisito()))) // Verifica si algún requisito a evaluar tiene el mismo código que el requisito válido
		        // Se han obtenido todos los requisitos válidos que tienen algún match entre los requisitos a evaluar y pasan al siguiente flujo
		        .allMatch(requisitoValido -> requisitosAEvaluar.stream() // Verifica que todos los requisitos válidos tengan el mismo valor que algún requisito a evaluar
		                .filter(requisitoAEvaluar -> requisitoAEvaluar.getCodigoRequisito().equals(requisitoValido.getCodigoRequisito())) // Filtra los requisitos a evaluar por aquellos con el mismo código que el requisito válido
		                // Se ha obtenido el requisito a evaluar que tiene un match entre los requisitos válidos filtrados
		                .anyMatch(requisitoAEvaluar -> requisitoAEvaluar.getValorRequisito().equals(requisitoValido.getValorRequisito()))); // Verifica si el requisito a evaluar tiene el mismo valor que el requisito válido

		return tienenMismoValor ? 1 : 0;
		*/
	}
}

	