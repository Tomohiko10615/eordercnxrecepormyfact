package com.enel.scom.api.eordercnxrecepormyfact.validator.dto;

import java.util.Objects;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class Requisito {
	private String codigoRequisito;
	private Boolean valorRequisito;
	
	@Override
	public boolean equals(Object o) {
	    if (this == o) return true;
	    if (o == null || getClass() != o.getClass()) return false;
	    Requisito requisito = (Requisito) o;
	    return Objects.equals(codigoRequisito, requisito.codigoRequisito) &&
	            Objects.equals(valorRequisito, requisito.valorRequisito);
	}

	@Override
	public int hashCode() {
	    return Objects.hash(codigoRequisito, valorRequisito);
	}
	
	@Override
	public String toString() {
	    return String.format("%ncodigo=%s - valor=%s%n",
	            codigoRequisito, valorRequisito);
	}
}
