package com.enel.scom.api.eordercnxrecepormyfact.validator.dto;

import java.util.Comparator;

public class RequisitoComparator implements Comparator<Requisito> {

	@Override
    public int compare(Requisito r1, Requisito r2) {
        return r1.getCodigoRequisito().compareTo(r2.getCodigoRequisito());
    }

}
