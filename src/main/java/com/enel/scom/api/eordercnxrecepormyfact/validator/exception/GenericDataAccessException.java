package com.enel.scom.api.eordercnxrecepormyfact.validator.exception;

import org.springframework.core.NestedRuntimeException;

public class GenericDataAccessException extends NestedRuntimeException {

	private static final long serialVersionUID = 1L;

	public GenericDataAccessException(String message, Throwable cause) {
        super(message, cause);
    }
}
