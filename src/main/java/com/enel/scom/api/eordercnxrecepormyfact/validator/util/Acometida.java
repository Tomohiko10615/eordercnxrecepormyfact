package com.enel.scom.api.eordercnxrecepormyfact.validator.util;

public enum Acometida {

	S("SUBTERRANEA"),
    A("AEREA"),
    M("MIXTA");

    private final String descripcion;

    Acometida(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
