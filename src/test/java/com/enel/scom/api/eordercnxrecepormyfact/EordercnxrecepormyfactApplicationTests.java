package com.enel.scom.api.eordercnxrecepormyfact;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import com.enel.scom.api.eordercnxrecepormyfact.validator.FactAcometidaValidator;
import com.enel.scom.api.eordercnxrecepormyfact.validator.dto.Requisito;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@SpringJUnitConfig(classes = EordercnxrecepormyfactApplication.class)
@Slf4j
class EordercnxrecepormyfactApplicationTests {
	
	@Autowired
	private FactAcometidaValidator factAcometidaValidator;
	
	@Data
	@AllArgsConstructor
	@ToString
	class Input {
		Long idOrdTransfer;
		Long nroRecepciones;
		Integer idTipoAcometida;
	}
	
	@Test
	void factAcometidaValidatorTest() {
		List<Input> testInputs = new ArrayList<>();
		//testInputs.add(new Input(187923L, 1L, 10));
		//testInputs.add(new Input(279343L, 1L, 10));
		//testInputs.add(new Input(216183L, 1L, 10));
		//testInputs.add(new Input(216181L, 1L, 10));
		//testInputs.add(new Input(236167L, 1L, 10));
		testInputs.add(new Input(187898L, 1L, 10));
		/*
		testInputs.add(new Input(1182159L, 1L, 10));
		
		
		
		
		
		testInputs.add(new Input(278874L, 1L, 10));
		testInputs.add(new Input(279108L, 1L, 10));
		testInputs.add(new Input(236342L, 1L, 10));
		testInputs.add(new Input(276721L, 1L, 10));
		testInputs.add(new Input(279261L, 1L, 10));
		testInputs.add(new Input(187625L, 1L, 10));
		testInputs.add(new Input(216184L, 1L, 10));
		testInputs.add(new Input(212676L, 1L, 10));
		testInputs.add(new Input(1182136L, 1L, 10));
		testInputs.add(new Input(187947L, 1L, 10));
		testInputs.add(new Input(187937L, 1L, 10));
		testInputs.add(new Input(1182137L, 1L, 10));
		testInputs.add(new Input(216167L, 1L, 10));
		testInputs.add(new Input(1182139L, 1L, 10));
		testInputs.add(new Input(277189L, 1L, 10));
		testInputs.add(new Input(216169L, 1L, 10));
		testInputs.add(new Input(329188L, 1L, 10));
		testInputs.add(new Input(1182075L, 1L, 10));
		testInputs.add(new Input(235933L, 1L, 10));
		testInputs.add(new Input(1182157L, 1L, 10));
		testInputs.add(new Input(212677L, 1L, 10));
		testInputs.add(new Input(216170L, 1L, 10));
		testInputs.add(new Input(22073341L, 1L, 10));
		testInputs.add(new Input(187883L, 1L, 10));
		testInputs.add(new Input(216168L, 1L, 10));
		testInputs.add(new Input(826056L, 1L, 10));
		testInputs.add(new Input(826089L, 1L, 10));
		testInputs.add(new Input(833042L, 1L, 10));
		testInputs.add(new Input(235934L, 1L, 10));
		testInputs.add(new Input(589065L, 1L, 10));
		testInputs.add(new Input(187928L, 1L, 10));
		testInputs.add(new Input(216182L, 1L, 10));
		testInputs.add(new Input(228716L, 1L, 10));
		testInputs.add(new Input(277426L, 1L, 10));
		testInputs.add(new Input(236341L, 1L, 10));
		testInputs.add(new Input(1182036L, 1L, 10));
		testInputs.add(new Input(279107L, 1L, 10));
		*/
		for (Input input : testInputs) {
			int result = factAcometidaValidator.validarRequisitosObligatorios(input.getIdOrdTransfer(), input.getNroRecepciones(), input.getIdTipoAcometida());
			if (result == 1) {
				log.info("RESULTADO PRUEBA REQUISITOS OBLIGATORIOS EXITOSO: IdOrdTransfer={}", input.getIdOrdTransfer());
			} else {
				log.info("RESULTADO PRUEBA REQUISITOS OBLIGATORIOS FALLIDO: IdOrdTransfer={}", input.getIdOrdTransfer());
			}
		}
		
		for (Input input : testInputs) {
			int result = factAcometidaValidator.validarRequisitosOpcionales(input.getIdOrdTransfer(), input.getNroRecepciones(), input.getIdTipoAcometida());
			if (result == 1) {
				log.info("RESULTADO PRUEBA REQUISITOS OPCIONALES EXITOSO: IdOrdTransfer={}", input.getIdOrdTransfer());
			} else {
				log.info("RESULTADO PRUEBA REQUISITOS OPCIONALES FALLIDO: IdOrdTransfer={}", input.getIdOrdTransfer());
			}
		}
	}

	//@Test
	void validacionObligatoriaTest() {
		List<Requisito> listaRequisitosValidos = new ArrayList<>();
		listaRequisitosValidos.add(new Requisito("OBLI01", true));
		listaRequisitosValidos.add(new Requisito("OBLI02", true));
		listaRequisitosValidos.add(new Requisito("OBLI03", true));
		listaRequisitosValidos.add(new Requisito("OBLI03", false)); // Repetido con valor diferente
		listaRequisitosValidos.add(new Requisito("OBLI04", true));
		listaRequisitosValidos.add(new Requisito("OBLI05", true));
		listaRequisitosValidos.add(new Requisito("OBLI06", true));
		listaRequisitosValidos.add(new Requisito("OBLI07", true));
		listaRequisitosValidos.add(new Requisito("OBLI08", true));
		
		//Valido
		List<Requisito> listaRequisitosAEvaluar1 = new ArrayList<>();
		listaRequisitosAEvaluar1.add(new Requisito("OBLI01", true));
		listaRequisitosAEvaluar1.add(new Requisito("OBLI02", true));
		listaRequisitosAEvaluar1.add(new Requisito("OBLI03", true));
		listaRequisitosAEvaluar1.add(new Requisito("OBLI04", true));
		listaRequisitosAEvaluar1.add(new Requisito("OBLI05", true));
		listaRequisitosAEvaluar1.add(new Requisito("OBLI06", true));
		listaRequisitosAEvaluar1.add(new Requisito("OBLI07", true));
		listaRequisitosAEvaluar1.add(new Requisito("OBLI08", true));
		
		//Valido
		List<Requisito> listaRequisitosAEvaluar2 = new ArrayList<>();
		listaRequisitosAEvaluar2.add(new Requisito("OBLI01", true));
		listaRequisitosAEvaluar2.add(new Requisito("OBLI02", true));
		listaRequisitosAEvaluar2.add(new Requisito("OBLI03", false));
		listaRequisitosAEvaluar2.add(new Requisito("OBLI04", true));
		listaRequisitosAEvaluar2.add(new Requisito("OBLI05", true));
		listaRequisitosAEvaluar2.add(new Requisito("OBLI06", true));
		listaRequisitosAEvaluar2.add(new Requisito("OBLI07", true));
		listaRequisitosAEvaluar2.add(new Requisito("OBLI08", true));
		
		//Invalido
		List<Requisito> listaRequisitosAEvaluar3 = new ArrayList<>();
		listaRequisitosAEvaluar3.add(new Requisito("OBLI01", true));
		listaRequisitosAEvaluar3.add(new Requisito("OBLI02", true));
		listaRequisitosAEvaluar3.add(new Requisito("OBLI04", true));
		listaRequisitosAEvaluar3.add(new Requisito("OBLI05", true));
		listaRequisitosAEvaluar3.add(new Requisito("OBLI06", true));
		listaRequisitosAEvaluar3.add(new Requisito("OBLI07", true));
		listaRequisitosAEvaluar3.add(new Requisito("OBLI08", true));
		
		//Invalido
		List<Requisito> listaRequisitosAEvaluar4 = new ArrayList<>();
		listaRequisitosAEvaluar4.add(new Requisito("OBLI01", true));
		listaRequisitosAEvaluar4.add(new Requisito("OBLI02", true));
		listaRequisitosAEvaluar4.add(new Requisito("OBLI03", true));
		listaRequisitosAEvaluar4.add(new Requisito("OBLI04", true));
		listaRequisitosAEvaluar4.add(new Requisito("OBLI05", true));
		listaRequisitosAEvaluar4.add(new Requisito("OBLI06", true));
		listaRequisitosAEvaluar4.add(new Requisito("OBLI08", true));
		
		//Invalido
		List<Requisito> listaRequisitosAEvaluar5 = new ArrayList<>();
		listaRequisitosAEvaluar5.add(new Requisito("OBLI01", true));
		listaRequisitosAEvaluar5.add(new Requisito("OBLI02", true));
		listaRequisitosAEvaluar5.add(new Requisito("OBLI03", true));
		listaRequisitosAEvaluar5.add(new Requisito("OBLI04", true));
		listaRequisitosAEvaluar5.add(new Requisito("OBLI05", true));
		listaRequisitosAEvaluar5.add(new Requisito("OBLI06", true));
		listaRequisitosAEvaluar5.add(new Requisito("OBLI07", true));
		listaRequisitosAEvaluar5.add(new Requisito("OBLI08", false));
		
		//Invalido
		List<Requisito> listaRequisitosAEvaluar6 = new ArrayList<>();
		
		List<List<Requisito>> requisitos = Arrays.asList(listaRequisitosAEvaluar1, listaRequisitosAEvaluar2, listaRequisitosAEvaluar3, listaRequisitosAEvaluar4, listaRequisitosAEvaluar5, listaRequisitosAEvaluar6);
	
		for (List<Requisito> lista : requisitos) {
			int valido = validarRequisitosObligatorios(lista, listaRequisitosValidos);
			log.info("OBLI {}", valido);
		}
	}
	
	public static int validarRequisitosObligatorios(List<Requisito> requisitosAEvaluar, List<Requisito> requisitosValidos) {
		return requisitosAEvaluar.stream().allMatch(requisitosValidos::contains) &&
		           requisitosValidos.stream().allMatch(reqValido ->
		               requisitosAEvaluar.stream().anyMatch(reqAEvaluar ->
		                   reqAEvaluar.getCodigoRequisito().equals(reqValido.getCodigoRequisito())
		               )
		           ) ? 1 : 0;
	}
	
	//@Test
	void validacionOpcionalTest() {
		List<Requisito> listaRequisitosValidos = new ArrayList<>();
		listaRequisitosValidos.add(new Requisito("OPCI01", true));
		listaRequisitosValidos.add(new Requisito("OPCI02", true));
		listaRequisitosValidos.add(new Requisito("OPCI03", true));
		listaRequisitosValidos.add(new Requisito("OPCI03", false)); // Repetido con valor diferente
		listaRequisitosValidos.add(new Requisito("OPCI04", true));
		listaRequisitosValidos.add(new Requisito("OPCI05", true));
		listaRequisitosValidos.add(new Requisito("OPCI06", true));
		listaRequisitosValidos.add(new Requisito("OPCI07", true));
		listaRequisitosValidos.add(new Requisito("OPCI08", true));
		
		//Valido
		List<Requisito> listaRequisitosAEvaluar1 = new ArrayList<>();
		listaRequisitosAEvaluar1.add(new Requisito("OPCI01", true));
		listaRequisitosAEvaluar1.add(new Requisito("OPCI02", true));
		listaRequisitosAEvaluar1.add(new Requisito("OPCI03", false));
		
		//Valido
		List<Requisito> listaRequisitosAEvaluar2 = new ArrayList<>();
		listaRequisitosAEvaluar2.add(new Requisito("OPCI01", true));
		listaRequisitosAEvaluar2.add(new Requisito("OPCI02", true));
		listaRequisitosAEvaluar2.add(new Requisito("OPCI03", false));
		listaRequisitosAEvaluar2.add(new Requisito("OPCI07", true));
		
		//Invalido
		List<Requisito> listaRequisitosAEvaluar3 = new ArrayList<>();
		listaRequisitosAEvaluar3.add(new Requisito("OPCI01", true));
		listaRequisitosAEvaluar3.add(new Requisito("OPCI02", true));
		listaRequisitosAEvaluar3.add(new Requisito("OPCI04", false));
		
		//Invalido
		List<Requisito> listaRequisitosAEvaluar4 = new ArrayList<>();
		listaRequisitosAEvaluar4.add(new Requisito("OPCI06", true));
		listaRequisitosAEvaluar4.add(new Requisito("OPCI07", true));
		listaRequisitosAEvaluar4.add(new Requisito("OPCI08", true));
		
		//Valido
		List<Requisito> listaRequisitosAEvaluar5 = new ArrayList<>();
		
		List<List<Requisito>> requisitos = Arrays.asList(listaRequisitosAEvaluar1, listaRequisitosAEvaluar2, listaRequisitosAEvaluar3, listaRequisitosAEvaluar4, listaRequisitosAEvaluar5);
	
		for (List<Requisito> lista : requisitos) {
			int valido = validarRequisitosOpcionales(lista, listaRequisitosValidos);
			log.info("OPCI {}", valido);
		}
	}

	private int validarRequisitosOpcionales(List<Requisito> requisitosAEvaluar, List<Requisito> requisitosValidos) {
		return requisitosAEvaluar.stream().allMatch(requisitosValidos::contains) ? 1 : 0;
	}

}
